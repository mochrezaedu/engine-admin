import { current } from "@reduxjs/toolkit"

Array.prototype.first = function() {
  return this.length > 0 ? this[0]: null
}
Array.prototype.all = function() {
  return this
}
Array.prototype.reorderAsc = function(startIndex,endIndex) {
  var result = this;
  
  if(result.length > 0) {
    result.splice(endIndex, 0, result.splice(startIndex, 1)[0]);
    for(var i = 0; i < result.length; i++) {
      result[i]['order'] = i
    }
  }

  return result;
}

Array.prototype.mergeByKey = function(items, key) {
  if(!(items instanceof Array)) {
      throw 'Merge prop must be an Array.';
  }
  if(!key) {
      throw 'Merge key required.';
  }

  const values = this
  const array = items

  array.forEach((a,b) => {
      const found = values.findIndex((c) => c.id === a[key || 'id']);
      if(found === -1) {
        const z = a
        values.push(z);
      }
  })
  const _n = values
  return _n
}