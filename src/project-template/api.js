import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const Api = {
  all: createAsyncThunk('api_name/all', async () => {
    let result = await axios.get('/flow_group_keyword/all');
    return result.data;
  }),
  store: createAsyncThunk('api_name/store', async (args, {rejectWithValue}) => {
    const {data,successAction,rejectAction} = args
    try {
      const api = await axios.post('/api_name/store', data)
      if(api.data) {
        if(successAction) {
          successAction()
        }
      }
      return api.data
    } catch (error) {
      if(rejectAction) {
        rejectAction(error.response?.data.message)
      }
      return rejectWithValue(error)
    }
  }),
  destroy: createAsyncThunk('api_name/destroy', async (args, {rejectWithValue}) => {
    const {data,successAction,rejectAction} = args
    try {
      const api = await axios.post('/api_name/destroy', data)
      if(api.data) {
        if(successAction) {
          successAction()
        }
      }
      return api.data
    } catch (error) {
      if(rejectAction) {
        rejectAction(error.response?.data.message)
      }
      return rejectWithValue(error)
    }
  }),
}

export default Api