import { createSlice } from "@reduxjs/toolkit";
import Model from '../../helpers/Model'
import { getStoreState } from "../../helpers/Store";

const path = 'database.your_reducer_path'

const BaseModel = new Model(path) 

const table = {
  data: [],
  pendingIds: [],
  failedIds: [],
  status: 'idle',
  message: {
    type: null,
    text: null
  }
};
const methods = {
  // YOUR METHODS
  ...BaseModel.extractSetter(),
  messageCleanup: (state,action) => {
    state.message = {
      type: null,
      text: null
    }
  }
}

export const slice_name = createSlice({
  name: 'your_slicer_name',
  initialState: table,
  reducers: methods,
  extraReducers: {}
})

export default slice_name;

export const ModelAction = slice_name.actions

export const ModelGetter = {
  ...BaseModel.extractGetter(),
  getStatus: (state) => getStoreState(state,path)['status'],
  getMessage: (state) => getStoreState(state,path)['message']
}