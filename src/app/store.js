import { configureStore } from '@reduxjs/toolkit';

// SLICERS
import counterReducer from '../features/counter/counterSlice';
import Engine from '../features/Engines/EngineSlicer';
import Modal from '../services/app/modal/ModalSlicer';
import InputSingle from '../services/app/input/InputSlicer';

// DATABASE
import Database from '../services/datacenter/index';
import Loader from '../services/app/loader';
import Auth from '../services/app/Auth';
import FlowNextAction from '../services/app/engine/FlowNextAction';
import AlertSlicer from '../services/app/alert/AlertSlicer';
import Application from '../features/Apps/Slice';

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    basicModal: Modal.reducer,
    inputSingle: InputSingle.reducer,
    flowNextAction: FlowNextAction.reducer,
    engine: Engine.reducer,
    database: Database,
    loader: Loader,
    alert: AlertSlicer.reducer,
    auth: Auth.reducer,
    application: Application.reducer
  },
});
