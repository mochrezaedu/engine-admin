import { createTheme } from '@mui/material';

export const theme = createTheme({
  components: {
    MuiButtonBase: {
      defaultProps: {
        disableRipple: true,
      },
    },
    MuiListItemButton: {
      styleOverrides: {
        root: {
          "&.Mui-selected": {
            background: '#37517E',
            color: "white",
            "&.Mui-focusVisible": { background: "#37517E" },
            "&:hover": {
              background: '#37517E',
              color: "white"
            } 
          },
          "&:hover": {
            background: 'transparent',
          } 
        }
      }
    }
  },
  palette: {
    primary: {
      main: '#37517E',
      darker: '#053e85',
    }
  },
});