import { createSlice } from "@reduxjs/toolkit";
import Model from '../../helpers/Model'
import FlowTypeApi from "../api/FlowType/api";

export const path = 'database.flow_type'

const BaseModel = new Model(path) 

const table = {
  data: [],
  pendingIds: [],
  failedIds: [],
  status: 'idle'
};
const methods = {
  // YOUR METHODS
  ...BaseModel.extractSetter()
}

export const flow_type = createSlice({
  name: 'flow_type',
  initialState: table,
  reducers: methods,
  extraReducers: {
    [FlowTypeApi.all.pending]: (state,action) => {
      state.status = 'pending'
    },
    [FlowTypeApi.all.pending]: (state,action) => {
      state.status = 'idle'
    },
    [FlowTypeApi.all.fulfilled]: (state,action) => {
      BaseModel.createMany(state,action)
      state.status = 'idle'
    },
    [FlowTypeApi.store.fulfilled]: (state,action) => {
      const {payload} = action
      BaseModel.create(state,{payload: payload.flow_type});
    },
    [FlowTypeApi.toggleFlowTypeUsable.fulfilled]: (state,action) => {
      const {payload} = action
      BaseModel.update(state,{payload: payload.flow_type})
    }
  }
})

export default flow_type;

export const FlowTypeAction = flow_type.actions

export const FlowTypeGetter = {
  ...BaseModel.extractGetter()
}