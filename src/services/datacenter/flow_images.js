import { createSlice } from "@reduxjs/toolkit";
import Model from '../../helpers/Model'
import { getStoreState } from "../../helpers/Store";
import FlowImageApi from "../api/FlowImages/api";

const path = 'database.flow_images'

const BaseModel = new Model(path) 

const table = {
  data: [],
  pendingIds: [],
  failedIds: [],
  status: 'idle'
};
const methods = {
  // YOUR METHODS
  ...BaseModel.extractSetter()
}

export const flow_images = createSlice({
  name: 'table_flow_images',
  initialState: table,
  reducers: methods,
  extraReducers: {
    [FlowImageApi.all.fulfilled]: (state,action) => {
      BaseModel.createMany(state,action)
    },
    [FlowImageApi.store.fulfilled]: (state,action) => {
      const {payload} = action
      BaseModel.create(state,{payload: payload.flow_image})
    }
  }
})

export default flow_images;

export const FlowImagesAction = flow_images.actions

export const FlowImagesGetter = {
  ...BaseModel.extractGetter(),
  getStatus: (state) => getStoreState(state,path)['status']
}