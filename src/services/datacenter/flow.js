import { createSlice } from "@reduxjs/toolkit";
import { Uniqid } from "../../helpers/helper";
import Model from '../../helpers/Model'
import { getStoreState } from "../../helpers/Store";
import FlowApi from "../api/Flow/api";
import { storeSingleFlow } from "../api/Resources";
import { alertActions } from "../app/alert/AlertSlicer";
import { modalActions } from "../app/modal/ModalSlicer";

// CONSTANTS
export const path = 'database.flow'

const BaseModel = new Model(path) 

const table = {
  data: [],
  pendingIds: [],
  failedIds: [],
  status: 'idle',
  message: {
    type: null,
    text: null
  }
};
const methods = {
  ...BaseModel.extractSetter(),
  createData: (state,action) => {
    const {payload} = action
    const rows = state.data

    const findIndex = rows.findIndex((item) => item.id === payload.id)
    if(findIndex > -1) {
      var newData = {...rows[findIndex].data}
      newData['-new-'+Uniqid()] = ''
      rows[findIndex].data = {...newData}
    }

    state.data = [...rows]
  },
  messageCleanup: (state,action) => {
    state.message = {
      type: null,
      text: null
    }
  }
}

export const flow = createSlice({
  name: 'table_flow',
  initialState: table,
  reducers: methods,
  extraReducers: {
    [storeSingleFlow.fulfilled]: (state,action) => {
      state.status = 'idle'
    },
    [storeSingleFlow.rejected]: (state,action) => {
      BaseModel.createFailed(state, {payload: action.payload.existing_id})
    },
    [storeSingleFlow.pending]: (state,action) => {
      state.status = 'pending'
    },
    [FlowApi.all.fulfilled]: (state,action) => {
      BaseModel.createMany(state,action)
    },
    [FlowApi.autoOrder.fulfilled]: (state,action) => {
      const {payload} = action
      BaseModel.delete(state,{payload: [payload.flow.id]})
      BaseModel.create(state,{payload: payload.flow})
    },
    [FlowApi.store.fulfilled]: (state,action) => {
      const {payload} = action
      BaseModel.create(state,{payload:payload.item})
      BaseModel.deletePending(state,{payload: [payload.existing_payload.id]})
      if(payload.item) {
        modalActions.openModal(state, {
          activate: true,
          title: payload.item.name,
          name: 'add-new-flow-'+payload.item.id,
          loading: true
        })
      }
    },
    [FlowApi.store.pending]: (state,action) => {

    },
    [FlowApi.store.rejected]: (state,action) => {
      const {payload} = action
      state.failedIds = state.failedIds.filter((item) => item != payload.response.data.flow.id)
      state.failedIds = [...state.failedIds,payload.response.data.flow.id]
      state.message = {
        type: 'error',
        text: payload.response.data.message
      }
    },
    [FlowApi.updateSingleAttribute.fulfilled]: (state,action) => {
      BaseModel.update(state,{payload:action.payload.item})
    },
    [FlowApi.storeFlowNext.fulfilled]: (state,action) => {
      BaseModel.create(state,{payload:action.payload.item})
      BaseModel.update(state,{payload:action.payload.updated_item})
    },
    [FlowApi.storeFlowNext.rejected]: (state,action) => {
      const {payload} = action
    }
  }
})

export default flow;

export const actions = flow.actions
export const FlowModelActions = flow.actions

export const Getter = BaseModel.extractGetter()
export const FlowModelGetter = {
  ...BaseModel.extractGetter(),
  getMessage: (state) => getStoreState(state,path)['message']
}