import { createSlice } from "@reduxjs/toolkit";
import Model from '../../helpers/Model'
import { getStoreState } from "../../helpers/Store";
import BotAccountApi from "../api/BotAccount/api";

const path = 'database.bot_account'

const BaseModel = new Model(path) 

const table = {
  data: [],
  pendingIds: [],
  failedIds: [],
  status: 'idle',
  message: {
    type: null,
    text: null
  }
};
const methods = {
  // YOUR METHODS
  ...BaseModel.extractSetter(),
  messageCleanup: (state,action) => {
    state.message = {
      type: null,
      text: null
    }
  }
}

export const bot_account = createSlice({
  name: 'bot_account',
  initialState: table,
  reducers: methods,
  extraReducers: {
    [BotAccountApi.all.fulfilled]: (state,action) => {
      const {payload} = action
      BaseModel.createMany(state, {payload:payload})
    },
    [BotAccountApi.store.fulfilled]: (state,action) => {
      const {payload} = action
      BaseModel.create(state,{payload:payload.data})
    },
    [BotAccountApi.destroy.fulfilled]: (state,action) => {
      const {payload} = action
      BaseModel.delete(state,{payload: [payload.data.id]})
    }
  }
})

export default bot_account;

export const BotAccountAction = bot_account.actions

export const BotAccountGetter = {
  ...BaseModel.extractGetter(),
  getStatus: (state) => getStoreState(state,path)['status'],
  getMessage: (state) => getStoreState(state,path)['message']
}