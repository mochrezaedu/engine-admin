import { createSlice } from "@reduxjs/toolkit";
import Model from '../../helpers/Model'
import { getStoreState } from "../../helpers/Store";
import FlowTextImageEditorListImageApi from "../api/FlowTextImageEditorListImage/api";

const path = 'database.flow_text_image_editor_list_image'

const BaseModel = new Model(path) 

const table = {
  data: [],
  pendingIds: [],
  failedIds: [],
  status: 'idle',
  message: {
    type: null,
    text: null
  }
};
const methods = {
  // YOUR METHODS
  ...BaseModel.extractSetter()
}

export const flow_text_image_editor_list_image = createSlice({
  name: 'table_flow_text_image_editor_list_image',
  initialState: table,
  reducers: methods,
  extraReducers: {
    [FlowTextImageEditorListImageApi.all.pending]: (state,action) => {
      state.status = 'pending'
    },
    [FlowTextImageEditorListImageApi.all.fulfilled]: (state,action) => {
      BaseModel.createMany(state,action)
      state.status = 'idle'
    },
    [FlowTextImageEditorListImageApi.store.fulfilled]: (state,action) => {
      BaseModel.create(state,{payload: action.payload.data})
      state.status = 'idle'
    },
    [FlowTextImageEditorListImageApi.destroy.fulfilled]: (state,action) => {
      const {payload} = action
      BaseModel.delete(state,{payload: [payload.existing_payload.id]})
      state.status = 'idle'
    }
  }
})

export default flow_text_image_editor_list_image;

export const FlowTextImageEditorListImageActions = flow_text_image_editor_list_image.actions

export const FlowTextImageEditorListImageGetter = {
  ...BaseModel.extractGetter(),
  getStatus: (state) => getStoreState(state,path)['status'],
  getMessage: (state) => getStoreState(state,path)['message']
}