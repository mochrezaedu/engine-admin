import { createSlice } from "@reduxjs/toolkit";
import Model from '../../helpers/Model'
import FlowWikipedia from "../api/FlowWikipedia/api";

const path = 'database.flow_wikipedia'

const BaseModel = new Model(path) 

const table = {
  data: [],
  pendingIds: [],
  failedIds: [],
  status: 'idle'
};
const methods = {
  // YOUR METHODS
  ...BaseModel.extractSetter()
}

export const flow_wikipedia = createSlice({
  name: 'flow_wikipedia',
  initialState: table,
  reducers: methods,
  extraReducers: {
    [FlowWikipedia.all.fulfilled]: (state,action) => {
      BaseModel.createMany(state,action)
    },
  }
})

export default flow_wikipedia;

export const actions = flow_wikipedia.actions
export const FlowWikiPediaAction = flow_wikipedia.actions

export const Getter = {
  // YOUR METHODS
  ...BaseModel.extractGetter()
}
export const FlowWikipediaGetter = {
  // YOUR METHODS
  ...BaseModel.extractGetter()
}