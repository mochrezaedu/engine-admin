import { createSlice, createEntityAdapter } from "@reduxjs/toolkit";
import Model from "../../helpers/Model";
import { store } from "../api/FlowGroup/api";

import { getFlowGroups } from "../api/Resources";

// CONSTANTS
export const path = 'database.flow_group'

const BaseModel = new Model(path)

const table = {
  data: [],
  pendingIds: [],
  failedIds: [],
  status: 'idle'
};
const methods = {
  ...BaseModel.extractSetter()
}

export const flow_group = createSlice({
  name: 'table_flow_group',
  initialState: table,
  reducers: methods,
  extraReducers: {
    [store.pending]: (state, action) => {

    },
    [store.fulfilled]: (state, action) => {
      const {payload} = action
      BaseModel.create(state, {payload: payload.flow_group})
    },
    [getFlowGroups.pending]: (state, action) => {
      state.status = 'pending'
    },
    [getFlowGroups.pending]: (state, action) => {
      state.status = 'idle'
    },
    [getFlowGroups.fulfilled]: (state, action) => {
      var payloads = action.payload
      payloads.map((item) => {
        item?.flow_masters?.map((itemMaster) => {
          if(itemMaster.data) {
            itemMaster.data = JSON.parse(itemMaster.data)
          } else {
            itemMaster.data = {}
          }

          return itemMaster
        })

        return item
      })
      state.data = payloads
      state.status = 'idle'
    }
  }
})

export default flow_group;

export const actions = flow_group.actions
export const flowGroupAction = flow_group.actions

export const Getter = BaseModel.extractGetter()
export const flowGroupGetter = BaseModel.extractGetter()