import { createSlice } from "@reduxjs/toolkit";
import Model from '../../helpers/Model'
import { getStoreState } from "../../helpers/Store";
import FlowGroupKeywordApi from "../api/FlowGroupKeyword/api";

const path = 'database.flow_group_keyword'

const BaseModel = new Model(path) 

const table = {
  data: [],
  pendingIds: [],
  failedIds: [],
  status: 'idle',
  message: {
    type: null,
    text: null
  }
};
const methods = {
  // YOUR METHODS
  ...BaseModel.extractSetter(),
  messageCleanup: (state,action) => {
    state.message = {
      type: null,
      text: null
    }
  }
}

export const flow_group_keyword = createSlice({
  name: 'flow_group_keyword',
  initialState: table,
  reducers: methods,
  extraReducers: {
    [FlowGroupKeywordApi.all.fulfilled]: (state,action) => {
      BaseModel.createMany(state,action)
    },
    [FlowGroupKeywordApi.store.fulfilled]: (state,action) => {
      const {payload} = action
      BaseModel.create(state,{payload: payload.item})
    },
    [FlowGroupKeywordApi.destroy.fulfilled]: (state,action) => {
      const {payload} = action
      BaseModel.delete(state,{payload: [payload.existing_payload.id]})
    },
  }
})

export default flow_group_keyword;

export const FlowGroupKeywordActions = flow_group_keyword.actions

export const FlowGroupKeywordGetter = {
  ...BaseModel.extractGetter(),
  getStatus: (state) => getStoreState(state,path)['status'],
  getMessage: (state) => getStoreState(state,path)['message']
}