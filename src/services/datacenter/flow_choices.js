import { createSlice } from "@reduxjs/toolkit";
import Model from '../../helpers/Model'
import FlowChoice from "../api/FlowChoice/api";

const path = 'database.flow_choices'

const BaseModel = new Model(path) 

const table = {
  data: [],
  pendingIds: [],
  failedIds: [],
  status: 'idle'
};
const methods = {
  // YOUR METHODS
  ...BaseModel.extractSetter()
}

export const flow_choices = createSlice({
  name: 'table_flow_choices',
  initialState: table,
  reducers: methods,
  extraReducers: {
    [FlowChoice.store.fulfilled]: (state,action) => {
      const {payload} = action
      BaseModel.delete(state,{payload: [payload.existing_payload.id]})
      BaseModel.deletePending(state,{payload: [payload.existing_payload.id]})
      BaseModel.create(state,{payload:payload.item})
    },
    [FlowChoice.get.fulfilled]: (state,action) => {
      BaseModel.createMany(state,action)
    }
  }
})

export default flow_choices;

export const actions = flow_choices.actions
export const FlowChoiceActions = flow_choices.actions

export const Getter = {
  // YOUR METHODS
  ...BaseModel.extractGetter()
}
export const FlowChoiceGetter = {
  // YOUR METHODS
  ...BaseModel.extractGetter()
}