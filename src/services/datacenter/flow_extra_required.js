import { createSlice } from "@reduxjs/toolkit";
import Model from '../../helpers/Model'
import { getStoreState } from "../../helpers/Store";
import FlowExtraRequired from "../api/FlowExtraRequired/api";

const path = 'database.flow_extra_required'

const BaseModel = new Model(path) 

const table = {
  data: [],
  pendingIds: [],
  failedIds: [],
  status: 'idle'
};
const methods = {
  // YOUR METHODS
  ...BaseModel.extractSetter()
}

export const flow_extra_required = createSlice({
  name: 'table_flow_extra_required',
  initialState: table,
  reducers: methods,
  extraReducers: {
    [FlowExtraRequired.all.pending]: (state, action) => {
      state.status = 'pending'
    },
    [FlowExtraRequired.all.rejected]: (state, action) => {
      state.status = 'idle'
    },
    [FlowExtraRequired.all.fulfilled]: (state, action) => {
      if(state.data.length === 0) {
        BaseModel.createMany(state,action)
      }
      state.status = 'idle'
    },
    [FlowExtraRequired.store.fulfilled]: (state,action) => {
      const {payload} = action
      BaseModel.delete(state,{payload:[payload.existing_payload.id]})
      BaseModel.create(state,{payload:payload.flow_type_required})
    }
  }
})

export default flow_extra_required;

export const actions = flow_extra_required.actions
export const FlowExtraRequiredActions = flow_extra_required.actions

export const Getter = {
  // YOUR METHODS
  ...BaseModel.extractGetter()
}
export const FlowExtraRequiredGetter = {
  // YOUR METHODS
  ...BaseModel.extractGetter(),
  getStatus: (state) => getStoreState(state,path)['status']
}