import { createSlice } from "@reduxjs/toolkit";
import Model from '../../helpers/Model'
import { getStoreState } from "../../helpers/Store";
import FlowExtraChoiceApi from "../api/FlowExtraChoice/api";

const path = 'database.flow_extra_choice'

const BaseModel = new Model(path) 

const table = {
  data: [],
  pendingIds: [],
  failedIds: [],
  status: 'idle'
};
const methods = {
  // YOUR METHODS
  ...BaseModel.extractSetter()
}

export const flow_extra_choice = createSlice({
  name: 'table_flow_extra_choice',
  initialState: table,
  reducers: methods,
  extraReducers: {
    [FlowExtraChoiceApi.rejected]: (state,action) => {
      const {payload} = action
      alert('error')
    },
    [FlowExtraChoiceApi.get.fulfilled]: (state,action) => {
      BaseModel.createMany(state,action)
    },
    [FlowExtraChoiceApi.store.fulfilled]: (state,action) => {
      const {payload} = action
      BaseModel.create(state,{payload: payload.item})
    },
    [FlowExtraChoiceApi.destroy.fulfilled]: (state,action) => {
      const {payload} = action
      BaseModel.delete(state,{payload: [payload.existing_item.id]})
    }
  }
})

export default flow_extra_choice;

export const FlowExtraChoiceAction = flow_extra_choice.actions

export const FlowExtraChoiceGetter = {
  ...BaseModel.extractGetter(),
  getStatus: (state) => getStoreState(state,path)['status']
}