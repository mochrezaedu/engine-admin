import { createSlice } from "@reduxjs/toolkit";
import Model from '../../helpers/Model'
import { getStoreState } from "../../helpers/Store";
import FlowTextImageEditorListApi from "../api/FlowTextImageEditorList/api";

const path = 'database.flow_text_image_editor_list'

const BaseModel = new Model(path) 

const table = {
  data: [],
  pendingIds: [],
  failedIds: [],
  status: 'idle',
  message: {
    type: null,
    text: null
  }
};
const methods = {
  // YOUR METHODS
  ...BaseModel.extractSetter()
}

export const flow_text_image_editor_list = createSlice({
  name: 'table_flow_text_image_editor_list',
  initialState: table,
  reducers: methods,
  extraReducers: {
    [FlowTextImageEditorListApi.all.fulfilled]: (state,action) => {
      BaseModel.createMany(state,action)
    },
    [FlowTextImageEditorListApi.store.fulfilled]: (state,action) => {
      const {payload} = action
      if(payload.flow_text_image_editor_list) {
        BaseModel.create(state,{payload: payload.flow_text_image_editor_list})
      }
    },
    [FlowTextImageEditorListApi.destroy.fulfilled]: (state,action) => {
      const {payload} = action
      BaseModel.delete(state,{payload: [payload.existing_flow_text_image_editor_list.id]})
    },
    [FlowTextImageEditorListApi.updateSingleAttribute.fulfilled]: (state,action) => {
      BaseModel.update(state,{payload:action.payload.item})
    }
  }
})

export default flow_text_image_editor_list;

export const FlowTextImageEditorListAction = flow_text_image_editor_list.actions

export const FlowTextImageEditorListGetter = {
  ...BaseModel.extractGetter(),
  getStatus: (state) => getStoreState(state,path)['status'],
  getMessage: (state) => getStoreState(state,path)['message']
}