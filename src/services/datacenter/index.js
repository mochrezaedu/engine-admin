import { combineReducers } from "@reduxjs/toolkit";

import flow_group from "./flow_group";
import flow from "./flow";
import flow_choices from "./flow_choices";
import flow_extra from "./flow_extra";
import flow_extra_required from "./flow_extra_required";
import flow_wikipedia from "./flow_wikipedia";
import upscale_dataset_names from "./upscale_dataset_names";
import flow_type from "./flow_type";
import application from "./application";
import flow_extra_choice from "./flow_extra_choice";
import flow_images from "./flow_images";
import flow_text_image_editor_list from "./flow_text_image_editor_list";
import flow_text_image_editor_list_image from "./flow_text_image_editor_list_image";
import flow_group_keyword from "./flow_group_keyword";
import bot_account from "./bot_account";

export default combineReducers({
  flow_group: flow_group.reducer, 
  flow: flow.reducer, 
  flow_choices: flow_choices.reducer,
  flow_extra: flow_extra.reducer,
  flow_extra_required: flow_extra_required.reducer,
  flow_wikipedia: flow_wikipedia.reducer,
  flow_type: flow_type.reducer,
  flow_extra_choice: flow_extra_choice.reducer,
  flow_images: flow_images.reducer,
  flow_group_keyword: flow_group_keyword.reducer,
  flow_text_image_editor_list: flow_text_image_editor_list.reducer,
  flow_text_image_editor_list_image: flow_text_image_editor_list_image.reducer,
  upscale_dataset_names: upscale_dataset_names.reducer,
  application:application.reducer,
  bot_account:bot_account.reducer
});