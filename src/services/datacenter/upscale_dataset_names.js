import { createSlice } from "@reduxjs/toolkit";
import Model from '../../helpers/Model'
import { getUpscaleTables } from "../api/Resources";

const path = 'database.upscale_dataset_names'

const BaseModel = new Model(path) 

const table = {
  data: [],
  pendingIds: [],
  failedIds: [],
  status: 'idle'
};
const methods = {
  // YOUR METHODS
  ...BaseModel.extractSetter()
}

export const upscale_dataset_names = createSlice({
  name: 'upscale_dataset_names',
  initialState: table,
  reducers: methods,
  extraReducers: {
    [getUpscaleTables.fulfilled]: (state,action) => {
      BaseModel.createMany(state,action)
    }
  }
})

export default upscale_dataset_names;

export const UpscaleTablesAction = upscale_dataset_names.actions

export const UpscaleTablesGetter = {
  ...BaseModel.extractGetter()
}