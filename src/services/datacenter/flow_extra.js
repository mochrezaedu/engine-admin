import { createSlice } from "@reduxjs/toolkit";
import Model from '../../helpers/Model'
import FlowExtra from "../api/FlowExtra/api";

const path = 'database.flow_extra'

const BaseModel = new Model(path) 

const table = {
  data: [],
  pendingIds: [],
  failedIds: [],
  status: 'idle'
};
const methods = {
  // YOUR METHODS
  ...BaseModel.extractSetter()
}

export const flow_extra = createSlice({
  name: 'table_flow_extra',
  initialState: table,
  reducers: methods,
  extraReducers: {
    [FlowExtra.get.fulfilled]: (state,action) => {
      const {payload} = action
      if(payload.length > 0) {
        BaseModel.sync(state,{
          payload: {
            data: payload,
            filter: {flow_id: payload.first().flow_id}
          } 
        })
      }
    },
    [FlowExtra.refillExtra.fulfilled]: (state,action) => {
      const {payload} = action
      BaseModel.sync(state,{
        payload: {
          data: payload.extras,
          filter: {flow_id: payload.existing_payload.flow_id}
        }
      })
    }
  }
})

export default flow_extra;

export const actions = flow_extra.actions
export const FlowExtraModelActions = flow_extra.actions

export const Getter = {
  // YOUR METHODS
  ...BaseModel.extractGetter()
}
export const FlowExtraModelGetter = {
  // YOUR METHODS
  ...BaseModel.extractGetter()
}