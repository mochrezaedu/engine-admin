import { createSlice } from "@reduxjs/toolkit";
import Model from '../../helpers/Model'
import { getStoreState } from "../../helpers/Store";
import ApplicationApi from "../api/Application/api";

const path = 'database.application'

const BaseModel = new Model(path) 

const table = {
  data: [],
  pendingIds: [],
  failedIds: [],
  status: 'idle'
};
const methods = {
  // YOUR METHODS
  ...BaseModel.extractSetter()
}

export const application = createSlice({
  name: 'table_application',
  initialState: table,
  reducers: methods,
  extraReducers: {
    [ApplicationApi.all.fulfilled]: (state,action) => {
      BaseModel.createMany(state,action)
    },
    [ApplicationApi.store.fulfilled]: (state,action) => {
      const {payload} = action
      BaseModel.delete(state,{payload: [payload.existing_id]})
      BaseModel.create(state,{payload: payload.application})
    },
  }
})

export default application;

export const ApplicationActions = application.actions

export const ApplicationGetter = {
  ...BaseModel.extractGetter(),
  getStatus: (state) => getStoreState(state,path)['status']
}