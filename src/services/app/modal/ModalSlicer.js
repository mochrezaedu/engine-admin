import { createSlice } from "@reduxjs/toolkit";


const initialState = {
  activate: false,
  name: '',
  title: '',
  loading: false
}

const methods = {
  closeModal: (state, action) => {
    state.activate = false
    state.name = ''
    state.title = ''
    state.loading = false
  },
  openModal: (state, action) => {
    state.activate = action.payload.activate
    state.title = action.payload.title
    state.name = action.payload.name
    state.loading = action.payload.loading || false
  },
  setModal: (state, action) => {
    state[action.payload.key] = action.payload.value
  },
  cleanup: () => initialState,
}

const Modal = createSlice({
  name: 'basic_modal',
  initialState: initialState,
  reducers: methods
})

export const actions = Modal.actions;
export const modalActions = Modal.actions;

export default Modal;

export const path = 'basicModal'

export const get = (state,key) => state[path][key]