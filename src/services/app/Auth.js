import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { login } from "../api/Auth/api";

export const path = 'auth'

const initialState = {
  user: null,
  loading: false,
  errors: [],
  error: null
}

const methods = {
  logout: () => {
    axios.defaults.headers.common['Authorization'] = 'Bearer c0c9f02f0350d789b052adc51df1f7a7'
    return initialState
  },
  login: (state,action) => {
    const {payload} = action
    state.user = payload.user
    axios.defaults.headers.common['Authorization'] = 'Bearer '+state.user.token
  },
  clearError: (state,action) => {
    state.error = null
    state.errors = []
  }
}

const Auth = createSlice({
  name: 'auth',
  initialState: initialState,
  reducers: methods,
  extraReducers: {
    [login.pending]: (state,action) => {
      state.loading = true
    },
    [login.rejected]: (state,action) => {
      state.errors = action?.payload?.errors || []
      state.error = action?.payload?.error || null
      if(state.errors.length === 0 && !state.error) {
        state.error = 'Network Error! Check your connection.'
      }
      state.loading = false
    },
    [login.fulfilled]: (state,action) => {
      if(action.payload.user) {
        Auth.caseReducers.login(state,action)
      } else if(action.payload.errors || action.payload.error) {
        state.errors = action.payload.errors || []
        state.error = action.payload.error || null
      }
      state.loading = false
    }
  }
})

export const authActions = Auth.actions;

export default Auth;


export const getUser = (state,key) => state[path]['user']
export const getLoading = (state,key) => state[path]['loading']
export const getErrors = (state) => state[path]['errors']
export const getError = (state) => state[path]['error']