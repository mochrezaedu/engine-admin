import { createSlice } from "@reduxjs/toolkit";


const initialState = {
  type: null,
  text: null
}

const methods = {
  openAlert: (state,action) => {
    const {payload} = action
    state.type = payload.type
    state.text = payload.text
  },
  closeAlert: () => initialState
}

const AlertSlicer = createSlice({
  name: 'alert',
  initialState: initialState,
  reducers: methods
})

export const actions = AlertSlicer.actions;
export const alertActions = AlertSlicer.actions;

export default AlertSlicer;

export const path = 'alert'

export const getState = (state) => state[path]
export const getAlert = (state,key) => state[path]
export const getAlertType = (state) => state[path]['type']
export const getAlertText = (state) => state[path]['text']