import { createSlice } from "@reduxjs/toolkit";


const initialState = {
  activate: null,
  key: '',
  value: '',
  loading: false
}

const methods = {
  openInput: (state,action) => {
    state.activate = action.payload.activate
    state.value = action.payload.value || ''
    state.key = action.payload.key || ''
  },
  closeInput: () => initialState,
  setState: (state,action) => {
    state[action.payload.key] = action.payload.value
  },
  setValue: (state,action) => {
    state.value = action.payload
  },
  onChange: (state,action) => {
    //
  },
  onEnter: (state,action) => {
    //
  },
  cleanup: () => initialState,
}

const InputSingle = createSlice({
  name: 'input_single',
  initialState: initialState,
  reducers: methods
})

export const actions = InputSingle.actions;
export const inputActions = InputSingle.actions;

export default InputSingle;

export const path = 'inputSingle'

export const getState = (state) => state[path]
export const get = (state,key) => state[path][key]
export const getInput = (state,key) => state[path][key]