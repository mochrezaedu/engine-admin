import { combineReducers } from "@reduxjs/toolkit";
import RootLoaderSlice from "./RootLoaderSlice";

export default combineReducers({
  root: RootLoaderSlice.reducer
});