import { createSlice } from "@reduxjs/toolkit";
import { getStoreState } from "../../../helpers/Store";

const path = 'loader.root'

const initialState = {
  activeLoaders: []
}

const methods = {
  add: (state,action) => {
    const {payload} = action
    state.activeLoaders = [...state.activeLoaders, payload]
  },
  remove: (state,action) => {
    const {payload} = action
    state.activeLoaders = state.activeLoaders.filter((item) => item !== payload)
  }
}

const RootLoaderSlice = createSlice({
  name: 'root',
  initialState: initialState,
  reducers: methods
})

export const rootLoaderActions = RootLoaderSlice.actions;

export default RootLoaderSlice;

export const getRootLoaderState = (state) => getStoreState(state,path)
export const getRootLoader = (state,key) => getStoreState(state,path)[key]
export const isActive = (state) => {
  return getStoreState(state,path)['activeLoaders']?.length > 0
}