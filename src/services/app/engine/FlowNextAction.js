import { createSlice } from "@reduxjs/toolkit";


const initialState = {
  active: false,
  source: null,
}

const methods = {
  closeAction: () => initialState,
  setNextMode: (state,action) =>  {
    const {payload} = action
    Object.keys(payload).map((item) => {
      state[item] = payload[item]
    })
  }
}

const FlowNextAction = createSlice({
  name: 'flow_next_action',
  initialState: initialState,
  reducers: methods
})

export const flowNextActions = FlowNextAction.actions;

export default FlowNextAction;

export const path = 'flowNextAction'

export const getFlowNextState = (state) => state[path]
export const get = (state,key) => state[path][key]