import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const FlowChoice = {
  store: createAsyncThunk('flow_choice/store', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      let result = await axios.post('/flow_choice/store', data);
      return result.data;
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  updateAttribute: createAsyncThunk('flow_choice/updateAttribute', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/flow_choice/update_single_attribute', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  destroy: createAsyncThunk('flow_choice/destroy', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/flow_choice/destroy', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  get: createAsyncThunk('flow_choice/get_flow_choices', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/flow_choice/get_flow_choices', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
}

export default FlowChoice