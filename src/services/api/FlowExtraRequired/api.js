import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const FlowExtraRequired = {
  all: createAsyncThunk('flow_type/all_extra_required', async () => {
    let result = await axios.get('/flow_type/all_extra_required');
    return result.data;
  }),
  store: createAsyncThunk('flow_type/store_master_data', async (args, {rejectWithValue}) => {
    const data = new FormData()
    Object.keys(args.data).map((item) => {
      data.append(item,args.data[item])
    })

    try {
      let result = await axios.post('/flow_type/store_master_data', data, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });
      return result.data;
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  destroy: createAsyncThunk('flow_type/destroy_master_data', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/flow_type/destroy_master_data', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  update: createAsyncThunk('flow_type/update_master_data', async (args, {rejectWithValue}) => {
    var data = args.data
    if(args.data.image instanceof File) {
      data = new FormData()
      Object.keys(args.data).map((item) => {
        data.append(item,args.data[item])
      })
    }
    try {
      let result = await axios.post('/flow_type/update_master_data', data, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });
      return result.data;
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
}

export default FlowExtraRequired