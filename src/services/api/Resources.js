import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const getFlowGroups = createAsyncThunk('flow/byGroup', async () => {
  let result = await axios.get('/flow_group/all');
  return result.data;
})
export const storeSingleFlow = createAsyncThunk('flow/store_empty', async (args, {rejectWithValue}) => {
  try {
    let result = await axios.post('flow/store_empty', {...args})
    return result.data 
  } catch (e) {
    return rejectWithValue(e.response.data)
  }
})

export const getUpscaleTables = createAsyncThunk('resource/get_resource_model_names', async () => {
  let result = await axios.get('/resource/get_resource_model_names');
  return result.data;
})

export const getUpscaleTableColumnList = createAsyncThunk('resource/get_table_column_list', async (args, {rejectWithValue}) => {
  try {
    let result = await axios.post('resource/get_table_column_list', {...args})
    return result.data 
  } catch (e) {
    return rejectWithValue(e.response.data)
  }
})

export const ApiPost = (url, params) => {
  return createAsyncThunk(url, async ({rejectWithValue}) => {
    try {
      let result = await axios.post(url, {...params})
      return result.data 
    } catch (e) {
      return rejectWithValue(e.response.data)
    }
  })
}