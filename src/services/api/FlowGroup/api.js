import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const store = createAsyncThunk('flowGroup/store', async (args, {rejectWithValue}) => {
  const {data} = args
  try {
    const api = await axios.post('/flow_group/store', data)
    return api.data
  } catch (error) {
    return rejectWithValue(error)
  }
})
export const destroy = createAsyncThunk('flowGroup/destroy', async (args, {rejectWithValue}) => {
  const {data} = args
  try {
    const api = await axios.post('/flow_group/destroy', data)
    return api.data
  } catch (error) {
    return rejectWithValue(error)
  }
})
export const setDefaultRoute = createAsyncThunk('flowGroup/setDefaultRoute', async (args, {rejectWithValue}) => {
  const {data} = args
  try {
    const api = await axios.post('/flow_group/set_default_group', data)
    return api.data
  } catch (error) {
    return rejectWithValue(error)
  }
})
export const updateName = createAsyncThunk('flowGroup/updateName', async (args, {rejectWithValue}) => {
  const {data} = args
  try {
    const api = await axios.post('/flow_group/update_name', data)
    return api.data
  } catch (error) {
    return rejectWithValue(error)
  }
})
export const updateSingleAttribute = createAsyncThunk('flowGroup/updateSingleAttribute', async (args, {rejectWithValue}) => {
  const {data} = args
  try {
    const api = await axios.post('/flow_group/update_single_attribute', data)
    return api.data
  } catch (error) {
    return rejectWithValue(error)
  }
})

const FlowGroupApi = {
  updateSingleAttribute
}

export default FlowGroupApi