import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const all = createAsyncThunk('flow/all', async () => {
  let result = await axios.get('/flow/all');
  return result.data;
})
const store = createAsyncThunk('flow/store', async (args, {rejectWithValue}) => {
  const {data} = args
  try {
    const api = await axios.post('/flow/store', data)
    return api.data
  } catch (error) {
    return rejectWithValue(error)
  }
})
const destroy = createAsyncThunk('flow/destroy', async (args, {rejectWithValue}) => {
  const {data} = args
  try {
    const api = await axios.post('/flow/destroy', data)
    return api.data
  } catch (error) {
    return rejectWithValue(error)
  }
})
const setDefaultRoute = createAsyncThunk('flow/setDefaultRoute', async (args, {rejectWithValue}) => {
  const {data} = args
  try {
    const api = await axios.post('/flow/set_default', data)
    return api.data
  } catch (error) {
    return rejectWithValue(error)
  }
})
const updateName = createAsyncThunk('flow/updateName', async (args, {rejectWithValue}) => {
  const {data} = args
  try {
    const api = await axios.post('/flow/update_name', data)
    return api.data
  } catch (error) {
    return rejectWithValue(error)
  }
})

const FlowApi = {
  all,
  store: store,
  destroy: destroy,
  setDefaultRoute: setDefaultRoute,
  updateName: updateName,
  updateSingleAttribute: createAsyncThunk('flow/updateName', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/flow/update_single_attribute', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  reorder: createAsyncThunk('flow/reorder', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/flow/reorder', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  autoOrder: createAsyncThunk('flow/auto_order', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/flow/auto_order', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  storeFlowNext: createAsyncThunk('flow/store_flow_next', async (args, {rejectWithValue}) => {
    const {data,successAction,rejectAction} = args
    try {
      const api = await axios.post('/flow/store_flow_next', data)
      if(api.data) {
        if(successAction) {
          successAction()
        }
      }
      return api.data
    } catch (error) {
      if(rejectAction) {
        rejectAction(error.response?.data.message)
      }
      return rejectWithValue(error)
    }
  })
}

export default FlowApi