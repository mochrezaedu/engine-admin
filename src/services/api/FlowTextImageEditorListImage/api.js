import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const FlowTextImageEditorListImageApi = {
  all: createAsyncThunk('flow_text_image_editor_list/all_images', async () => {
    let result = await axios.get('/flow_text_image_editor_list/all_images');
    return result.data;
  }),
  store: createAsyncThunk('flow_text_image_editor_list/store_images', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      let result = await axios.post('/flow_text_image_editor_list/store_images', data);
      return result.data;
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  destroy: createAsyncThunk('flow_text_image_editor_list/destroy_images', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      let result = await axios.post('/flow_text_image_editor_list/destroy_images', data);
      return result.data;
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  reorder: createAsyncThunk('flow_text_image_editor_list/reorder_image', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      let result = await axios.post('/flow_text_image_editor_list/reorder_image', data);
      return result.data;
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
}

export default FlowTextImageEditorListImageApi