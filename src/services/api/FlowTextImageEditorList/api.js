import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const FlowTextImageEditorListApi = {
  all: createAsyncThunk('flow_text_image_editor_list/all', async () => {
    let result = await axios.get('/flow_text_image_editor_list/all');
    return result.data;
  }),
  store: createAsyncThunk('flow_text_image_editor_list/store', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      let result = await axios.post('/flow_text_image_editor_list/store', data);
      return result.data;
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  destroy: createAsyncThunk('flow_text_image_editor_list/destroy', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      let result = await axios.post('/flow_text_image_editor_list/destroy', data);
      return result.data;
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  reorder: createAsyncThunk('flow_text_image_editor_list/reorder', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      let result = await axios.post('/flow_text_image_editor_list/reorder', data);
      return result.data;
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  updateSingleAttribute: createAsyncThunk('flow_text_image_editor_list/update_single_attribute', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      let result = await axios.post('/flow_text_image_editor_list/update_single_attribute', data);
      return result.data;
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
}

export default FlowTextImageEditorListApi