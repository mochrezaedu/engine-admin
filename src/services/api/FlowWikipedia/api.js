import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const FlowWikipedia = {
  all: createAsyncThunk('flow_wikipedia/all', async () => {
    let result = await axios.get('/flow_wikipedia/all');
    return result.data;
  }),
  store: createAsyncThunk('flow_wikipedia/store', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      let result = await axios.post('/flow_wikipedia/store', data);
      return result.data;
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  updateAttribute: createAsyncThunk('flow_wikipedia/updateAttribute', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/flow_wikipedia/update_single_attribute', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  destroy: createAsyncThunk('flow_wikipedia/destroy', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/flow_wikipedia/destroy', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
}

export default FlowWikipedia