import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const FlowTypeApi = {
  all: createAsyncThunk('flow_type/all', async () => {
    let result = await axios.get('/flow_type/all');
    return result.data;
  }),
  store: createAsyncThunk('flow_type/store', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      let result = await axios.post('/flow_type/store', data);
      return result.data;
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  updateAttribute: createAsyncThunk('flow_type/updateAttribute', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/flow_type/update_single_attribute', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  destroy: createAsyncThunk('flow_type/destroy', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/flow_type/destroy', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  updateExampleImage: createAsyncThunk('flow_type/update_example_image', async (args, {rejectWithValue}) => {
    const data = new FormData()
    Object.keys(args.data).map((item) => {
      data.append(item,args.data[item])
    })
    
    try {
      const api = await axios.post('/flow_type/update_example_image', data, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  toggleFlowTypeUsable: createAsyncThunk('flow_type/toggle_flow_type_usable', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/flow_type/toggle_flow_type_usable', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
}

export default FlowTypeApi