import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const ApplicationApi = {
  store: createAsyncThunk('application/store', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/application/store', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  destroy: createAsyncThunk('application/destroy', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/application/destroy', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  setDefaultRoute: createAsyncThunk('application/setDefaultRoute', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/application/set_default_group', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  updateName: createAsyncThunk('application/updateName', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/application/update_name', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  update: createAsyncThunk('application/update', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/application/update', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  all: createAsyncThunk('application/all', async () => {
    let result = await axios.get('/application/all');
    return result.data;
  })
}

export default ApplicationApi