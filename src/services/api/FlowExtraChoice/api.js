import { createAsyncThunk } from "@reduxjs/toolkit"
import axios from "axios"

export const FlowExtraChoiceApi = {
  get: createAsyncThunk('flow_extra_choice/get', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      let result = await axios.post('flow_extra_choice/get', data);
      return result.data;
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  store: createAsyncThunk('flow_extra_choice/store', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      let result = await axios.post('flow_extra_choice/store', data);
      return result.data;
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  destroy: createAsyncThunk('flow_extra_choice/destroy', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      let result = await axios.post('flow_extra_choice/destroy', data);
      return result.data;
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
}

export default FlowExtraChoiceApi