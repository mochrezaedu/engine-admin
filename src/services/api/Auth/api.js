import { createAsyncThunk } from "@reduxjs/toolkit"
import axios from "axios"

export const login = createAsyncThunk('login', async (args,{rejectWithValue}) => {
  try {
    let result = await axios.post('login', {...args})
    return result.data 
  } catch (e) {
    return rejectWithValue(e.response.data)
  }
})