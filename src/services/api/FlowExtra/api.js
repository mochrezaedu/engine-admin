import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const FlowExtra = {
  update: createAsyncThunk('flow_extra/update', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      let result = await axios.post('/flow_extra/update', data);
      return result.data;
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  get: createAsyncThunk('flow_extra/get', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      let result = await axios.post('/flow_extra/get', data);
      return result.data;
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  refillExtra: createAsyncThunk('flow_extra/refill_extra', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      let result = await axios.post('/flow_extra/refill_extra', data);
      return result.data;
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
}

export default FlowExtra