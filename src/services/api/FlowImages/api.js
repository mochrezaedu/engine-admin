import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const FlowImageApi = {
  all: createAsyncThunk('flow_image/all', async () => {
    let result = await axios.get('/flow_image/all');
    return result.data;
  }),
  store: createAsyncThunk('flow_image/store', async (args, {rejectWithValue}) => {
    const data = new FormData()
    Object.keys(args.data).map((item) => {
      data.append(item,args.data[item])
    })
    
    try {
      const api = await axios.post('/flow_image/store', data, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
}

export default FlowImageApi