import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const FlowGroupKeywordApi = {
  all: createAsyncThunk('flow_group_keyword/all', async () => {
    let result = await axios.get('/flow_group_keyword/all');
    return result.data;
  }),
  store: createAsyncThunk('flow_group_keyword/store', async (args, {rejectWithValue}) => {
    const {data,successAction,rejectAction} = args
    try {
      const api = await axios.post('/flow_group_keyword/store', data)
      if(api.data) {
        if(successAction) {
          successAction()
        }
      }
      return api.data
    } catch (error) {
      if(rejectAction) {
        rejectAction(error.response?.data.message)
      }
      return rejectWithValue(error)
    }
  }),
  destroy: createAsyncThunk('flow_group_keyword/destroy', async (args, {rejectWithValue}) => {
    const {data,successAction,rejectAction} = args
    try {
      const api = await axios.post('/flow_group_keyword/destroy', data)
      if(api.data) {
        if(successAction) {
          successAction()
        }
      }
      return api.data
    } catch (error) {
      if(rejectAction) {
        rejectAction(error.response?.data.message)
      }
      return rejectWithValue(error)
    }
  }),
}

export default FlowGroupKeywordApi