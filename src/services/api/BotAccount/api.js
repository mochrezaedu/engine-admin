import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const BotAccountApi = {
  all: createAsyncThunk('bot_account/all', async () => {
    let result = await axios.get('/bot_account/all');
    return result.data;
  }),
  store: createAsyncThunk('bot_account/store', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/bot_account/store', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  destroy: createAsyncThunk('bot_account/destroy', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/bot_account/destroy', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
  update: createAsyncThunk('bot_account/update', async (args, {rejectWithValue}) => {
    const {data} = args
    try {
      const api = await axios.post('/bot_account/update', data)
      return api.data
    } catch (error) {
      return rejectWithValue(error)
    }
  }),
}

export default BotAccountApi