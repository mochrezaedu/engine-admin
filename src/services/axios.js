import axios from 'axios';

// axios.defaults.baseURL = 'https://upscale.id/api/admin';
axios.defaults.baseURL = `${process.env.REACT_APP_API_URL}/api/admin`;
axios.defaults.headers.common['Authorization'] = 'Bearer c0c9f02f0350d789b052adc51df1f7a7';