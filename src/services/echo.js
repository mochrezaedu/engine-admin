import Echo from "laravel-echo"
import {io} from './io'

export const Socket = new Echo({
  broadcaster: 'socket.io',
  client: io,
  host: window.location.hostname + ':6001'
});