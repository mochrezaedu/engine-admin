import React, { useEffect, useState } from 'react'
import './Modifiers/index'

import './App.css';

import Engine from './features/Engines/Engine';
import BaseLayout from './features/Layout/BaseLayout';
import {
  Route,
  Routes
} from 'react-router-dom'
import {default as FlowType} from './features/FlowType/Index';
import Apps from './features/Apps/Apps';
import { useSelector } from 'react-redux';
import { getUser } from './services/app/Auth';
import Login from './features/Auth/Login';
import FlowDetail from './features/Engines/FlowDetail';
import LiveChat from './features/LiveChat/LiveChat';
import BotAccount from './features/BotAccount/BotAccount';
import Dashboard from './features/Dashboard/Dashboard'; 
import { Socket } from './services/echo';

function App() {
  const user = useSelector(getUser)
  const appUrl = process.env.REACT_APP_API_URL

  useEffect(() => {
    if(user) {
      if(typeof(Socket) != undefined) {
        Socket.channel('channel-admin-global-conversations').listen('.event-new-conversations', (e) => {
          console.log(e)
        })
      }
    }
  }, [user])

  if(!user) {
    return <Login />
  }

  return (
    <Routes>
      <Route exact path={'/'} element={<Dashboard />} layout={BaseLayout} />
      <Route path={'/application'} element={<Apps />} layout={BaseLayout} />
      <Route path={'/flow'} element={<Engine />} />
      <Route path={'/bots'} element={<BotAccount />} />
      <Route path={'/application/:applicationId/g/:flowGroupId/flow'} element={<Engine />} />
      <Route path={'/application/:applicationId/flow/:flowId/detail'} element={<FlowDetail />} />
      <Route path={'/application/:applicationId/live_chat'} element={<LiveChat />} />
      <Route path={'/flow/type'} element={<FlowType />} />
    </Routes>
  );
}

export default App;
