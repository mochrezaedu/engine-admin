import React from 'react';
import { createRoot } from 'react-dom/client';
import { ThemeProvider } from '@mui/material/styles';
import { Provider } from 'react-redux';
import { store } from './app/store';
import { theme } from './app/theme';
import App from './App';
import reportWebVitals from './reportWebVitals';
import './index.css';
import './services/axios';
import { BrowserRouter } from 'react-router-dom';
import DndProvider from './components/DndProvider';

const container = document.getElementById('root');
const root = createRoot(container);

const index = () => {

  return (
    <React.StrictMode>
     <ThemeProvider theme={theme}>
       <Provider store={store}>
        <DndProvider>
          <BrowserRouter>
            <App />
          </BrowserRouter>
        </DndProvider>
       </Provider>
     </ThemeProvider>
    </React.StrictMode>
  )
}

root.render(index());

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();