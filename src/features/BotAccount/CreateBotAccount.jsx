import { Box, Button, MenuItem, Portal, Select, Typography } from "@mui/material";
import { useState } from "react";
import { useDispatch,useSelector } from "react-redux";
import BasicModal from "../../components/BasicModal";
import InputText from "../../components/InputText";
import { modalActions } from "../../services/app/modal/ModalSlicer";
import { getUser } from '../../services/app/Auth';
import { BotAccountAction } from "../../services/datacenter/bot_account";
import { BotAccountApi } from "../../services/api/BotAccount/api";

export default function CreateBotAccount(props) {
  const dispatch = useDispatch()
  const user = useSelector(getUser)
  const [name,setName] = useState('')
  const [email,setEmail] = useState('')
  const [bio,setBio] = useState('')
  const _handleSubmit = (args) => {
    dispatch(modalActions.closeModal())

    const payload = {name, email, bio}
    dispatch(BotAccountApi.store({
      data: payload
    }))
  }

  return (
    <Portal>
      <BasicModal title={'Create New Bot'} titleAction={
        <Button variant={'contained'} size={'small'} color={'success'} onClick={_handleSubmit}>Simpan</Button>
      }>
        <Box>
          <Box sx={{mb:2}}>
            <Typography variant={'subtitle2'} sx={{mb:1}}>Name</Typography>
            <InputText placeholder={'Bot Name'} value={name} onChange={(e) => setName(e.target.value)} />
          </Box>
          <Box sx={{mb:2}}>
            <Typography variant={'subtitle2'} sx={{mb:1}}>Email</Typography>
            <InputText placeholder={'Bot Email'} value={email} onChange={(e) => setEmail(e.target.value)} />
          </Box>
          <Box sx={{mb:2}}>
            <Typography variant={'subtitle2'} sx={{mb:1}}>Bio</Typography>
            <InputText placeholder={'Biography'} value={bio} onChange={(e) => setBio(e.target.value)} />
          </Box>
        </Box>
      </BasicModal>
    </Portal>
  )
}

const options = [
  'Web Application',
  'Mobile Application',
  'Social Media',
]