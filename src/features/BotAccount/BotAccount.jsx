import { Box,Paper, Button, List, ListItem, ListItemButton, ListItemText, Portal, Typography, IconButton } from "@mui/material";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import BasicModal from "../../components/BasicModal";
import { modalActions, get as getModal } from "../../services/app/modal/ModalSlicer";
import CreateBotAccount from "./CreateBotAccount";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { Link, useNavigate } from "react-router-dom";
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import DeleteIcon from '@mui/icons-material/Delete';
import { useCallback } from "react";
import { useEffect } from "react";
import BaseLayout from "../Layout/BaseLayout";
import { flowGroupGetter } from "../../services/datacenter/flow_group";
import { getFlowGroups } from "../../services/api/Resources";
import { BotAccountAction, BotAccountGetter } from "../../services/datacenter/bot_account";
import { BotAccountApi } from "../../services/api/BotAccount/api";
import { getUser } from "../../services/app/Auth";

export default function BotAccount(props) {
  const dispatch = useDispatch()
  const activeModal = useSelector((state) => getModal(state,'name'))
  const user = useSelector(getUser)
  const accounts = useSelector((state) => BotAccountGetter.get(state, (item) => item.user_id == user.id))
  const navigator = useNavigate()
  const _navigate = useCallback((args) => navigator(args, {replace:true}), [navigator])

  const _handleCreateBotAccount = () => {
    dispatch(modalActions.openModal({
      activate: true,
      title: '',
      name: 'create-bot-account',
      loading: false
    }))
  }
  const _handleUpdateBotAccount = () => {
    alert('updated')
  }
  const _handleDeleteBotAccount = (args) => {
    dispatch(BotAccountAction.delete([args.id]))
    dispatch(BotAccountApi.destroy({
      data: {id: args.id}
    }))
  }

  useEffect(() => {
    if(accounts.length === 0) {
      dispatch(BotAccountApi.all())
    }
  }, [])

  return (
    <BaseLayout>
      <Box>
        <Box sx={{mb:2,display: 'flex', alignItems:'center', justifyContent:'space-between'}}>
          <Typography variant={'subtitle2'}>Manage Bot Account</Typography>
          <Button onClick={_handleCreateBotAccount} variant={'outlined'} size={'small'}>Create Bot Account</Button>
        </Box>
        <Box>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell>Email</TableCell>
                  <TableCell>Bio</TableCell>
                  <TableCell></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {accounts.map((row) => (
                  <TableRow
                    key={row.id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell>{row.email}</TableCell>
                    <TableCell>{row.bio}</TableCell>
                    <TableCell align="right">
                      {/* <Link to={_getRoute(row)} title={'Open App'}><IconButton size={'small'}><PlayArrowIcon fontSize={'small'} /></IconButton></Link> */}
                      <IconButton size={'small'} disabled={true} onClick={_handleUpdateBotAccount}><ModeEditIcon fontSize={'small'} /></IconButton>
                      <IconButton size={'small'} onClick={() => _handleDeleteBotAccount(row)}><DeleteIcon fontSize={'small'} /></IconButton>
                    </TableCell>
                  </TableRow>
                ))}
                <TableRow>
                {
                  accounts.length === 0 &&
                  <TableCell align="center" colSpan={3}>No Bots</TableCell>
                }
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
        {
          activeModal === 'create-bot-account' &&
          <CreateBotAccount />
        }
      </Box>
    </BaseLayout>
  )
}
function createData(name, owner) {
  return { name, owner };
}
const rows = [
  createData('Frozen yoghurt', 'Admin'),
  createData('Ice cream sandwich', 'Admin')
];