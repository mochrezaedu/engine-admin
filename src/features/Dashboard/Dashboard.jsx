import { Box } from "@mui/material";
import BaseLayout from "../Layout/BaseLayout";

export default function Dashboard(props) {

  return (
    <BaseLayout>
      <Box>Dashboard</Box>
    </BaseLayout>
  )
}