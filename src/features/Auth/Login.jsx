import { Alert, Box, Button, Paper, TextField, Typography } from "@mui/material";
import { grey } from "@mui/material/colors";
import { useState } from "react";
import LoadingButton from '@mui/lab/LoadingButton';
import { useDispatch, useSelector } from "react-redux";
import { authActions, getError, getErrors, getLoading, getUser } from "../../services/app/Auth";
import { login as loginApi } from "../../services/api/Auth/api";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

export default function Login(props) {
  const [email,setEmail] = useState('admin@gmail.com')
  const [password,setPassword] = useState('Gerbangbiru01!')
  const dispatch = useDispatch()
  const user = useSelector(getUser)
  const loading = useSelector(getLoading)
  const errors = useSelector(getErrors)
  const error = useSelector(getError)
  const navigator = useNavigate()

  const _handleLogin = () => {
    dispatch(authActions.clearError())
    dispatch(loginApi({email,password}))
  }

  if(user) {
    // navigator('/')
  }

  return (
    <Box sx={{backgroundColor:grey[300],width: '100vw', height: '100vh', display: 'flex', alignItems:'center',justifyContent: 'center'}}>
      <Paper sx={{width:300,px:4,py:3}}>
        <Typography variant={'subtitle2'} sx={{mb:1}}>UPSCALE LOGIN</Typography>
        <TextField 
        id="standard-email" 
        value={email}
        type={'email'}
        onChange={(e) => setEmail(e.target.value)} 
        label="Email" variant="standard" sx={{width:'100%',mb:2}} />
        <TextField 
        id="standard-password" type={'password'} 
        onChange={(e) => setPassword(e.target.value)} 
        value={password} 
        label="Password" 
        variant="standard" sx={{width:'100%',mb:3}} />
        {
          errors?.length > 0 &&
          errors.map((item) => <Alert key={item} severity="error" sx={{mb:2}}>{item}</Alert>)
        }
        {
          error && 
          <Alert severity="error" sx={{mb:2}}>{error}</Alert>
        }
        <LoadingButton loading={loading} variant="contained" sx={{width:'100%'}} onClick={_handleLogin}>
          Login
        </LoadingButton>
      </Paper>
    </Box>
  )
}