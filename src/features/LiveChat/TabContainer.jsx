import * as React from 'react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { useTheme } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import RequestedChat from './RequestedChat';
import AllChat from './AllChat';
import { Badge, Divider, InputBase } from '@mui/material';
import { grey } from '@mui/material/colors';
import ActiveChat from './ActiveChat';


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ py: 2 }}>
          {children}
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

export default function TabContainer() {
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  return (
    <Box sx={{ bgcolor: 'background.paper', flex:1,width:'100%'}}>
      <AppBar position="static" color='transparent' elevation={0}>
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label={
            <Badge component={'span'} sx={{
              '& .MuiBadge-badge': {
                right: 0,
                bottom: 0,
                transform: 'translate(90%,-50%)'
              }
            }} badgeContent={100} color="secondary">
              <span>All</span>
            </Badge>
          } {...a11yProps(0)} />
          <Tab label={
            <Badge component={'span'} sx={{
              '& .MuiBadge-badge': {
                right: 0,
                bottom: 0,
                transform: 'translate(90%,-50%)'
              }
            }} badgeContent={100} color="secondary">
              <span>Request</span>
            </Badge>
          } {...a11yProps(1)} />
          <Tab label={
            <Badge component={'span'} sx={{
              '& .MuiBadge-badge': {
                right: 0,
                bottom: 0,
                transform: 'translate(90%,-50%)'
              }
            }} badgeContent={4} color="secondary">
              <span>Active</span>
            </Badge>
          } {...a11yProps(2)} />  
        </Tabs>
      </AppBar>
      <Divider variant="fullWidth" />
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
          <Box sx={{py:0, px:2}}>
            <InputBase
              sx={{ px: 1, width:'100%',cursor: 'pointer', border: '1px solid '+grey[300], borderRadius: '4px'}}
              defaultValue={''}
              placeholder={'Search'}
            />
          </Box>
          <Box sx={{height: '100vh',maxHeight: '62vh',overflowY:'auto'}}>
            <Box>
              <AllChat />
            </Box>
          </Box>
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
          <Box sx={{py:0, px:2}}>
            <InputBase
              sx={{ px: 1, width:'100%',cursor: 'pointer', border: '1px solid '+grey[300], borderRadius: '4px'}}
              defaultValue={''}
              placeholder={'Search'}
            />
          </Box>
          <Box sx={{height: '100vh',maxHeight: '62vh',overflowY:'auto'}}>
            <Box>
              <RequestedChat />
            </Box>
          </Box>
        </TabPanel>
        <TabPanel value={value} index={2} dir={theme.direction}>
          <Box sx={{py:0, px:2}}>
            <InputBase
              sx={{ px: 1, width:'100%',cursor: 'pointer', border: '1px solid '+grey[300], borderRadius: '4px'}}
              defaultValue={''}
              placeholder={'Search'}
            />
          </Box>
          <Box sx={{height: '100vh',maxHeight: '62vh',overflowY:'auto'}}>
            <Box>
              <ActiveChat />
            </Box>
          </Box>
        </TabPanel>
      </SwipeableViews>
    </Box>
  );
}
