import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import { Box, Button, Chip, IconButton, ListSubheader, Stack } from '@mui/material';
import FilterListIcon from '@mui/icons-material/FilterList';
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import FilterAltOutlinedIcon from '@mui/icons-material/FilterAltOutlined';
import { memo } from 'react';

function AllChat() {
  
  return (
    <List sx={{ width: '100%', bgcolor: 'background.paper' }} 
    subheader={
      <ListSubheader sx={{display:'flex',alignItems:'center',justifyContent:'space-between'}}>
        <div>All Messages</div>
        <Stack direction="row" spacing={1}>
          <IconButton size='small'>
            <FilterAltOutlinedIcon fontSize='small' />
          </IconButton>
        </Stack>
      </ListSubheader>
    }
    dense>
      {
        [...Array(100).keys()].map((item) => (
          <Box key={item}>
            <ListItem alignItems="flex-start">
              <ListItemAvatar>
                <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
              </ListItemAvatar>
              <ListItemText
                primary="Brunch this weekend?"
                secondary={
                  <React.Fragment>
                    <Typography
                      sx={{ display: 'inline' }}
                      component="span"
                      variant="body2"
                      color="text.primary"
                    >
                      Ali Connors
                    </Typography>
                    {" — I'll be in your neighborhood doing errands this…"}
                  </React.Fragment>
                }
              />
            </ListItem>
            <Divider variant="inset" component="li" />
          </Box>
        ))
      }
    </List>
  )
}

export default memo(AllChat)