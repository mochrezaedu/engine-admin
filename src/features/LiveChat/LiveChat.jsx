import { Avatar, Badge, Box, Button, Divider, IconButton, InputBase, List, ListItem, ListItemAvatar, ListItemButton, ListItemIcon, ListItemText, Paper, Stack, Toolbar, Tooltip } from "@mui/material"
import { grey } from "@mui/material/colors"
import LayoutBar from "../Layout/LayoutBar"
import Settings from '@mui/icons-material/Settings';
import ArrowRight from '@mui/icons-material/ArrowRight';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import TabContainer from "./TabContainer";
import ChatContainer from "./ChatContainer";

function Layout(props) {

  return (
    <div></div>
  )
}

export default function LiveChat(props) {
  
  return (
    <Box>
      <LayoutBar title={'Live Chat'} full={true} />
      <Box sx={{ display: 'flex',maxHeight:'100vh'}}>
        <Paper variant="outlined" sx={{width: '35%', mt:8,borderRadius: '0px'}}>
          <List sx={{borderRadius:'0px'}}>
            <ListItem component="div" disablePadding>
              <ListItemButton>
                <ListItemText
                  primary="Recent Chats"
                  primaryTypographyProps={{
                    color: 'primary',
                    fontWeight: 'bold',
                    variant: 'body2',
                  }}
                />
              </ListItemButton>
              <Stack direction={'row'} spacing={0} sx={{mr:1}}>
                <Tooltip title="Settings">
                  <IconButton
                    sx={{
                      '& svg': {
                        transition: '0.2s',
                        transform: 'translateX(0) rotate(0)',
                      },
                      '&:hover, &:focus': {
                        bgcolor: 'unset',
                        '& svg:first-of-type': {
                          transform: 'translateX(-4px) rotate(-20deg)',
                        },
                        '& svg:last-of-type': {
                          right: 0,
                          opacity: 1,
                        },
                      },
                    }}
                  >
                    <Settings />
                    <ArrowRight sx={{ position: 'absolute', right: 4, opacity: 0 }} />
                  </IconButton>
                </Tooltip>
                <IconButton>
                  <ArrowBackIcon />
                </IconButton>
              </Stack>
            </ListItem>
          </List>
          <Divider />
          <TabContainer />
        </Paper>
        <Box sx={{flex:1,bgcolor:grey[50],mt:8,position: 'relative' }}>
          <ChatContainer />
        </Box>
      </Box>
    </Box>
  )
}