import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import { Box, Button, Chip, IconButton, ListSubheader, Stack } from '@mui/material';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import { green, grey, red } from '@mui/material/colors';
import { memo } from 'react';
import FilterAltOutlinedIcon from '@mui/icons-material/FilterAltOutlined';

function RequestedChat(props) {
  
  return (
    <List sx={{ width: '100%', bgcolor: 'background.paper' }} 
    subheader={
      <ListSubheader sx={{display:'flex',alignItems:'center',justifyContent:'space-between'}}>
        <div>Request Chat</div>
        <Stack direction="row" spacing={1}>
          <IconButton size='small'>
            <FilterAltOutlinedIcon fontSize='small' />
          </IconButton>
        </Stack>
      </ListSubheader>
    }
    dense>
      {
        [...Array(10).keys()].map((item) => (
          <Box key={item}>
            <ListItem alignItems="flex-start">
              <ListItemAvatar>
                <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
              </ListItemAvatar>
              <ListItemText
                primary="Brunch this weekend?"
                secondary={
                  <React.Fragment>
                    <Typography
                      sx={{ display: 'inline' }}
                      component="span"
                      variant="body2"
                      color="text.primary"
                    >
                      Ali Connors
                    </Typography>
                    {" — I'll be in your neighborhood doing errands this…"}
                  </React.Fragment>
                }
              />
              <Stack direction={'column'} spacing={1} justifyContent={'center'} alignItems={'center'}>
                <Button size={'small'} variant={'outlined'} sx={{
                  color:green[700],
                  borderColor:green[700],
                  '&:hover': {
                    borderColor:green[700],
                    color:green[700],
                    bgcolor:green[100]
                  },
                  '&:focus': {
                    borderColor:green[700],
                    color:green[700]
                  },
                }}>
                  Claim
                </Button>
                <Button size={'small'} variant={'outlined'} sx={{
                  color:red[700],
                  borderColor:red[700],
                  '&:hover': {
                    borderColor:red[700],
                    color:red[700],
                    bgcolor:red[100]
                  },
                  '&:focus': {
                    borderColor:red[700],
                    color:red[700]
                  },
                }}>
                  Skip
                </Button>
                {/* <IconButton sx={{
                  bgcolor:red[700],
                  color:'white',
                  '&:hover': {
                    bgcolor:red[400],
                    color:'white',
                  },
                  '&:focus': {
                    bgcolor:red[400],
                    color:'white',
                  },
                }} size={'small'} color={'error'}>
                  <CloseIcon fontSize={'small'} />
                </IconButton> */}
              </Stack>
            </ListItem>
            <Divider variant="inset" component="li" />
          </Box>
        ))
      }
    </List>
  )
}

export default memo(RequestedChat)