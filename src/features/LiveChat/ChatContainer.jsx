import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Checkbox from '@mui/material/Checkbox';
import Avatar from '@mui/material/Avatar';
import { Box } from '@mui/system';
import { Chip, Divider, IconButton, Paper, Stack, Typography } from '@mui/material';
import ChatInput from './ChatInput';
import ChatItemLeft from './ChatItemLeft';
import ChatItemRight from './ChatItemRight';
import StarBorderIcon from '@mui/icons-material/StarBorder';

export default function ChatContainer() {
  const [checked, setChecked] = React.useState([1]);

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  return (
    <Box>
      <List dense sx={{ width: '100%' }} disablePadding>
        <ListItem
          sx={{py:0}}
          secondaryAction={
            <Stack direction={'row'} spacing={1} alignItems={'center'}>
              <Chip label="Connected" color="success" />
              <IconButton title='Star Message'>
                <StarBorderIcon />
              </IconButton>
            </Stack>
          }
          disablePadding
        >
          <ListItemButton sx={{cursor:'default'}}>
            <ListItemAvatar>
              <Avatar
                sx={{ width: 42, height: 42 }}
                alt={`Avatar n°${1 + 1}`}
                src={`/static/images/avatar/${1+1}.jpg`}
              />
            </ListItemAvatar>
            <ListItemText>
              <Typography variant='subtitle1'>Mizuno Ahasi</Typography>
              <Typography variant='caption'>Online</Typography>
            </ListItemText>
          </ListItemButton>
        </ListItem>
        <Divider />
      </List>
      <Paper elevation={1} variant={'elevation'} sx={{bgcolor:'white',maxHeight:'69vh',overflowY:'auto',borderBottomLeftRadius:'12px',borderBottomRightRadius:'12px'}}>
        <Box sx={{display:'flex',flexDirection:'column-reverse',minHeight:'69vh'}}>
          {
            [...Array(3).keys()].map((item) => (
              <ChatItemLeft key={item} />
            ))
          }
          {
            [...Array(3).keys()].map((item) => (
              <ChatItemRight key={item} />
            ))
          }
          {
            [...Array(30).keys()].map((item) => (
              <ChatItemRight key={item} />
            ))
          }
        </Box>
      </Paper>
      <ChatInput />
    </Box>
  );
}
