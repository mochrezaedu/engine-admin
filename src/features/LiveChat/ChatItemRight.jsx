import { Box } from "@mui/material";
import { grey } from "@mui/material/colors";

export default function ChatItemRight(props) {
  
  return (
    <Box sx={{px:2,py:1,display:'flex',justifyContent:'flex-end'}}>
      <Box sx={{border: `1px solid ${grey[300]}`,px:2,py:1,borderRadius:'5000px'}}>
        {
          'Lorem ipsum dolor amet. RIGHT'
        }
      </Box>
    </Box>
  )
}