import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Checkbox from '@mui/material/Checkbox';
import Avatar from '@mui/material/Avatar';
import { Box } from '@mui/system';
import { Divider, IconButton, InputBase, Typography } from '@mui/material';
import { grey } from '@mui/material/colors';
import SendIcon from '@mui/icons-material/Send';

export default function ChatInput() {
  const [checked, setChecked] = React.useState([1]);

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  return (
    <>
      <Box sx={{ width: '100%', position: 'absolute', bottom: 0, left: 0 }}>
        <Box sx={{py:2,px:2}}>
          <ListItem
            sx={{py:0}}
            secondaryAction={
              <Box></Box>
            }
            disablePadding
          >
            <InputBase
              sx={{ bgcolor: 'white', fontSize: '14px',px: 2, width:'100%',cursor: 'pointer', border: '1px solid '+grey[300], borderRadius: '999px'}}
              defaultValue={''}
              placeholder={'Tulis Balasan'}
            />
            <IconButton color='primary'>
              <SendIcon />
            </IconButton>
          </ListItem>
        </Box>
      </Box>
    </>
  );
}
