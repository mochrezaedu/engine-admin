import { Box, Button, MenuItem, Portal, Select, Typography } from "@mui/material";
import { useState } from "react";
import { useDispatch,useSelector } from "react-redux";
import BasicModal from "../../components/BasicModal";
import InputText from "../../components/InputText";
import { Uniqid } from "../../helpers/helper";
import { modalActions } from "../../services/app/modal/ModalSlicer";
import { ApplicationActions } from "../../services/datacenter/application";
import ApplicationApi from '../../services/api/Application/api'
import { getUser } from '../../services/app/Auth';
import { BotAccountGetter } from "../../services/datacenter/bot_account";

export default function UpdateBot(props) {
  const {forwardItem} = props
  const dispatch = useDispatch()
  const user = useSelector(getUser)
  const bots = useSelector((state) => BotAccountGetter.get(state,(item) => item.user_id == user.id))
  const allBots = useSelector(BotAccountGetter.all)
  const [bot,setBot] = useState(forwardItem.bot_user_id || '-- Select Bot --')
  
  const _handleSubmit = (args) => {
    dispatch(modalActions.closeModal())

    const payload = {...forwardItem}
    payload['bot_user_id'] = bot

    dispatch(ApplicationActions.update(payload))
    dispatch(ApplicationApi.update({
      data: {application: payload}
    }))
  }

  return (
    <Portal>
      <BasicModal titleAction={
        <Button variant={'contained'} size={'small'} color={'success'} onClick={_handleSubmit}>Simpan</Button>
      }>
        <Box>
          <Box sx={{mb:2}}>
            <Typography variant={'subtitle2'} sx={{mb:1}}>Use For {forwardItem.name}*</Typography>
            <Select
              labelId="demo-customized-select-label"
              sx={{ px:2,flex: 1,backgroundColor:'white', borderRadius: '4px',mb:2,flex:1,width:'100%',fontSize: 14}}
              inputProps={{ 'aria-label': 'Without label' }}
              id="demo-customized-select"
              input={<InputText placeholder={'Select Type'} />}
              onChange={(e) => setBot(e.target.value)}
              value={bot}
            >
              <MenuItem value={`-- Select Bot --`} selected={true} disabled>
                <em>-- Select Type --</em>
              </MenuItem>
              {
                bots?.map((item) => {
                  return (
                    <MenuItem key={item.id} value={item.id} selected={Boolean(item.id === bot)}>{item.name}</MenuItem>
                  )
                })
              }
            </Select>
          </Box>
        </Box>
      </BasicModal>
    </Portal>
  )
}

const options = [
  'Web Application',
  'Mobile Application',
  'Social Media',
]