import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  confirmation: false,
  data: null
}

const methods = {
  openConfirmation: (state,action) => {
    const {payload} = action
    state.confirmation = payload
  },
  closeConfirmation: (state,action) => {
    const {payload} = action
    state.confirmation = payload
  },
}

export const Application = createSlice({
  name: 'application',
  initialState: initialState,
  reducers: methods
})

export default Application

export const applicationActions = Application.actions
export const applicationState = {
  getConfirmation: (state) => state.engine.confirmation
}