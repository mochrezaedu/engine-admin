import { Box, Button, MenuItem, Portal, Select, Typography } from "@mui/material";
import { useState } from "react";
import { useDispatch,useSelector } from "react-redux";
import BasicModal from "../../components/BasicModal";
import InputText from "../../components/InputText";
import { Uniqid } from "../../helpers/helper";
import { modalActions } from "../../services/app/modal/ModalSlicer";
import { ApplicationActions } from "../../services/datacenter/application";
import ApplicationApi from '../../services/api/Application/api'
import { getUser } from '../../services/app/Auth';

export default function UpdateApplication(props) {
  const {forwardItem} = props
  const dispatch = useDispatch()
  const user = useSelector(getUser)
  const [name,setName] = useState(forwardItem.name || '')
  const [type,setType] = useState(forwardItem.type || '-- Select Type --')
  
  const _handleSubmit = (args) => {
    dispatch(modalActions.closeModal())

    const payload = {...forwardItem}
    payload['name'] = name
    payload['type'] = type

    dispatch(ApplicationActions.update(payload))
    dispatch(ApplicationApi.update({
      data: {application: payload}
    }))
  }

  return (
    <Portal>
      <BasicModal title={'Update '+forwardItem.name} titleAction={
        <Button variant={'contained'} size={'small'} color={'success'} onClick={_handleSubmit}>Simpan</Button>
      }>
        <Box>
          <Box sx={{mb:2}}>
            <Typography variant={'subtitle2'} sx={{mb:1}}>Application Name*</Typography>
            <InputText placeholder={'Application Name'} value={name} onChange={(e) => setName(e.target.value)} />
          </Box>
          <Box sx={{mb:2}}>
            <Typography variant={'subtitle2'} sx={{mb:1}}>Project For*</Typography>
            <Select
              labelId="demo-customized-select-label"
              sx={{ px:2,flex: 1,backgroundColor:'white', borderRadius: '4px',mb:2,flex:1,width:'100%',fontSize: 14}}
              inputProps={{ 'aria-label': 'Without label' }}
              id="demo-customized-select"
              input={<InputText placeholder={'Select Type'} />}
              onChange={(e) => setType(e.target.value)}
              value={type}
            >
              <MenuItem value={`-- Select Type --`} selected={true}>
                <em>-- Select Type --</em>
              </MenuItem>
              {
                options?.map((item) => {
                  return (
                    <MenuItem key={item} value={item} selected={Boolean(item === type)}>{item}</MenuItem>
                  )
                })
              }
            </Select>
          </Box>
        </Box>
      </BasicModal>
    </Portal>
  )
}

const options = [
  'Web Application',
  'Mobile Application',
  'Social Media',
]