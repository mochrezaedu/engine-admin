import { Box, Button, IconButton, TableCell, TableRow } from "@mui/material"
import { useDispatch } from "react-redux"
import { Link } from "react-router-dom"
import ApplicationApi from "../../services/api/Application/api"
import { ApplicationActions } from "../../services/datacenter/application"
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import DeleteIcon from '@mui/icons-material/Delete';
import { flowGroupGetter } from "../../services/datacenter/flow_group"
import { useSelector } from "react-redux"
import { modalActions,get as getModal } from "../../services/app/modal/ModalSlicer"
import UpdateBot from "./UpdateBot"
import { BotAccountGetter } from "../../services/datacenter/bot_account"
import CreateIcon from '@mui/icons-material/Create';
import { grey } from "@mui/material/colors"
import UpdateApplication from "./UpdateApplication"


export default function ApplicationItem(props) {
  const {forwardItem} = props
  const dispatch = useDispatch()

  const flowGroups = useSelector(flowGroupGetter.all)
  const activeModal = useSelector((state) => getModal(state,'name'))
  const botUser = useSelector((state) => BotAccountGetter.get(state, (item) => item.id == forwardItem.bot_user_id)).first()


  const _handleUpdateApplication = () => {
    dispatch(modalActions.openModal({
      activate: true,
      title: '',
      name: 'update-application',
      loading: false
    }))
  }

  const _handleDeleteApplication = (args) => {
    dispatch(ApplicationActions.openConfirmation())
  }
  const _handleConfirmDeleteApplication = (args) => {
    dispatch(ApplicationActions.delete([args.id]))
    dispatch(ApplicationApi.destroy({
      data: {application_id: args.id}
    }))
  }

  const _handleOpenUpdateBot = () => {
    dispatch(modalActions.openModal({
      activate: true,
      title: 'Add Bot',
      name: 'update-bot-application',
      loading: false
    }))
  }

  const _getRoute = (args) => {
    const flowGroupId = flowGroups.find((item) => item.application_id == args.id)
    return `/application/${args.id}/g/${flowGroupId?.id}/flow`
  }

  return (
    <TableRow
      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
    >
      <TableCell component="th" scope="row">
        <Link to={`/application/${forwardItem.id}/flow`}>{forwardItem.name}</Link>
      </TableCell>
      <TableCell>{forwardItem.type}</TableCell>
      <TableCell>
        {
          !forwardItem.bot_user_id &&
          <Button onClick={_handleOpenUpdateBot}>Set Bot</Button>
        }
        {
          forwardItem.bot_user_id &&
          <Box>
            {botUser?.name}
            <IconButton onClick={_handleOpenUpdateBot} size={'small'} variant={'contained'} sx={{ml:1,backgroundColor: grey[300]}}>
              <CreateIcon fontSize={'small'} />
            </IconButton>
          </Box>
        }
        {
          activeModal === 'update-bot-application' &&
          <UpdateBot forwardItem={forwardItem} />
        }
      </TableCell>
      <TableCell>{forwardItem.owner_name}</TableCell>
      <TableCell align="right">
        <Link to={_getRoute(forwardItem)} title={'Open App'}><IconButton size={'small'}><PlayArrowIcon fontSize={'small'} /></IconButton></Link>
        <IconButton size={'small'} onClick={_handleUpdateApplication}><ModeEditIcon fontSize={'small'} /></IconButton>
        <IconButton size={'small'} onClick={() => _handleDeleteApplication(forwardItem)}><DeleteIcon fontSize={'small'} /></IconButton>
      
        {
          activeModal === 'update-application' &&
          <UpdateApplication forwardItem={forwardItem} />
        }
      </TableCell>
    </TableRow>

  )
}