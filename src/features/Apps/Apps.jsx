import { Box,Paper, Button, List, ListItem, ListItemButton, ListItemText, Portal, Typography, IconButton } from "@mui/material";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import BasicModal from "../../components/BasicModal";
import { modalActions, get as getModal } from "../../services/app/modal/ModalSlicer";
import CreateApplication from "./CreateApplication";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { Link, useNavigate } from "react-router-dom";
import { ApplicationActions, ApplicationGetter } from "../../services/datacenter/application";
import { useCallback } from "react";
import { useEffect } from "react";
import ApplicationApi from "../../services/api/Application/api";
import BaseLayout from "../Layout/BaseLayout";
import { flowGroupGetter } from "../../services/datacenter/flow_group";
import { getFlowGroups } from "../../services/api/Resources";
import ApplicationItem from "./AplicationItem";
import UpdateBot from "./UpdateBot";
import { BotAccountGetter } from "../../services/datacenter/bot_account";
import BotAccountApi from "../../services/api/BotAccount/api";
import UpdateApplication from "./UpdateApplication";

export default function Apps(props) {
  const dispatch = useDispatch()
  const activeModal = useSelector((state) => getModal(state,'name'))
  
  const applications = useSelector(ApplicationGetter.all)
  const flowGroups = useSelector(flowGroupGetter.all)
  const bots = useSelector(BotAccountGetter.all)

  const navigator = useNavigate()
  const _navigate = useCallback((args) => navigator(args, {replace:true}), [navigator])

  const _handleOpenCreateApplication = () => {
    dispatch(modalActions.openModal({
      activate: true,
      title: '',
      name: 'create-application',
      loading: false
    }))
  }

  useEffect(() => {
    if(applications.length === 0) {
      dispatch(ApplicationApi.all())
      if(flowGroups.length === 0) {
        dispatch(getFlowGroups())
      }
    }
    if(bots.length == 0) {
      dispatch(BotAccountApi.all())
    }
  }, [])

  return (
    <BaseLayout>
      <Box>
        <Box sx={{mb:2,display: 'flex', alignItems:'center', justifyContent:'space-between'}}>
          <Typography variant={'subtitle2'}>Manage Applications</Typography>
          <Button onClick={_handleOpenCreateApplication} variant={'outlined'} size={'small'}>Create Application</Button>
        </Box>
        <Box>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell>Type</TableCell>
                  <TableCell>Bot</TableCell>
                  <TableCell>Owner</TableCell>
                  <TableCell></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {applications.map((row) => <ApplicationItem key={row.id} forwardItem={row} />)}
                <TableRow>
                {
                  applications.length === 0 &&
                  <TableCell align="center" colSpan={3}>No Application Exist</TableCell>
                }
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
        {
          activeModal === 'create-application' &&
          <CreateApplication />
        }
      </Box>
    </BaseLayout>
  )
}
function createData(name, owner) {
  return { name, owner };
}
const rows = [
  createData('Frozen yoghurt', 'Admin'),
  createData('Ice cream sandwich', 'Admin')
];