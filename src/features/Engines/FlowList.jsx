import { useEffect, useMemo, useState } from 'react';
import FLowItem from './FlowItem';
import { Droppable } from 'react-beautiful-dnd';
import { useDispatch, useSelector } from 'react-redux';
import { FlowModelGetter, Getter } from '../../services/datacenter/flow';
import { Alert, Box, Divider, ListItem, ListItemButton, ListItemText } from '@mui/material';
import {actions} from '../../services/datacenter/flow'
import {actions as inputActions, get as getInput} from '../../services/app/input/InputSlicer'
import EditableText from '../../components/EditableText';
import FlowApi from '../../services/api/Flow/api';
import { Uniqid } from '../../helpers/helper';
import { modalActions } from '../../services/app/modal/ModalSlicer';
import { useNavigate, useParams } from 'react-router-dom';

export default function FlowList(props) {
  const {forwardGroup} = props;
  const {applicationId} = useParams()
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [enableAnimation,setEnableAnimation] = useState(false);
  const pendingIds = useSelector(Getter.getPendingIds)
  const items = useSelector((state) => Getter.get(state, (item) => item.flow_group_id === forwardGroup.id));
  const pendingItem = useMemo(() => items.find((item) => item.id === pendingIds.first()?.id), [pendingIds])
  const inputName = useSelector((state) => getInput(state, 'activate'))
  const _getLastFlow = () => {
    if(items.length > 0) {
      return items.sort((i,j) => (i.order - j.order))[items.length -1]
    } 
    return null
  }
  const flowMessage = useSelector(FlowModelGetter.getMessage)

  const _getLastFlowOrder = () => {
    if(_getLastFlow()) {
      return _getLastFlow().order+1
    }
    return 0
  }

  const _handleSubmitNewFlow = (args) => {
    dispatch(inputActions.closeInput())
    if(args) {
      const createdItem = {id: Uniqid(),name: args, response: args, flow_group_id: forwardGroup.id, order: _getLastFlowOrder()}
      // dispatch(actions.createEmpty(createdItem))
      dispatch(FlowApi.store({
        data: createdItem
      })).then((_res) => {
        const currentId = _res.payload.item.id
        navigate(`/application/${applicationId}/flow/${currentId}/detail`)
      })
    }
  }

  useEffect(() => {
    if(forwardGroup.flow_masters) {
      dispatch(actions.createMany(forwardGroup.flow_masters));
    }
  }, [forwardGroup,forwardGroup.id])

  useEffect(() => {
    if(items.length === 0) {
      dispatch(FlowApi.all())
    }
    const animation = requestAnimationFrame(() => setEnableAnimation(true));
    return () => {
      cancelAnimationFrame(animation);
      setEnableAnimation(false);
    }
  }, []);

  if(!enableAnimation) {
    return null;
  }

  return (
    <>
      <Divider />
      <Droppable isCombineEnabled={false} direction={'vertical'} droppableId={'flow-group-'+forwardGroup.id.toString()} >
        {
          provided => {
            return (
              <div ref={provided.innerRef} {...provided.droppableProps}>
                {
                  items.length > 0 && items.sort((i,j) => (i.order - j.order)).map((item,index) => {
                    return (
                      <Box key={item.id}>
                        <FLowItem forwardItem={item} forwardIndex={index}  />
                      </Box>
                    );
                  })
                }
                {provided.placeholder}
              </div>
            );
          }
        }
      </Droppable>
      {
        Boolean(inputName !== 'add-new-flow-'+forwardGroup.id && items.length == 0) && 
        <ListItem disablePadding>
          <ListItemButton>
            <ListItemText primary="No Data Found" />
          </ListItemButton>
        </ListItem>
      }
      {
        Boolean(inputName === 'add-new-flow-'+forwardGroup.id) &&
        <Box sx={{mb:2, px: 2}}>
          <EditableText onClose={() => dispatch(inputActions.closeInput())} onSubmit={_handleSubmitNewFlow} placeholder={'Flow Name'} />
        </Box>
      }
      {
        flowMessage.type === 'error' &&
        <Alert severity="error">{flowMessage.text}!</Alert>
      }
    </>
  );
}


