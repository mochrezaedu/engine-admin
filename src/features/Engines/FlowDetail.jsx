import { Alert, Box, Button, FormControlLabel, IconButton, Paper, Switch, Tooltip, Typography } from "@mui/material";
import { blue, grey, indigo } from "@mui/material/colors";
import { useDispatch, useSelector } from "react-redux";
import {Getter,actions as flowActions, FlowModelGetter, path, FlowModelActions} from '../../services/datacenter/flow'
import {Getter as flowChoiceGetter,actions as flowChoiceAction} from '../../services/datacenter/flow_choices'
import {get as getInput} from '../../services/app/input/InputSlicer'
import { useEffect, useState } from "react";
import EditableText from "../../components/EditableText";
import {actions as inputActions} from '../../services/app/input/InputSlicer'
import Collection from "../../helpers/Collection";
import EditableSelect from "../../components/EditableSelect";
import EditableSelectWithSearch from "../../components/EditableSelectWithSearch";
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import DragHandleIcon from '@mui/icons-material/DragHandle';
import { Draggable, Droppable } from "react-beautiful-dnd";
import { Stack } from "@mui/system";
import { Uniqid } from "../../helpers/helper";
import WikiDropdown from "./FlowDetail/WikiDropdown";
import FlowApi from "../../services/api/Flow/api";
import FlowChoice from "../../services/api/FlowChoice/api";
import Extra from "./FlowDetail/Extra";
import { FlowTypeGetter } from "../../services/datacenter/flow_type";
import FlowTypeApi from "../../services/api/FlowType/api";
import TextImage, { TextImagePreview } from "./FlowDetail/TextImage";
import TextImageEditorList from "./FlowDetail/TextImageEditorList";
import { useNavigate, useParams } from "react-router-dom";
import { BeatLoader, ClipLoader } from "react-spinners";
import FlowType from "./FlowDetail/FlowType";
import { rootLoaderActions } from "../../services/app/loader/RootLoaderSlice";
import { getStoreState } from "../../helpers/Store";
import DndProvider from "../../components/DndProvider";
import { useCallback } from "react";
import { store } from "../../app/store";
import { alertActions, getAlertText } from "../../services/app/alert/AlertSlicer";
import EngineLayout from "../Layout/EngineLayout";

export const RenderChoiceItem = (props) => {
  const {forwardFlow, forwardItem, spreadNext, nextable} = props
  const dispatch = useDispatch()
  const editMode = useSelector((state) => getInput(state,'activate'))
  const prefix = forwardItem?.name
  const options = useSelector((state) => Getter.get(state,(item) => {
    const _g = true
    const _wM = Boolean(item.id != forwardFlow.id)
    return _g && _wM
  }))
  const nextItem = useSelector((state) => Getter.get(state, (item) => item.id == forwardItem?.next)).first();
  const flow_choices = useSelector((state) => flowChoiceGetter.get(state, (item) => item.flow_id === forwardFlow.id))
  const _handleEditMode = (args) => {
    editMode !== prefix+args.name && 
    dispatch(inputActions.openInput({activate:prefix+args.name,key: args.key,value:args.value || ''}))
  }
  const _handleSubmitEditableNext = (args) => {
    dispatch(inputActions.closeInput())
    if(args?.id) {
      var payload = flow_choices.find((item) => item.id === args.choice.id)
      payload = {...payload, next: args.id}
      dispatch(flowChoiceAction.update(payload))
      dispatch(FlowChoice.updateAttribute({
        data: {flow_choice: payload}
      }))
    }
  }
  const _handleEditChoice = (args) => {
    const {value,item} = args
    const payload = {...item, text: value}
    dispatch(inputActions.closeInput())
    dispatch(flowChoiceAction.update(payload))
    dispatch(FlowChoice.updateAttribute({
      data: {flow_choice: payload}
    }))
  }
  const _handleDeleteItem = () => {
    dispatch(flowChoiceAction.delete([forwardItem?.id]))
    dispatch(FlowChoice.destroy({
      data: {flow_choice_id: forwardItem?.id}
    }))
  }

  return (
    <Draggable isDragDisabled={false} draggableId={'flow-'+forwardFlow.id+'-choices-'+forwardItem?.id.toString()} index={forwardItem?.order}>
      {
        (provided,snapshot) => {
          const {draggableProps,dragHandleProps,innerRef, ...rest} = provided
          const {style, ...restProps} = draggableProps
          return (
            <>
              <Paper ref={innerRef} {...restProps}
              variant={'outlined'} elevation={0} 
              sx={{px:2,py:1,mb:1,cursor: 'pointer', ...style}}>
                {
                  <>
                    <Box>
                      <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start', flexGrow: 1}}>
                          <IconButton  
                          {...dragHandleProps}
                          aria-label="drag handle" size={'small'} sx={{mr:1}}>
                            <DragHandleIcon fontSize={'small'} />
                          </IconButton>
                          <Box sx={{width: '100%'}}>
                            {
                              Boolean(editMode === prefix+'-edit-'+forwardFlow.id+'-choice-'+forwardItem?.id) ?
                              <EditableText onClose={() => dispatch(inputActions.closeInput())} onSubmit={(e) => _handleEditChoice({value: e, item: forwardItem})} placeholder={'Edit Choice'} />:
                              <Typography variant={'subtitle1'} sx={{fontWeight: 700}} onClick={() => _handleEditMode({name: '-edit-'+forwardFlow.id+'-choice-'+forwardItem?.id,value: forwardItem?.text})}>
                                {forwardItem?.text}
                              </Typography>
                            }
                          </Box>
                        </Box>
                        <Button aria-label="opentab" onClick={_handleDeleteItem} sx={{color: grey[700], minWidth: 'auto', ml: 1}}>
                          <DeleteOutlineIcon fontSize={'small'} />
                        </Button>
                      </Box>
                      {
                        Boolean(spreadNext && nextable) &&
                        <Paper elevation={0} variant={'outlined'} 
                        sx={{backgroundColor:grey[200], px:2,py:1,cursor:'pointer',mb:2,mt:1}}
                        onClick={() => _handleEditMode({name:'-choices-'+forwardItem?.id+'next',value:forwardItem?.next,key:'-choices-'+forwardItem?.id+'next'})}>
                          <Typography variant={'caption'} sx={{mb: editMode === '-choices-'+forwardItem?.id+'next' ? 1:0}}>Next</Typography>
                          {
                            editMode === prefix+'-choices-'+forwardItem?.id+'next' ? 
                            <EditableSelectWithSearch keyMatches={['id', 'name', 'response']} 
                            onSubmit={(e) => _handleSubmitEditableNext({...e, choice: forwardItem})} 
                            onClose={() => dispatch(inputActions.closeInput())} 
                            description={(item) => item.response}
                            options={options} />
                            :<Typography variant={'subtitle2'}>{nextItem?.name || <span style={{color: 'red'}}>{'Required'}</span>}</Typography>
                          }
                        </Paper>
                      }
                    </Box>
                  </>
                }
              </Paper>
              {provided.placeholder}
            </>
          )
        }
      }
    </Draggable>
  )
}

export const RenderChoice = (props) => {
  const {forwardItem, nextable} = props;
  const dispatch = useDispatch()
  const editMode = useSelector((state) => getInput(state,'activate'));
  const prefix = forwardItem?.name
  const choices = useSelector((state) => flowChoiceGetter.get(state, (item) => item?.flow_id == forwardItem?.id));

  const _handleSubmitChoice = (args) => {
    dispatch(inputActions.closeInput())
    if(args) {
      const payload = {text: args, flow_master_id: forwardItem?.id, flow_id: forwardItem?.id, order: choices.length, id: Uniqid()}
      dispatch(flowChoiceAction.createEmpty(payload))
      dispatch(FlowChoice.store({
        data: payload
      }))
    }
  }
  const _handleEditMode = (args) => {
    editMode !== prefix+args.name && 
    dispatch(inputActions.openInput({activate:prefix+args.name,key: args.key,value:args.value || ''}))
  }
  const _handleToggleSpreadNext = (args) => {
    var payload = null;
    if(!args.target.checked) {
      var existingChoices = choices.map((item) => {
        return {...item, next: null}
      })
      dispatch(flowChoiceAction.updateMany([...existingChoices]))
      payload = {...forwardItem, next_on: null}
      dispatch(flowActions.update(payload))
    } else {
      if(Boolean(forwardItem?.next)) {
        var existingChoices = choices.map((item) => {
          return {...item, next: null}
        })
        dispatch(flowChoiceAction.updateMany([...existingChoices]))
      }
      payload = {...forwardItem, next_on: 'flow_choices'}
      dispatch(flowActions.update({...forwardItem, next_on: 'flow_choices'}))
    }

    dispatch(FlowApi.updateSingleAttribute({
      data: {
        flow: {...payload}
      }
    }))
  }

  const [enableAnimation,setEnableAnimation] = useState(false);
  useEffect(() => {
    const animation = requestAnimationFrame(() => setEnableAnimation(true));
    if(choices.length === 0) {
      dispatch(FlowChoice.get({
        data: {flow_id: forwardItem?.id}
      }))
    }

    return () => {
      cancelAnimationFrame(animation);
      setEnableAnimation(false);
    }
  },[])

  if(!enableAnimation) {
    return null;
  }

  return (
    <>
      <Box sx={{pb:1}}>
        <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'space-between', mb: 2}}>
          <Typography variant={'subtitle1'} sx={{fontWeight: 700}}>Choices</Typography>
          <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'flex-end'}}>
            {
              nextable &&
              <Stack direction="row" spacing={1} alignItems="center">
                <FormControlLabel control={<Switch checked={Boolean(forwardItem?.next_on === 'flow_choices') || Boolean(forwardItem?.next_on === 'choices')} onChange={_handleToggleSpreadNext} />} label={<Typography sx={{fontSize: 14,fontWeight: 500}}>Next On Choice</Typography>} labelPlacement={'start'} />
              </Stack>
            }
            <Button variant="contained" sx={{ml:1}} size="small" onClick={() => _handleEditMode({name: 'add-choice-'+forwardItem?.id})} disabled={true} title={'Fitur belum tersedia'}>Import</Button>
            <Button variant="contained" sx={{ml:1}} size="small" onClick={() => _handleEditMode({name: 'add-choice-'+forwardItem?.id})}>Add Choices</Button>
          </Box>
        </Box>
        {
          nextable &&
          <Alert severity="warning" variant="outlined" sx={{mb:1}}>
            Warning, Jika SPREAD NEXT di non Aktifkan akan menghapus value next pada semua Choice
          </Alert>
        }
        <Droppable isCombineEnabled={false} type={'flow-group-'+forwardItem?.flow_group_id.toString()} droppableId={'flow-'+forwardItem?.id.toString()+'-dp-choice'}>
          {
            providedDroppable => {
              return (
                <>
                  <div ref={providedDroppable.innerRef} {...providedDroppable.droppableProps}>
                    {
                      choices.map((item,i) => {
                        return (
                          <RenderChoiceItem key={item.id} forwardItem={item} forwardFlow={forwardItem} spreadNext={Boolean(forwardItem?.next_on === 'flow_choices')} nextable={nextable} />
                        )
                      })
                    }
                  </div>
                  {providedDroppable.placeholder}
                </>
              )
            }
          }
        </Droppable>
      </Box>
      {
        Boolean(editMode === prefix+'add-choice-'+forwardItem?.id) &&
        <Box sx={{mb:2}}>
          <EditableText onClose={() => dispatch(inputActions.closeInput())} onSubmit={_handleSubmitChoice} placeholder={'Input Choice'} />
        </Box>
      }
    </>
  )
}

export default function FlowDetail(props) {
  // const {forwardItem} = props;
  const {applicationId,flowId} = useParams()
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const forwardItem = useSelector((state) => FlowModelGetter.get(state,(item) => item.id == flowId)).first()
  const prefix = forwardItem?.name
  const nextItem = new Collection(useSelector((state) => Getter.get(state, (item) => item.id == forwardItem?.next))).first();
  const choices = useSelector((state) => flowChoiceGetter.get(state, (item) => item?.flow_id == forwardItem?.id));
  const types = useSelector((state) => FlowTypeGetter.get(state, (item) => !item.disabled))
  const editMode = useSelector((state) => getInput(state,'activate'))
  const _key = useSelector((state) => getInput(state,'key'));
  const errors = useSelector(getAlertText);
  
  const itemWithoutMe = useSelector((state) => Getter.get(state,(item) => {
    const _g = true
    const _wM = Boolean(item.id != forwardItem?.id)
    return _g && _wM
  }))
  
  const _handleEditMode = (args) => {
    dispatch(alertActions.closeAlert())
    editMode !== prefix+args.name && 
    dispatch(inputActions.openInput({
      activate:prefix+args.name,
      key: args.key,
      value:args.value || ''
    }))
  }
  const _handleCloseEditMode = (args) => {
    dispatch(inputActions.closeInput())
  }
  const _handleSubmit = (args) => {
    dispatch(inputActions.closeInput())

    // if(args) {
      
    // }'
    var itemToUpdate = {}
    itemToUpdate[_key] = args
    if(_key == 'type') {
      const type = types.find((item) => item.name === args)
      itemToUpdate['flow_type_id'] = type?.id
      itemToUpdate['flow_type'] = type
    }
    var payload = {...forwardItem, ...itemToUpdate}
    dispatch(flowActions.update(payload))
    dispatch(FlowApi.updateSingleAttribute({ 
      data: {
        flow: {...forwardItem, ...itemToUpdate}
      }
    }))
  }
  const _handleSubmitEditableNext = (args) => {
    dispatch(inputActions.closeInput())
    var itemToUpdate = {}
    itemToUpdate[_key] = args.id || null
    var payload = {...forwardItem, ...itemToUpdate}
    dispatch(flowActions.update(payload))
    dispatch(FlowApi.updateSingleAttribute({
      data: {
        flow: {...forwardItem, ...itemToUpdate}
      }
    }))
  }
  const _handleDragEnd = (args) => {
    console.log(args)
  }

  const [onCreateNext,setOnCreateNext] = useState(false)
  const _handleCreateNewNext = (args) => {
    dispatch(inputActions.closeInput())
    dispatch(FlowApi.storeFlowNext({
      data: {flow_group_id: forwardItem.flow_group_id,flow_id: forwardItem.id, name: args},
      successAction: () => setOnCreateNext(true),
      rejectAction: (error) => {
        dispatch(alertActions.openAlert({
          type: 'error',
          text: error
        }))
      }
    }))
  }

  useEffect(() => {
    if(forwardItem?.id && Boolean(forwardItem?.next_on !== null && forwardItem?.type !== 'input_choice')) {
      var payload = {...forwardItem}
      var existingChoices = choices.map((item) => {
        return {...item, next: null}
      })
      dispatch(flowChoiceAction.updateMany([...existingChoices]))
      payload['next_on'] = null
      dispatch(flowActions.update(payload))
      dispatch(FlowApi.updateSingleAttribute({
        data: {
          flow: {...forwardItem, next_on: null}
        }
      }))
    }

  }, [forwardItem?.type])

  const status = useSelector((state) => getStoreState(state,path)['status'])
  useEffect(() => {
    if(status === 'pending') {
      dispatch(rootLoaderActions.add('flow_group'))
    }
    if(status === 'idle') {
      dispatch(rootLoaderActions.remove('flow_group'))
    }
  }, [status])

  useEffect(() => {
    return () => {
      setOnCreateNext(false)
    }
  }, [])

  const loaderCSS = {
    display: "block",
    margin: "0 auto",
    borderColor: grey[300],
  };
  if(!forwardItem?.id) {
    // return <BeatLoader
    //   color={indigo[700]}
    //   loading={true}
    //   cssOverride={loaderCSS}
    //   size={20}
    //   aria-label="Loading Spinner"
    //   data-testid="loader"
    // />
    // dispatch(rootLoaderActions.add('flow'))
    return null
  } else {
    // dispatch(rootLoaderActions.remove('flow'))
  }

  return (
    <EngineLayout>
      <Paper elevation={0} variant={'outlined'} sx={{backgroundColor:grey[200], px:2,py:1,cursor:'pointer',mb:2}} 
      onClick={() => _handleEditMode({name:'name',value:forwardItem?.name,key:'name'})}>
        <Typography variant={'subtitle1'} sx={{mb: editMode === 'name' ? 1:0}}>Name</Typography>
        {
          editMode === prefix+'name' ? 
          <EditableText onClose={_handleCloseEditMode} onSubmit={_handleSubmit} />
          :<Typography variant={'subtitle2'}>{forwardItem?.name || <span style={{color: 'red'}}>{'Required'}</span>}</Typography>
        }
      </Paper>
      <Paper elevation={0} variant={'outlined'} sx={{backgroundColor:grey[200], px:2,py:1,cursor:'pointer',mb:2}} 
      onClick={() => _handleEditMode({name:'response',value:forwardItem?.response,key:'response'})}>
        <Typography variant={'subtitle1'} sx={{mb: editMode === 'response' ? 1:0}}>Response</Typography>
        {
          editMode === prefix+'response' ? 
          <EditableText onClose={_handleCloseEditMode} onSubmit={_handleSubmit} />
          :<Typography variant={'subtitle2'}>{forwardItem?.response}</Typography>
        }
      </Paper>
      
      <FlowType forwardItem={forwardItem} />
      
      {
        <Extra forwardItem={forwardItem} />
      }
      {
        Boolean(forwardItem?.next_on === null) &&
        <Paper elevation={0} variant={'outlined'} sx={{backgroundColor:grey[200], px:2,py:1,cursor:'pointer',mb:2}}
        onClick={() => _handleEditMode({name:'next',value:forwardItem?.next,key:'next'})}>
          <Typography variant={'subtitle1'} sx={{mb: editMode === 'next' ? 1:0}}>Next</Typography>
          {
            editMode === prefix+'next' ? 
            <EditableSelectWithSearch 
            keyMatches={['id', 'name', 'response']} 
            onSubmit={_handleSubmitEditableNext} 
            onClose={_handleCloseEditMode} 
            defaultValue={nextItem}
            description={(item) => item.response} 
            options={itemWithoutMe} />
            :<Typography variant={'subtitle2'}>{nextItem?.name}</Typography>
          }
        </Paper>
      }
      {
        <>
          {
            errors &&
            <Alert severity="error" variant="outlined" sx={{mb:2}}>
              {errors}
            </Alert>
          }
          {
            <>
              {
                !forwardItem.next && !onCreateNext &&
                <>
                  {
                    editMode === prefix+'create-flow-for-next' ? 
                    <EditableText onClose={_handleCloseEditMode} onSubmit={_handleCreateNewNext} placeholder={'Flow Name'} />:
                    <Alert severity="info" variant="outlined" sx={{mb:2}}>
                      Next not available ? <a onClick={() => _handleEditMode({name:'create-flow-for-next'})} style={{color: blue[500],textDecoration: 'underline',cursor: 'pointer'}}>Create now</a>.
                    </Alert>
                  }
                </>
              }
            </>
          }
          {
            onCreateNext && forwardItem.next &&
            <Button variant={'outlined'} size={'small'} onClick={() => navigate(`/application/${applicationId}/flow/${forwardItem.next}/detail`)}>Continue to Next Flow</Button>
          }
        </>
      }
      {/* <Button variant={'contained'} size={'small'} color={'primary'} onClick={() => {}} sx={{mr:1}}>Create New Next</Button> */}
    </EngineLayout>
  )
}

const style = {
  py: 1,
  px: 2
}