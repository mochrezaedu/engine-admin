import { IconButton, Paper, Tooltip, Typography } from "@mui/material"
import { grey } from "@mui/material/colors"
import EditableSelect from "../../../components/EditableSelect"
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import TextImage, { TextImagePreview } from "./TextImage";
import TextImageEditorList from "./TextImageEditorList";
import { RenderChoice } from "../FlowDetail";
import WikiDropdown from "./WikiDropdown";
import { useSelector } from "react-redux";
import { getInput, inputActions } from "../../../services/app/input/InputSlicer";
import { useDispatch } from "react-redux";
import FlowTypeApi from "../../../services/api/FlowType/api";
import { FlowTypeGetter } from "../../../services/datacenter/flow_type";
import { useEffect } from "react";
import { FlowModelActions } from "../../../services/datacenter/flow";
import FlowApi from "../../../services/api/Flow/api";

export default function FlowType(props) {
  const {forwardItem} = props
  const dispatch = useDispatch()
  const editMode = useSelector((state) => getInput(state,'activate'))
  const prefix = forwardItem?.name

  const _key = useSelector((state) => getInput(state,'key'));
  const types = useSelector((state) => FlowTypeGetter.get(state, (item) => !item.disabled))
  const typeOptions = useSelector(FlowTypeGetter.all)

  const _handleEditMode = (args) => {
    editMode !== prefix+args.name && 
    dispatch(inputActions.openInput({activate:prefix+args.name,key: args.key,value:args.value || ''}))
  }
  const _handleCloseEditMode = (args) => {
    dispatch(inputActions.closeInput())
  }
  const _handleSubmit = (args) => {
    dispatch(inputActions.closeInput())

    if(args) {
      var itemToUpdate = {}
      itemToUpdate[_key] = args
      if(_key == 'type') {
        const type = types.find((item) => item.name === args)
        itemToUpdate['flow_type_id'] = type?.id
        itemToUpdate['flow_type'] = type
      }
      var payload = {...forwardItem, ...itemToUpdate}
      dispatch(FlowModelActions.update(payload))
      dispatch(FlowApi.updateSingleAttribute({
        data: {
          flow: {...forwardItem, ...itemToUpdate}
        }
      }))
    }
  }

  useEffect(() => {
    if(typeOptions.length === 0) {
      dispatch(FlowTypeApi.all())
    }
  }, [])

  return (
    <>
      <Paper elevation={0} variant={'outlined'} sx={{backgroundColor:grey[200], px:2,py:1,cursor:'pointer',mb:2}}
      onClick={() => _handleEditMode({name:'type',value:forwardItem?.type,key:'type'})}>
        <Typography variant={'subtitle1'} sx={{mb: editMode === 'type' ? 1:0,display:'flex',justifyContent:'space-between',alignItems:'center'}}>
          Type
          {
            (forwardItem?.type && forwardItem?.flow_type?.example_image) &&
            <Tooltip title={
              <>
                <Typography color="inherit">{forwardItem?.type}</Typography>
                <img src={`${forwardItem?.flow_type.example_image}`} width={200} />
              </>
            } sx={{ml:1}}>
              <IconButton size={'small'}>
                <HelpOutlineIcon fontSize={'small'} />
              </IconButton>
            </Tooltip>
          } 
        </Typography>
        {
          editMode === prefix+'type' ? 
          <EditableSelect onClose={_handleCloseEditMode} onSubmit={_handleSubmit} options={typeOptions.map((item) => item.name)} />
          :<Typography variant={'subtitle2'}>{forwardItem?.type || <span style={{color: 'red'}}>{'Required'}</span>}</Typography>
        }
      </Paper>
      {
        Boolean(forwardItem?.type === 'text_image') &&
        <Paper elevation={0} variant={'outlined'} sx={{backgroundColor:grey[200], px:2,py:1,cursor:'pointer',mb:2}}
        onClick={() => _handleEditMode({name:'-text-image',value:forwardItem?.type})}>
          <Typography variant={'subtitle1'} sx={{mb: editMode === 'type' ? 1:0,display:'flex',justifyContent:'space-between',alignItems:'center'}}>
            Image
          </Typography>
          {
            editMode === prefix+'-text-image' ? 
            <TextImage forwardItem={forwardItem} />
            :<TextImagePreview forwardItem={forwardItem} />
          }
        </Paper>
      }
      {
        Boolean(forwardItem?.type === 'text_image_editor_list') &&
        <TextImageEditorList forwardItem={forwardItem} />
      }
      {
        Boolean((forwardItem?.type === 'input_choice')) &&
        <RenderChoice forwardItem={forwardItem} nextable={true} />
      }
      {
        Boolean((forwardItem?.type === 'input_multi_search')) &&
        <RenderChoice forwardItem={forwardItem} nextable={false} />
      }
      {
        Boolean((forwardItem?.type === 'input_multi_choice_vertical_list')) &&
        <RenderChoice forwardItem={forwardItem} nextable={false} />
      }
      {
        Boolean(forwardItem?.type === 'wiki_dropdown') &&
        <WikiDropdown forwardItem={forwardItem} />
      }
    </>
  )
}