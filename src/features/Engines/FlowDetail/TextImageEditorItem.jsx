import { Box, Button, IconButton, ImageListItem, Paper, Typography } from "@mui/material"
import EditableText from "../../../components/EditableText"
import DragHandleIcon from '@mui/icons-material/DragHandle';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import { useSelector } from "react-redux";
import { getInput, inputActions } from "../../../services/app/input/InputSlicer";
import { useDispatch } from "react-redux";
import { grey } from "@mui/material/colors";
import { useEffect, useState } from "react";
import AddIcon from '@mui/icons-material/Add';
import SwapHorizIcon from '@mui/icons-material/SwapHoriz';
import { FileUploader } from "react-drag-drop-files";
import { FlowTextImageEditorListGetter } from "../../../services/datacenter/flow_text_image_editor_list";
import FlowTextImageEditorListImageApi from "../../../services/api/FlowTextImageEditorListImage/api";
import { FlowTextImageEditorListImageGetter } from "../../../services/datacenter/flow_text_image_editor_list_image";
import { Draggable, Droppable } from "react-beautiful-dnd";

export const TextImageEditorTextItem = (props) => {
  const {forwardFlow,forwardItem,onDelete,onSubmitUpdate,forwardIndex} = props
  const dispatch = useDispatch()
  const prefix = forwardFlow.name
  const editMode = useSelector((state) => getInput(state,'activate'))

  const _handleEditMode = (args) => {
    editMode !== prefix+args.name && 
    dispatch(inputActions.openInput({
      activate:prefix+args.name,
      key: args.key,
      value:args.value || ''
    }))
  }

  return (
    <Draggable isDragDisabled={false} draggableId={`flow-text-image-editor-list-${forwardItem.id}`} index={forwardIndex} direction={'vertical'}>
      {
        (provided) => {
          return (
            <div ref={provided.innerRef} {...provided.draggableProps}>
              <Paper
              variant={'outlined'} elevation={0} 
              sx={{mb:1,cursor: 'pointer'}}>
                <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'space-between'}}>
                  <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start', flexGrow: 1}}>
                    <IconButton 
                    {...provided.dragHandleProps}
                    aria-label="drag handle" size={'small'} sx={{mr:1,p:1}}>
                      <DragHandleIcon fontSize={'small'} />
                    </IconButton>
                    <Box sx={{width: '100%'}}>
                      {
                        Boolean(editMode === prefix+'-edit-'+forwardFlow.id+'-text-image-editor-list-'+forwardItem.id) ?
                        <EditableText onClose={() => dispatch(inputActions.closeInput())} onSubmit={(e) => onSubmitUpdate({value: e, item: forwardItem})} placeholder={'Edit Choice'} />:
                        <Typography variant={'subtitle1'} sx={{fontWeight: 700,my:1}} onClick={() => _handleEditMode({name: '-edit-'+forwardFlow.id+'-text-image-editor-list-'+forwardItem.id,value: forwardItem.content})}>
                          {forwardItem.content}
                        </Typography>
                      }
                    </Box>
                  </Box>
                  <Button aria-label="opentab" onClick={() => onDelete(forwardItem)} sx={{color: grey[700], minWidth: 'auto', ml: 1}}>
                    <DeleteOutlineIcon fontSize={'small'} />
                  </Button>
                </Box>
              </Paper>
            </div>
          )
        }
      }
    </Draggable>
  )
}

export const TextImageEditorImageItem = (props) => {
  const {forwardFlow,forwardItem,onDelete,onSubmitUpdate,forwardIndex} = props
  const dispatch = useDispatch()
  const prefix = forwardFlow.name
  const editMode = useSelector((state) => getInput(state,'activate'))
  const items = useSelector((state) => FlowTextImageEditorListImageGetter.get(state,(item) => item.flow_text_image_editor_list_id == forwardItem.id))
  const [files,setFiles] = useState(null)

  const _handleEditMode = (args) => {
    editMode !== prefix+args.name && 
    dispatch(inputActions.openInput({
      activate:prefix+args.name,
      key: args.key,
      value:args.value || ''
    }))
  }

  const _handleAddImages = (args) => {

  }
  const _handleDeleteItem = (args) => {

  }
  const _handlePreviewChange = (args) => {
    setFiles(args)
  }
  const _handleSubmitImage = (args) => {
    dispatch(inputActions.closeInput())
    const data = new FormData()
    data.append('id', forwardItem.id)
    data.append('image', files)
    dispatch(FlowTextImageEditorListImageApi.store({
      data: data
    }))
  }
  const _handleClose = (args) => {
    dispatch(inputActions.closeInput())
  }
  const _handleDeleteImage = (args) => {
    dispatch(FlowTextImageEditorListImageApi.destroy({
      data: {id: args.id}
    }))
  }

  const [enableAnimation,setEnableAnimation] = useState(false);
  useEffect(() => {
    const animation = requestAnimationFrame(() => setEnableAnimation(true));
    if(items.length === 0) {
      dispatch(FlowTextImageEditorListImageApi.all())
    }

    return () => {
      cancelAnimationFrame(animation);
      setEnableAnimation(false);
    }
  }, [])

  if(!enableAnimation) {
    return null;
  }

  return (
    <Draggable isDragDisabled={false} draggableId={`flow-text-image-editor-list-${forwardItem.id}`} index={forwardIndex} direction={'vertical'}>
      {
        provided => {
          return (
            <div ref={provided.innerRef} {...provided.draggableProps}>
              <Paper
              variant={'outlined'} elevation={0} 
              sx={{mb:1,cursor: 'pointer'}}>
                <Box sx={{display: 'flex', alignItems: 'flex-start', justifyContent: 'space-between'}}>
                  <Box sx={{display: 'flex', alignItems: 'stretch', justifyContent: 'flex-start', flexGrow: 1,maxWidth:'100%',overflowX:'hidden',minWidth:'200px'}}>
                    <Box sx={{display: 'flex', alignItems: 'flex-start',flexDirection:'column'}}>
                      <Button 
                      {...provided.dragHandleProps}
                      aria-label="drag handle" size={'small'} sx={{minWidth: 'auto',p:1,color: grey[700]}}>
                        <DragHandleIcon fontSize={'small'} />
                      </Button>
                      <Button title={'Add new image.'} aria-label="opentab" onClick={() => _handleEditMode({name:`create-text-image-editor-list-image-${forwardItem.id}-item`,value: ''})} sx={{color: grey[700], minWidth: 'auto'}}>
                        <AddIcon fontSize={'small'} />
                      </Button>
                      <Button title={'Image Direction ?'} aria-label="opentab" onClick={() => _handleAddImages(forwardItem)} sx={{color: grey[700], minWidth: 'auto'}}>
                        <SwapHorizIcon fontSize={'small'} />
                      </Button>
                      <Button title={'Delete'} aria-label="opentab" onClick={() => onDelete(forwardItem)} sx={{color: grey[700], minWidth: 'auto'}}>
                        <DeleteOutlineIcon fontSize={'small'} />
                      </Button>
                    </Box>
                    <Box sx={{width:'100%',overflowX:'auto'}}>
                      <Droppable isCombineEnabled={false} droppableId={`flow-${forwardFlow.id}-text-image-editor-${forwardItem.id}-image`} type={'flow-'+forwardFlow?.id.toString()+'-editor'} direction={'horizontal'} style={{overflow:'hidden!important'}}>
                        {
                          (providedDroppable,snapshotDroppable) => {
                            return (
                              <Box 
                              ref={providedDroppable.innerRef} 
                              {...providedDroppable.droppableProps}
                              sx={getListStyle(snapshotDroppable.isDraggingOver, items.length)}>
                                <Box sx={{py:1,display:'flex'}}>
                                  {
                                    Boolean(editMode === prefix+'-edit-'+forwardFlow.id+'-text-image-editor-list-image-'+forwardItem.id) ?
                                    <EditableText onClose={() => dispatch(inputActions.closeInput())} onSubmit={(e) => onSubmitUpdate({value: e, item: forwardItem})} placeholder={'Edit Choice'} />:
                                    items?.sort((i,j) => {return i.order - j.order}).map((item,i) => {
                                      return (
                                        <Draggable key={item.id} isDragDisabled={false} draggableId={`flow-text-image-editor-list-${forwardItem.id}-image-${item.id}`} index={item.order}>
                                          {
                                            (providedDraggable,snapshotDraggable) => {
                                              return (
                                                <Box 
                                                {...providedDraggable.dragHandleProps}
                                                ref={providedDraggable.innerRef} {...providedDraggable.draggableProps} sx={{width:'200px',position:'relative',...getItemStyle(
                                                  snapshotDraggable.isDragging,
                                                  providedDraggable.draggableProps.style
                                                )}} onClick={() => {}}>
                                                  <img
                                                    src={`${process.env.REACT_APP_API_URL+'/'+item.path}`}
                                                    loading="lazy"
                                                    style={{width:'100%'}}
                                                  />
                                                  <Box sx={{ml:1, position: 'absolute',top: 4,right:4}}>
                                                    <IconButton title={'DeleteImage'} aria-label="opentab" onClick={() => _handleDeleteImage(item)} sx={{backgroundColor: grey[300],color:grey[700]}}>
                                                      <DeleteOutlineIcon fontSize={'small'} />
                                                    </IconButton>
                                                    {/* <IconButton title={'DeleteImage'} aria-label="opentab" sx={{backgroundColor: grey[300],color:grey[700],ml:1}}>
                                                      <DragHandleIcon fontSize={'small'} />
                                                    </IconButton> */}
                                                  </Box>
                                                </Box>
                                              )
                                            }
                                          }
                                        </Draggable>
                                      )
                                    })
                                  }
                                  {providedDroppable.placeholder}
                                </Box>
                              </Box>
                            )
                          }
                        }
                      </Droppable>
                      {
                        editMode === (prefix+`create-text-image-editor-list-image-${forwardItem.id}-item`) &&
                        <Box sx={{mb:2,display:'flex',alignItems:'center',justifyContent:'space-between'}}>
                          <Box sx={{borderRadius: '4px',flex:1,width:'100%', borderStyle: 'solid',borderWidth: '1px',borderColor: grey[300],py:'3px',px:2,mr:2}}>
                            <FileUploader onDrop={_handlePreviewChange} handleChange={_handlePreviewChange} name="file" types={["JPG", "PNG", "GIF"]}>
                              <Typography variant={'caption'}>
                                {files?.name || 'Drag file here or Klik to Upload'}
                              </Typography>
                            </FileUploader>
                          </Box>
                          <Box>
                            <Button variant={'contained'} size={'small'} color={'success'} onClick={_handleSubmitImage} disabled={!files}>Simpan</Button>
                            <Button variant={'text'} size={'small'} color={'inherit'} 
                            onClick={_handleClose} 
                            sx={{'&:hover': {backgroundColor: 'transparent'}}}>Batal</Button>
                          </Box>
                        </Box>
                      }
                    </Box>
                  </Box>
                </Box>
              </Paper>
            </div>
          )
        }
      }
    </Draggable>
  )
}

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",

  // change background colour if dragging
  // background: isDragging ? "lightgreen" : "grey",

  // styles we need to apply on draggables
  ...draggableStyle
});

const getListStyle = (isDraggingOver, itemsLength) => ({
  display: "flex",
  width: itemsLength * 68.44 + 16,
  minWidth: '100%',
  // overflowX: 'auto'
});