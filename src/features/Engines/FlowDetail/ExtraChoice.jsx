import { Box, Button, IconButton, Paper, Typography } from "@mui/material"
import { useEffect } from "react"
import { useSelector } from "react-redux"
import { useDispatch } from "react-redux"
import EditableText from "../../../components/EditableText"
import FlowExtraChoiceApi from "../../../services/api/FlowExtraChoice/api"
import { getInput, inputActions } from "../../../services/app/input/InputSlicer"
import { FlowExtraChoiceGetter } from "../../../services/datacenter/flow_extra_choice"
import DragHandleIcon from '@mui/icons-material/DragHandle';
import { grey } from "@mui/material/colors"
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';


export default function ExtraChoice(props) {
  const {forwardFlow,forwardItem} = props
  const dispatch = useDispatch()
  const editMode = useSelector((state) => getInput(state,'activate'))
  const prefix = forwardFlow.name
  const choices = useSelector((state) => FlowExtraChoiceGetter.get(state,(item) => item.flow_extra_id === forwardItem.id))
  const _handleCreateExtraChoice = (args) => {
    dispatch(inputActions.closeInput())
    dispatch(FlowExtraChoiceApi.store({
      data: {flow_extra_id: forwardItem.id, text: args}
    }))
  }
  const _handleEditMode = (args) => {
    editMode !== prefix+args.name && 
    dispatch(inputActions.openInput({
      activate:prefix+args.name,
      value: args.value,
    }))
  }
  const _handleEditChoice = (args) => {

  }
  const _handleDeleteItem = (args) => {
    dispatch(FlowExtraChoiceApi.destroy({
      data: {flow_extra_choice_id:args.id}
    }))
  }

  useEffect(() => {
    if(choices.length === 0) {
      const payload = {
        data: {flow_extra_id: forwardItem.id}
      }
      dispatch(FlowExtraChoiceApi.get(payload))
    }
  }, [])

  return (
    <Box>
      <Paper variant={'outlined'} elevation={0} sx={{px:2,py:1,mb:1,cursor: 'pointer'}}>
        <Typography variant={'caption'} sx={{mb:1,fontWeight: 700, display: 'flex', alignItems: 'center', justifyContent: 'flex-start'}}>
          <span>{forwardItem.key}</span>
        </Typography>
        {
          choices.map((item) => {
            return (
              <Paper
              key={item.id}
              variant={'outlined'} elevation={0} 
              sx={{mb:1,cursor: 'pointer'}}>
                <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'space-between'}}>
                  <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start', flexGrow: 1}}>
                    <IconButton 
                    aria-label="drag handle" size={'small'} sx={{mr:1}}>
                      <DragHandleIcon fontSize={'small'} />
                    </IconButton>
                    <Box sx={{width: '100%'}}>
                      {
                        Boolean(editMode === prefix+'-edit-'+forwardFlow.id+'-choice-'+forwardItem.id) ?
                        <EditableText onClose={() => dispatch(inputActions.closeInput())} onSubmit={(e) => _handleEditChoice({value: e, item: item})} placeholder={'Edit Choice'} />:
                        <Typography variant={'subtitle1'} sx={{fontWeight: 700}} onClick={() => _handleEditMode({name: '-edit-'+forwardFlow.id+'-extra-choice-'+item.id,value: item.text})}>
                          {item.text}
                        </Typography>
                      }
                    </Box>
                  </Box>
                  <Button aria-label="opentab" onClick={() => _handleDeleteItem(item)} sx={{color: grey[700], minWidth: 'auto', ml: 1}}>
                    <DeleteOutlineIcon fontSize={'small'} />
                  </Button>
                </Box>
              </Paper>
            )
          })
        }
        {
          editMode === (prefix+'create-extra-choice') &&
          <Box sx={{mb:2}}><EditableText onClose={() => dispatch(inputActions.closeInput())} onSubmit={_handleCreateExtraChoice} placeholder={'Text'} /></Box>
        }
        <Button variant="contained" size="small" onClick={() => _handleEditMode({name:'create-extra-choice',value: ''})}>Add Item</Button>
      </Paper>
    </Box>
  )
}