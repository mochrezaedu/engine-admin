import { Box, Button, IconButton, Paper, Typography } from "@mui/material"
import { Fragment, useEffect, useState } from "react"
import { useSelector } from "react-redux"
import { useDispatch } from "react-redux"
import EditableText from "../../../components/EditableText"
import FlowExtraChoiceApi from "../../../services/api/FlowExtraChoice/api"
import { getInput, inputActions } from "../../../services/app/input/InputSlicer"
import { FlowExtraChoiceGetter } from "../../../services/datacenter/flow_extra_choice"
import { grey } from "@mui/material/colors"
import { FlowTextImageEditorListAction, FlowTextImageEditorListGetter } from "../../../services/datacenter/flow_text_image_editor_list"
import FlowTextImageEditorListApi from "../../../services/api/FlowTextImageEditorList/api"
import { FileUploader } from "react-drag-drop-files"
import { TextImageEditorImageItem, TextImageEditorTextItem } from "./TextImageEditorItem"
import { Draggable, Droppable } from "react-beautiful-dnd"


export default function TextImageEditorList(props) {
  const {forwardItem} = props
  const dispatch = useDispatch()
  const prefix = forwardItem.name
  const editMode = useSelector((state) => getInput(state,'activate'))
  const list = useSelector((state) => FlowTextImageEditorListGetter.get(state,(item) => item.flow_id == forwardItem.id))
  const lists = useSelector(FlowTextImageEditorListGetter.all)
  const [files,setFiles] = useState(null)
  const [enableAnimation,setEnableAnimation] = useState(false);

  const _handleEditMode = (args) => {
    editMode !== prefix+args.name && 
    dispatch(inputActions.openInput({
      activate:prefix+args.name,
      key: args.key,
      value:args.value || ''
    }))
  }
  const _handleEditChoice = (args) => {

  }
  const _handleDeleteItem = (args) => {
    dispatch(FlowTextImageEditorListApi.destroy({
      data: {id: args.id}
    }))
  }
  const _handleCreateItem = (args) => {

  }
  const _handlePreviewChange = (args) => {
    setFiles(args)
  }
  const _handleSubmitText = (args) => {
    dispatch(inputActions.closeInput())
    dispatch(FlowTextImageEditorListApi.store({
      data: {flow_id: forwardItem.id,content: args,type: 'text'}
    }))
  }
  const _handleSubmitImage = (args) => {
    dispatch(inputActions.closeInput())
    const data = new FormData()
    data.append('flow_id', forwardItem.id)
    data.append('content', null)
    data.append('type', 'image')
    data.append('image', files)
    dispatch(FlowTextImageEditorListApi.store({
      data: data
    }))
  }
  const _handleSubmitUpdate = (args) => {
    dispatch(inputActions.closeInput())
    if(args.value) {
      const payload = {id: args.id,name: 'content',value: args.value}
      dispatch(FlowTextImageEditorListApi.updateSingleAttribute({data: payload}))
    }
  }
  const _handleClose = (args) => {
    dispatch(inputActions.closeInput())
  }
  useEffect(() => {
    const animation = requestAnimationFrame(() => setEnableAnimation(true));
    if(list.length === 0) {
      dispatch(FlowTextImageEditorListApi.all())
    }

    return () => {
      cancelAnimationFrame(animation);
      setEnableAnimation(false);
      setFiles(null)
    }
  }, [])

  if(!enableAnimation) {
    return null;
  }

  return (
    <Box sx={{mb:1}}>
      <Box elevation={0} sx={{pb:1}}>
        <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'space-between', mb: 1}}>
          <Typography variant={'subtitle1'} sx={{fontWeight: 700}}>Editor</Typography>
        </Box>
        <Droppable isCombineEnabled={false} droppableId={'flow-'+forwardItem?.id.toString()+'-editor'}>
          {
            providedDroppable => {
              return (
                <>
                  <div ref={providedDroppable.innerRef} {...providedDroppable.droppableProps}>
                  {
                    list.sort((i,j) => (i.order - j.order)).map((item,i) => {
                      
                      return (
                        <Fragment key={item.id}>
                          {
                            item.type === 'text' &&
                            <TextImageEditorTextItem forwardFlow={forwardItem} forwardItem={item} onDelete={_handleDeleteItem} onSubmitUpdate={(e) => _handleSubmitUpdate({...e,id: item.id})} forwardIndex={item.order} />
                          }
                          {
                            item.type === 'image' &&
                            <TextImageEditorImageItem forwardFlow={forwardItem} forwardItem={item} onDelete={_handleDeleteItem} onSubmitUpdate={(args) => {}} forwardIndex={item.order} />
                          }
                        </Fragment>
                      )
                    })
                  }
                  {providedDroppable.placeholder}
                  </div>
                </>
              )
            }
          }
        </Droppable>
        {
          editMode === (prefix+'create-text-image-editor-list-text') &&
          <Box sx={{mb:2}}><EditableText onClose={() => dispatch(inputActions.closeInput())} onSubmit={_handleSubmitText} placeholder={'Content'} /></Box>
        }
        {
          editMode === (prefix+'create-text-image-editor-list-image') &&
          <Box sx={{mb:2,display:'flex',alignItems:'center',justifyContent:'space-between'}}>
            <Box sx={{borderRadius: '4px',flex:1,width:'100%', borderStyle: 'solid',borderWidth: '1px',borderColor: grey[300],py:'3px',px:2,mr:2}}>
              <FileUploader onDrop={_handlePreviewChange} handleChange={_handlePreviewChange} name="file" types={["JPG", "PNG", "GIF"]}>
                <Typography variant={'caption'}>
                  {files?.name || 'Drag file here or Klik to Upload'}
                </Typography>
              </FileUploader>
            </Box>
            <Box>
              <Button variant={'contained'} size={'small'} color={'success'} onClick={_handleSubmitImage} disabled={!files}>Simpan</Button>
              <Button variant={'text'} size={'small'} color={'inherit'} 
              onClick={_handleClose} 
              sx={{'&:hover': {backgroundColor: 'transparent'}}}>Batal</Button>
            </Box>
          </Box>
        }
        {
          <>
            <Button variant="contained" size="small" sx={{mr:1}} onClick={() => _handleEditMode({name:'create-text-image-editor-list-text',value: ''})}>Add Text</Button>
            <Button variant="contained" size="small" onClick={() => _handleEditMode({name:'create-text-image-editor-list-image',value: ''})}>Add Image</Button>
          </>
        }
      </Box>
    </Box>
  )
}