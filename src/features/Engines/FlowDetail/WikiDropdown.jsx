import AddIcon from '@mui/icons-material/Add';
import DragHandleIcon from '@mui/icons-material/DragHandle';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import { Button, IconButton, InputBase, Paper, Typography } from "@mui/material"
import { grey } from '@mui/material/colors';
import { Box } from '@mui/system';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Uniqid } from '../../../helpers/helper';
import { get as getInput, actions } from '../../../services/app/input/InputSlicer';
import { FlowWikiPediaAction, FlowWikipediaGetter } from '../../../services/datacenter/flow_wikipedia';
import FlowWikipedia from '../../../services/api/FlowWikipedia/api'; 

const RenderEditable = (props) => {
  const {onClose,onSubmit} = props
  const editItem = useSelector((state) => getInput(state,'value'));

  const [title,setTitle] = useState(editItem?.title || '')
  const [text,setText] = useState(editItem?.text || '')
  return (
    <Box elevation={0} sx={{my:1}}>
      <Paper sx={{mb:2,backgroundColor: grey[300]}}>
        <Typography variant={'caption'} sx={{py:1,px:2,fontWeight: 500}}>Title</Typography>
        <InputBase
          disabled={false}
          fullWidth
          multiline
          minRows={2}
          sx={{ wordWrap: 'break-word',px:2,flex: 1,backgroundColor:grey[100], borderBottom: 2, borderColor: 'whitesmoke',flex:1,fontSize: 13}}
          placeholder="Title"
          inputProps={{ 'aria-label': '' }}
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
        <Typography variant={'caption'} sx={{py:1,px:2,fontWeight: 500,width: '100%'}}>Text</Typography>
        <InputBase
          fullWidth
          multiline
          minRows={2}
          sx={{ px:2,flex: 1,backgroundColor:grey[100],flex:1,fontSize: 13}}
          placeholder="Text Content"
          inputProps={{ 'aria-label': '' }}
          value={text}
          onChange={(e) => setText(e.target.value)}
        />
      </Paper>
      <Box>
        <Button variant={'contained'} size={'small'} onClick={() => onSubmit({title: title, text: text})} color={'success'}>Simpan</Button>
        <Button variant={'text'} size={'small'} color={'inherit'} onClick={onClose} sx={{'&:hover': {backgroundColor: 'transparent'}}}>Batal</Button>
      </Box>
    </Box>
  )
}

export default function WikiDropdown(props) {
  const {forwardItem} = props
  const dispatch = useDispatch()
  const prefix = 'flow-'+forwardItem.id+'-wiki-items-'
  const currentInputName = useSelector((state) => getInput(state, 'activate'))
  const currentInputValue = useSelector((state) => getInput(state,'value'))
  const wikipedias = useSelector((state) => FlowWikipediaGetter.get(state, (item) => item.flow_id === forwardItem.id))

  const _handleInputActivator = (args) => {
    dispatch(actions.openInput({
      activate:args.name,
      value: args.value,
    }))
  }
  const _handleSubmitCreate = (args) => {
    dispatch(actions.closeInput())
    const payload = {
      id: Uniqid(),
      title: args.title,
      text: args.text,
      flow_id: forwardItem.id
    }
    dispatch(FlowWikiPediaAction.createEmpty(payload))
    dispatch(FlowWikipedia.store({
      data: payload
    }))
  }
  const _handleSubmitEdit = (args) => {
    const {title,text} = args
    const {id} = currentInputValue
    dispatch(actions.closeInput())
    const payload = {
      id: id,
      title: title,
      text: text,
      flow_id: forwardItem.id
    }
    dispatch(FlowWikiPediaAction.update(payload))
    dispatch(FlowWikipedia.updateAttribute({
      data: {
        flow_wikipedia: payload
      }
    }))
  }
  const _handleDelete = (args) => {
    dispatch(FlowWikiPediaAction.delete([args.id]))
    dispatch(FlowWikipedia.destroy({
      data: {flow_wikipedia_id: args.id}
    }))
  }
  const _handleCloseInput = () => {
    dispatch(actions.closeInput())
  }

  useEffect(() => {
    if(wikipedias.length == 0) {
      dispatch(FlowWikipedia.all())      
    }
  }, [])

  return (
    <>
      <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start', marginBottom: 16}}>
        <Typography variant={'subtitle1'} sx={{fontWeight: 700}}>Wikipedia Dropdown Items</Typography>
        {
          Boolean(currentInputName !== prefix+'create') &&
          <Button sx={{ml:2}} variant="contained" size="small" startIcon={<AddIcon fontSize={'small'} />} 
          onClick={() => _handleInputActivator({name: prefix+'create', value: {title: '', text: ''}})}>Add Item</Button>
        }
      </div>
      {
        wikipedias.map((item) => {
          return (
            <div key={item.id}>
              <Paper variant={'outlined'} elevation={0} sx={{px:2,py:1,mb:1,cursor: 'pointer'}}>
                {
                  currentInputName === (prefix+'editwikipedia-'+item.id) ?
                  <RenderEditable onSubmit={_handleSubmitEdit} onClose={_handleCloseInput} />:
                  <Box>
                    <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'space-between'}}>
                      <Box onClick={() => _handleInputActivator({name: prefix+'editwikipedia-'+item.id, value: {...item}})} sx={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start', flexGrow: 1}}>
                        <IconButton  
                        aria-label="drag handle" size={'small'} sx={{mr:1}}>
                          <DragHandleIcon fontSize={'small'} />
                        </IconButton>
                        <Box sx={{width: '100%'}}>
                          <Typography variant={'subtitle1'} sx={{fontWeight: 700}}>
                            {item.title}
                          </Typography>
                        </Box>
                      </Box>
                      <Button aria-label="opentab" onClick={() => _handleDelete(item)} sx={{color: grey[700], minWidth: 'auto', ml: 1}}>
                        <DeleteOutlineIcon fontSize={'small'} />
                      </Button>
                    </Box>
                    <Paper elevation={0} variant={'text'} 
                    sx={{px:2,py:1,cursor:'pointer',mb:2,mt:1}}>
                      <Typography variant={'subtitle2'}>{item.text || 'null'}</Typography>
                    </Paper>
                  </Box>
                }
              </Paper>
            </div>
          )
        })
      }
      {
        Boolean(currentInputName === prefix+'create') &&
        <RenderEditable onClose={_handleCloseInput} onSubmit={_handleSubmitCreate} />
      }
    </>
  )
}