import { Box, IconButton, ImageList, ImageListItem, Tooltip, Typography } from "@mui/material"
import { useState } from "react"
import { useSelector } from "react-redux"
import ModalComponent from "../../../components/ModalComponent"
import { FlowExtraRequiredGetter } from "../../../services/datacenter/flow_extra_required"
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';

export default function RenderHelp(props) {
  const {forwardFlow,forwardItem} = props
  const [open,setOpen] = useState(false)
  const requiredExtras = useSelector((state) => FlowExtraRequiredGetter.get(state, (item) => item.flow_type_id == parseInt(forwardFlow.flow_type_id)))
  const currentItem = requiredExtras.find((i) => {
    return Boolean(i.key === forwardItem.key)
  })
  return (
    <>
      {
        Boolean(requiredExtras.find((i) => {
          return Boolean(i.key === forwardItem.key)
        })?.description) &&
        <Box sx={{width:'100%',textAlign: 'right'}}>
          <Typography variant={'caption'}>
            <Tooltip title={requiredExtras.find((i) => {
              return Boolean(i.key === forwardItem.key)
            })?.description || ''}>
              <IconButton size={'small'} onClick={() => setOpen(true)}>
                <HelpOutlineIcon fontSize={'small'} />
              </IconButton>
            </Tooltip>
          </Typography>
        </Box> 
      }
      <ModalComponent title={currentItem?.key} onClose={() => setOpen(false)} open={open}>
        <Box>{currentItem?.description}</Box>
        <ImageList cols={1}>
          <ImageListItem>
            <img
              src={`${currentItem?.example_image_url}`}
              loading="lazy"
            />
          </ImageListItem>
        </ImageList>
      </ModalComponent>
    </>
  )
}