import { Box, Button, IconButton, ImageListItem, Typography } from "@mui/material"
import { FileUploader } from "react-drag-drop-files"
import CloseIcon from '@mui/icons-material/Close';
import { useState } from "react";
import { grey } from "@mui/material/colors";
import { useSelector } from "react-redux";
import { getInput, inputActions } from "../../../services/app/input/InputSlicer";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import FlowImageApi from "../../../services/api/FlowImages/api";
import { FlowImagesGetter } from "../../../services/datacenter/flow_images";

export function TextImagePreview(props) {
  const {forwardItem} = props
  const dispatch = useDispatch()
  const images = useSelector((state) => FlowImagesGetter.get(state,(item) => item.flow_id == forwardItem.id))
  useEffect(() => {
    if(images.length === 0) {
      dispatch(FlowImageApi.all())
    }
  }, [])

  return (
    <>
      {
        images.first() &&
        <Box>
          <ImageListItem sx={{width:'100%'}}>
            <img
              src={`http://localhost:8000/${images.first().path}`}
              loading="lazy"
              width={'100%'}
              style={{borderRadius:'6px'}}
            />
          </ImageListItem>
        </Box>
      }
    </>
  )
}

export default function TextImage(props) {
  const {forwardItem} = props
  const dispatch = useDispatch()
  const [preview,setPreview] = useState(null)
  const [files,setFiles] = useState(null)
  const editMode = useSelector(getInput)
  const [flowImages,setFlowImages] = useState([])

  const _handlePreviewChange = (args) => {
    setFiles(args)
  }
  const _handleClose = () => {
    dispatch(inputActions.closeInput())
  }
  const _handleSubmit = () => {
    dispatch(inputActions.closeInput())
    dispatch(FlowImageApi.store({
      data: {
        flow_id: forwardItem.id,
        image: files
      }
    }))
  }

  return (
    <Box sx={{mb:1}}>
      <Box sx={{py:1, pb:2}}>
        <FileUploader onDrop={_handlePreviewChange} handleChange={_handlePreviewChange} name="file" types={["JPG", "PNG", "GIF"]}>
          <Typography variant={'caption'}>
            {files?.name || 'Drag file here or Klik to Upload'}
          </Typography>
        </FileUploader>
      </Box>
      <Box>
        <Button variant={'contained'} size={'small'} color={'success'} onClick={_handleSubmit} disabled={!files}>Simpan</Button>
        <Button variant={'text'} size={'small'} color={'inherit'} 
        onClick={_handleClose} 
        sx={{'&:hover': {backgroundColor: 'transparent'}}}>Batal</Button>
      </Box>
    </Box>
  )
}