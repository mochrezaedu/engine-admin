import { Box, Button, IconButton, InputBase, Paper, Typography } from "@mui/material"
import { grey } from "@mui/material/colors"
import { useEffect, useState } from "react"
import { Droppable } from "react-beautiful-dnd"
import { useSelector } from "react-redux"
import { useDispatch } from "react-redux"
import FlowExtraChoiceApi from "../../../services/api/FlowExtraChoice/api"
import { getInput } from "../../../services/app/input/InputSlicer"
import { FlowExtraChoiceGetter } from "../../../services/datacenter/flow_extra_choice"
import { FlowExtraRequiredGetter } from "../../../services/datacenter/flow_extra_required"

// export const RenderChoiceItem = (props) => {
//   const {forwardFlow, forwardItem, spreadNext} = props
//   const dispatch = useDispatch()
//   const editMode = useSelector((state) => getInput(state,'activate'))
//   const prefix = forwardItem?.name
//   const options = useSelector((state) => Getter.get(state,(item) => {
//     const _g = true
//     const _wM = Boolean(item.id != forwardFlow.id)
//     return _g && _wM
//   }))
//   const nextItem = useSelector((state) => Getter.get(state, (item) => item.id == forwardItem.next)).first();
//   const flow_choices = useSelector((state) => flowChoiceGetter.get(state, (item) => item.flow_id === forwardFlow.id))

//   const _handleEditMode = (args) => {
//     editMode !== prefix+args.name && 
//     dispatch(inputActions.openInput({activate:prefix+args.name,key: args.key,value:args.value || ''}))
//   }

//   const _handleEditChoice = (args) => {
//     const {value,item} = args
//     const payload = {...item, text: value}
//     dispatch(flowChoiceAction.update(payload))
//   }
//   const _handleDeleteItem = () => {
//     dispatch(flowChoiceAction.delete([forwardItem.id]))
//   }

//   return (
//     <Draggable isDragDisabled={true} draggableId={'flow-'+forwardFlow.id+'-choices-'+forwardItem.id.toString()} index={forwardItem.order}>
//       {
//         (provided,snapshot) => {
//           const {draggableProps,dragHandleProps,innerRef, ...rest} = provided
//           const {style, ...restProps} = draggableProps
//           return (
//             <>
//               <Paper ref={innerRef} {...restProps}
//               variant={'outlined'} elevation={0} 
//               sx={{px:2,py:1,mb:1,cursor: 'pointer', ...style}}>
//                 {
//                   <>
//                     <Box>
//                       <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'space-between'}}>
//                         <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start', flexGrow: 1}}>
//                           <IconButton  
//                           {...dragHandleProps}
//                           aria-label="drag handle" size={'small'} sx={{mr:1}}>
//                             <DragHandleIcon fontSize={'small'} />
//                           </IconButton>
//                           <Box sx={{width: '100%'}}>
//                             {
//                               Boolean(editMode === prefix+'-edit-'+forwardFlow.id+'-choice-'+forwardItem.id) ?
//                               <EditableText onClose={() => dispatch(inputActions.closeInput())} onSubmit={(e) => _handleEditChoice({value: e, item: forwardItem})} placeholder={'Edit Choice'} />:
//                               <Typography variant={'subtitle1'} sx={{fontWeight: 700}} onClick={() => _handleEditMode({name: '-edit-'+forwardFlow.id+'-choice-'+forwardItem.id,value: forwardItem.text})}>
//                                 {forwardItem.text}
//                               </Typography>
//                             }
//                           </Box>
//                         </Box>
//                         <Button aria-label="opentab" onClick={_handleDeleteItem} sx={{color: grey[700], minWidth: 'auto', ml: 1}}>
//                           <DeleteOutlineIcon fontSize={'small'} />
//                         </Button>
//                       </Box>
//                       {
//                         spreadNext &&
//                         <Paper elevation={0} variant={'outlined'} 
//                         sx={{backgroundColor:grey[200], px:2,py:1,cursor:'pointer',mb:2,mt:1}}
//                         onClick={() => _handleEditMode({name:'-choices-'+forwardItem.id+'next',value:forwardItem.next,key:'-choices-'+forwardItem.id+'next'})}>
//                           <Typography variant={'caption'} sx={{mb: editMode === '-choices-'+forwardItem.id+'next' ? 1:0}}>Next</Typography>
//                           {
//                             editMode === prefix+'-choices-'+forwardItem.id+'next' ? 
//                             <EditableSelectWithSearch keyMatches={['id', 'name', 'response']} 
//                             onSubmit={(e) => _handleSubmitEditableNext({...e, choice: forwardItem})} 
//                             onClose={() => dispatch(inputActions.closeInput())} 
//                             description={(item) => item.response}
//                             options={options} />
//                             :<Typography variant={'subtitle2'}>{nextItem?.name || <span style={{color: 'red'}}>{'Required'}</span>}</Typography>
//                           }
//                         </Paper>
//                       }
//                     </Box>
//                   </>
//                 }
//               </Paper>
//               {provided.placeholder}
//             </>
//           )
//         }
//       }
//     </Draggable>
//   )
// }
const RenderChoices = (props) => {
  const {forwardItem} = props
  const dispatch = useDispatch()
  const options = useSelector((state) => FlowExtraChoiceGetter.get(state,(item) => item.flow_extra_id === forwardItem.id))
  useEffect(() => {
    if(options.length === 0) {
      dispatch(FlowExtraChoiceApi.get({
        data: {flow_extra_id: forwardItem.id}
      }))
    }
  }, [])

  return (
    <Droppable isCombineEnabled={false} droppableId={'flow-'+forwardItem.id.toString()+'-dp-choice'}>
      {
        providedDroppable => {
          return (
            <>
              <div ref={providedDroppable.innerRef} {...providedDroppable.droppableProps}>
                {
                  options.map((item,i) => {
                    return (
                      <Box>{i}</Box>
                      // <RenderChoiceItem key={item.id} forwardItem={item} forwardFlow={forwardItem} spreadNext={Boolean(forwardItem.next_on === 'flow_choices')} />
                    )
                  })
                }
              </div>
              {providedDroppable.placeholder}
            </>
          )
        }
      }
    </Droppable>
  )
}

export default function EditExtraText(props) {
  const {onClose,onSubmit,item} = props
  const editItem = useSelector((state) => getInput(state,'value'));
  const flowTypeRequired = useSelector((state => FlowExtraRequiredGetter.get(state,(i) => i.id === item.flow_type_required_id))).first()
  
  const [_key,setKey] = useState(editItem?.key || '')
  const [_value,setValue] = useState(editItem?.value || '')
  
  return (
    <Box elevation={0} sx={{my:1}}>
      <Paper sx={{mb:2,backgroundColor: grey[300]}}>
        <Typography variant={'caption'} sx={{py:1,px:2,fontWeight: 500}}>Key</Typography>
        <InputBase
          disabled={true}
          fullWidth
          multiline
          minRows={2}
          sx={{ wordWrap: 'break-word',px:2,flex: 1,backgroundColor:grey[100], borderBottom: 2, borderColor: 'whitesmoke',flex:1,fontSize: 13}}
          placeholder="Title"
          inputProps={{ 'aria-label': '' }}
          value={_key}
          onChange={(e) => setKey(e.target.value)}
        />
        <Typography variant={'caption'} sx={{py:1,px:2,fontWeight: 500,width: '100%',display:'flex',justifyContent:'space-between',alignItems:'center'}}>
          Value
        </Typography>
        {
          item.type === 'text' &&
          <InputBase
            fullWidth
            multiline
            minRows={2}
            sx={{ px:2,flex: 1,backgroundColor:grey[100],flex:1,fontSize: 13}}
            placeholder="Value"
            inputProps={{ 'aria-label': '' }}
            value={_value}
            onChange={(e) => setValue(e.target.value)}
          />
        }
      </Paper>
      <Box>
        <Button variant={'contained'} size={'small'} onClick={() => onSubmit({key: _key, value: _value})} color={'success'}>Simpan</Button>
        <Button variant={'text'} size={'small'} color={'inherit'} onClick={onClose} sx={{'&:hover': {backgroundColor: 'transparent'}}}>Batal</Button>
      </Box>
    </Box>
  )
}