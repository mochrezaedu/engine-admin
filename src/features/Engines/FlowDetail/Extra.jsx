import { Box, Button, IconButton, ImageList, ImageListItem, InputBase, Paper, Tooltip, Typography } from "@mui/material";
import { grey } from "@mui/material/colors";
import { useEffect, useMemo, useState } from "react";
import { useSelector } from "react-redux";
import {actions as inputActions, get as getInput} from '../../../services/app/input/InputSlicer'
import { useDispatch } from "react-redux";
import FlowExtra from "../../../services/api/FlowExtra/api";
import AddIcon from '@mui/icons-material/Add';
import { FlowExtraModelActions, FlowExtraModelGetter } from "../../../services/datacenter/flow_extra";
import { FlowTypeGetter } from "../../../services/datacenter/flow_type";
import { FlowExtraRequiredGetter } from "../../../services/datacenter/flow_extra_required";
import { FlowModelActions } from "../../../services/datacenter/flow";
import { get as getModal, actions as modalActions } from "../../../services/app/modal/ModalSlicer";
import { Uniqid } from "../../../helpers/helper";
import { UpscaleTablesGetter } from "../../../services/datacenter/upscale_dataset_names";
import EditableSelectWithSearch from "../../../components/EditableSelectWithSearch";
import { getUpscaleTables } from "../../../services/api/Resources";
import FlowExtraRequired from "../../../services/api/FlowExtraRequired/api";
import ModalComponent from "../../../components/ModalComponent";
import { Droppable } from "react-beautiful-dnd";
import ExtraText from "./ExtraText";
import ExtraChoice from "./ExtraChoice";

export default function Extra(props) {
  const {forwardItem} = props
  const dispatch = useDispatch()
  const editMode = useSelector((state) => getInput(state,'activate'))
  const inputKey = useSelector((state) => getInput(state,'key'))
  const prefix = forwardItem.name
  const extras = useSelector((state) => FlowExtraModelGetter.get(state, (item) => {
    return item.flow_id == forwardItem.id
  }))
  const requiredExtras = useSelector((state) => FlowExtraRequiredGetter.get(state, (item) => item.flow_type_id == parseInt(forwardItem.flow_type_id)))
  const requiredExtrasLength = requiredExtras.length
  const modalName = useSelector((state) => getModal(state,'name'))
  const modalActive = useSelector((state) => getModal(state,'activate'))
  const upscaleTables = useSelector(UpscaleTablesGetter.all)
  const hasDatasetName = requiredExtras.find((item) => Boolean(item.key === 'dataset_name'))

  const _handleSubmitExtraDatasetName = (args) => {
    dispatch(inputActions.closeInput())
    const existingData = extras.find((item) => item.key === inputKey)
    if(existingData) {
      const payload = {...existingData, value: args.value}
      dispatch(FlowExtraModelActions.update(payload))
      dispatch(FlowExtra.update({
        data: payload
      }))
    }
  }
  
  const _handleCloseEditMode = (args) => {
    dispatch(inputActions.closeInput())
  }
  const _refillExtras = () => {
    dispatch(FlowExtra.refillExtra({
      data: {flow_id:forwardItem.id}
    }))
  }

  useEffect(() => {
    dispatch(modalActions.setModal({key:'loading',value:false}))
  }, [])

  useEffect(() => {
    _refillExtras()
  }, [forwardItem.type])

  useEffect(() => {
    if(hasDatasetName) {
      if(upscaleTables.length === 0) {
        dispatch(getUpscaleTables())
      }
    }
  }, [hasDatasetName])


  if(extras.length === 0) {
    return null
  }
  return (
    <Box sx={{mb:3}}>
      <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start', marginBottom: 16}}>
        <Typography variant={'subtitle1'} sx={{fontWeight: 700}}>Additional Data</Typography>
      </div>
      {
        extras.map((item) => {
          return (
            <Box key={item.id}>
              {
                item.required?.type === 'text' &&
                <ExtraText forwardItem={item} forwardFlow={forwardItem} />
              }
              {
                item.required?.type === 'choice' &&
                <ExtraChoice forwardItem={item} forwardFlow={forwardItem} />
              }
            </Box>
          )
        })
      }
    </Box>
  )
}

const rename = () => {
  return (
    <div>rename</div>
  )
}