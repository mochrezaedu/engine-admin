import { Paper, Typography } from "@mui/material"
import { Box } from "@mui/system"
import { useDispatch } from "react-redux"
import { useSelector } from "react-redux"
import FlowExtra from "../../../services/api/FlowExtra/api"
import { getInput, inputActions } from "../../../services/app/input/InputSlicer"
import { FlowExtraModelActions, FlowExtraModelGetter } from "../../../services/datacenter/flow_extra"
import EditExtraText from "./EditExtraText"
import RenderHelp from "./ExtraHelp"

export default function ExtraText(props) {
  const {forwardItem,forwardFlow} = props
  const dispatch = useDispatch()
  const editMode = useSelector((state) => getInput(state,'activate'))
  const prefix = forwardFlow.name
  const extras = useSelector((state) => FlowExtraModelGetter.get(state,(item) => item.flow_id === forwardFlow.id))

  const _handleSubmitFlowData = (args) => {
    dispatch(inputActions.closeInput())
    if(args.value) {
      const existingData = extras.find((item) => item.key === args.key)
      if(existingData) {
        const payload = {...existingData}
        payload.value = args.value
        dispatch(FlowExtraModelActions.update(payload))
        dispatch(FlowExtra.update({
          data: payload
        }))
      }
    }
  }
  const _handleCloseEditMode = (args) => {
    dispatch(inputActions.closeInput())
  }
  const _handleEditMode = (args) => {
    editMode !== prefix+args.name && 
    dispatch(inputActions.openInput({
      activate:prefix+args.name,
      key: args.key,
      value:args.value || ''
    }))
  }

  return (
    <div>
      <Paper variant={'outlined'} elevation={0} sx={{px:2,py:1,mb:1,cursor: 'pointer'}}>
        {
          editMode === (prefix+'new-data-'+forwardItem.id) ?
          <EditExtraText item={forwardItem} onSubmit={_handleSubmitFlowData} onClose={_handleCloseEditMode} />:
          <Box sx={{display: 'flex',justifyContent: 'space-between', alignItems: 'flex-start'}}>
            <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'space-between',width: '100%'}}>
              <Box onClick={() => {
                _handleEditMode({
                  name:'new-data-'+forwardItem.id,
                  key:forwardItem.key,
                  value: {key: forwardItem.key, value: forwardItem.value, ...forwardItem}
                })}}>
                <Typography variant={'caption'} sx={{fontWeight: 700, display: 'flex', alignItems: 'center', justifyContent: 'flex-start'}}>
                  <span>{forwardItem.key}</span>
                </Typography>
                <Typography 
                variant={'subtitle1'}>{forwardItem.value || <span style={{color: 'red'}}>{'Required'}</span>}</Typography>
              </Box>
              {/* <Button aria-label="opentab" onClick={() => _handleDeleteData(item.id)} sx={{color: grey[700], minWidth: 'auto', ml: 1}}>
                <DeleteOutlineIcon fontSize={'small'} />
              </Button> */}
            </Box>
            <RenderHelp forwardFlow={forwardFlow} forwardItem={forwardItem} />
          </Box>
        }
      </Paper>
    </div>
  )
}