import { Box } from "@mui/material";

export default function Empty(props) {

  return (
    <Box>Flow empty, Please create one!</Box>
  )
}