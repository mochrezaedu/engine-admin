import { Box } from "@mui/system"
import { useDispatch } from "react-redux"
import { useParams } from "react-router-dom"
import EditableText from "../../../components/EditableText"
import { Uniqid } from "../../../helpers/helper"
import { store } from "../../../services/api/FlowGroup/api"
import { modalActions } from "../../../services/app/modal/ModalSlicer"
import { actions as FlowGroupModel } from "../../../services/datacenter/flow_group"

export default function CreateGroup(props) {
  const dispatch = useDispatch()
  const {applicationId} = useParams()

  const _handleStoreFlowGroup = (args) => {
    if(args) {
      const id = Uniqid()
      // dispatch(FlowGroupModel.createEmpty({id: id, name: args, application_id: applicationId}))
      dispatch(store({
        data: {
          flow_group_id: id,
          name: args, 
          application_id: applicationId
        }
      }))
      dispatch(modalActions.closeModal())
    }
  }

  return (
    <Box sx={{mb:2}}>
      <EditableText placeholder={'Group Name'} onSubmit={_handleStoreFlowGroup} submitButtonText={'Add'} />
    </Box>
  )
}