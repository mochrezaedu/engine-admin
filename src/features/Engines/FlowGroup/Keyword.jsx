import { Chip, List, ListItem, ListItemText } from "@mui/material";
import { Box } from "@mui/system";
import EditableText from "../../../components/EditableText";
import DeleteIcon from '@mui/icons-material/Delete';
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { FlowGroupKeywordActions, FlowGroupKeywordGetter } from "../../../services/datacenter/flow_group_keyword";
import FlowGroupKeywordApi from "../../../services/api/FlowGroupKeyword/api";
import { useEffect, useState } from "react";
import { getInput, inputActions } from "../../../services/app/input/InputSlicer";
import axios from "axios";

export default function Keyword(props) {
  const {forwardItem} = props
  const dispatch = useDispatch()
  const items = useSelector((state) => FlowGroupKeywordGetter.get(state,(item) => item.flow_group_id == forwardItem.id))
  const [keywordResponse,setKeywordResponse] = useState([])
  const _handleSubmit = (args) => {
    if(args) {
      dispatch(FlowGroupKeywordApi.store({
        data: {name: args,flow_group_id: forwardItem.id}
      }))
    }
    dispatch(inputActions.closeInput())
  }
  const _handleDelete = (args) => {
    dispatch(FlowGroupKeywordApi.destroy({
      data: {id: args.id}
    }))
  }
  const _handleTestHear = (args) => {
    if(args) {
      axios.post('/flow_group_keyword/test_keyword', {keyword: args}).then((_r) => {
        setKeywordResponse([..._r.data])
      })
    }
    dispatch(inputActions.closeInput())
  }

  useEffect(() => {
    if(items.length == 0) {
      dispatch(FlowGroupKeywordApi.all())
    }
  }, [])
  return (
    <Box>
      <Box sx={{mb:keywordResponse.length > 0 ? 1:2}}>
        <EditableText placeholder={'Send Message'} onSubmit={_handleTestHear} submitButtonText={'Send'} />
      </Box>
      {
        keywordResponse.length > 0 &&
        <Box sx={{mb:2}}>
          <List dense>
          {
            keywordResponse.map((item) => (
              <ListItem key={item.id}>
                <ListItemText
                  primary={item.name}
                />
              </ListItem>  
            ))
          }
          </List>
        </Box>
      }
      <Box sx={{mb:2}}>
        <EditableText placeholder={'Add Keyword'} onSubmit={_handleSubmit} submitButtonText={'Add'} />
      </Box>
      {
        items.map((item) => (
          <Chip
            key={item.id}
            label={item.name}
            onDelete={() => _handleDelete(item)}
            deleteIcon={<DeleteIcon />}
            variant="outlined"
            sx={{mr:1,bgColor:'white'}}
          />
        ))
      }
    </Box>
  )
}