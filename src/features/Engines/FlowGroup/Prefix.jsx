import { Box } from "@mui/system"
import { useDispatch } from "react-redux"
import { useParams } from "react-router-dom"
import EditableText from "../../../components/EditableText"
import { Uniqid } from "../../../helpers/helper"
import FlowGroupApi, { store } from "../../../services/api/FlowGroup/api"
import { modalActions } from "../../../services/app/modal/ModalSlicer"
import { actions as FlowGroupModel } from "../../../services/datacenter/flow_group"

export default function Prefix(props) {
  const {forwardItem} = props
  const dispatch = useDispatch()

  const _handleStorePrefix = (args) => {
    if(args) {
      dispatch(FlowGroupApi.updateSingleAttribute({
        data: {
          flow_group: {...forwardItem, prefix: args}
        }
      }))
      dispatch(modalActions.closeModal())
    }
  }

  return (
    <Box sx={{mb:2}}>
      <EditableText placeholder={'Prefix Name'} defaultValue={forwardItem.prefix || ''} onSubmit={_handleStorePrefix} submitButtonText={'Submit'} />
    </Box>
  )
}