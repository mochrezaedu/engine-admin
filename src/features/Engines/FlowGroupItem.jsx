import {useEffect, useState} from 'react';
import List from '@mui/material/List';
import ListSubheader from '@mui/material/ListSubheader';
import FlowList from './FlowList';
import { Box, Button, Chip, Collapse, Divider, IconButton, LinearProgress, ListItemIcon, ListItemText, Menu, MenuItem, Portal, Typography } from '@mui/material';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import DndProvider from '../../components/DndProvider';
import AddIcon from '@mui/icons-material/Add';
import { grey } from '@mui/material/colors';
import { useDispatch, useSelector } from 'react-redux';
import {actions as FlowModelActions, Getter} from '../../services/datacenter/flow'
import {actions as FlowGroupModelActions, Getter as flowGroupGetter} from '../../services/datacenter/flow_group'
import {actions as flowChoiceActions} from '../../services/datacenter/flow_choices'
import {actions as inputActions, get as getInput} from '../../services/app/input/InputSlicer'
import EditableText from '../../components/EditableText';
import CreateIcon from '@mui/icons-material/Create';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { destroy, setDefaultRoute, store, updateName, updateSingleAttribute } from '../../services/api/FlowGroup/api';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import { actions as modalActions, get as getModal } from '../../services/app/modal/ModalSlicer';
import FlowApi from '../../services/api/Flow/api';
import LabelImportantIcon from '@mui/icons-material/LabelImportant';
import Alias from './FlowGroup/Keyword';
import BasicModal from '../../components/BasicModal';
import Prefix from './FlowGroup/Prefix';

export default function FlowGroupItem(props) {
  const {forwardItem} = props;
  const dispatch = useDispatch()

  const [open,setOpen] = useState(false);
  const detailName = useSelector((state) => getModal(state,'name'))
  const loading = useSelector((state) => flowGroupGetter.getLoadingState(state)) === 'pending'
  const inputName = useSelector((state) => getInput(state,'activate'))
  const defaultGroup = useSelector((state) => flowGroupGetter.get(state, (item) => Boolean(item.default))).first()

  const _handleAddFlow = (args) => {
    dispatch(inputActions.openInput({activate: 'add-new-flow-'+forwardItem.id}))
    setOpen(true)
  }
  
  const _handleSubmitEditGroup = (args) => {
    dispatch(inputActions.closeInput())
    if(args) {
      var itemToUpdate = {}
      itemToUpdate['name'] = args 
      var payload = {...forwardItem, ...itemToUpdate}
      dispatch(FlowGroupModelActions.update(payload))
      dispatch(updateSingleAttribute({
        data: {
          flow_group: payload
        }
      }))
    }
  }

  const _handleDeleteItem = (args) => {
    dispatch(FlowGroupModelActions.delete([forwardItem.id]))
    dispatch(destroy({
      data: {
        flow_group_id: forwardItem.id
      }
    }))
  }

  const _handleOpenAlias = (args) => {
    dispatch(modalActions.openModal({
      activate: true,
      title: 'Keyword',
      name: 'flow-group-keyword-'+forwardItem.id,
      loading: false
    }))
  }

  const _handleSetPrefix = (args) => {
    dispatch(modalActions.openModal({
      activate: true,
      title: 'Prefix',
      name: 'flow-group-prefix-'+forwardItem.id,
      loading: false
    }))
  }

  const [menu, setMenu] = useState(null);
  const menuOpen = Boolean(menu);
  const _handleSetdefaultGroup = (args) => {
    dispatch(FlowGroupModelActions.update({...defaultGroup, default: null}))
    setMenu(null)
    dispatch(FlowGroupModelActions.update({...forwardItem, default: 'yes'}))
    dispatch(setDefaultRoute({
      data: {
        flow_group_id: forwardItem.id,
      }
    }))
  }
  useEffect(() => {
    // console.log(forwardItem)
  }, [forwardItem]);

  return (
    <>
      <List sx={{ width: '100%', bgcolor: 'background.paper'}}
      subheader={
        <>
          <Box sx={{p:2,display: 'flex',alignItems: 'center',justifyContent: 'space-between'}}>
            {
              Boolean(inputName === 'group-name-'+forwardItem.id) ? 
              <EditableText onClose={() => dispatch(inputActions.closeInput())} onSubmit={_handleSubmitEditGroup} />
              :<Box onClick={() => dispatch(inputActions.openInput({
                'activate': 'group-name-'+forwardItem.id,
                'value': forwardItem.name
              }))} sx={{flexGrow: 1,cursor: 'pointer'}}>
                <strong>{forwardItem.name}</strong> Resource
                {
                  Boolean(forwardItem.default) &&
                  <Chip label={`Default`} size='small' color={'success'} sx={{ml:1,fontSize:11,fontWeight:500}} />
                }
                <IconButton size={'small'} variant={'contained'} sx={{ml:1,backgroundColor: grey[300]}}>
                  <CreateIcon fontSize={'small'} />
                </IconButton>
              </Box>
            }
            <Box>
              {/* <IconButton size={'small'} aria-label="drag handle" onClick={() => setOpen(!open)} sx={{backgroundColor: grey[300]}}>
                {
                  open ? <ExpandLess />: <ExpandMore />
                }
              </IconButton> */}
              <IconButton
                aria-label="more"
                id="long-button"
                aria-haspopup="true"
                onClick={(e) => setMenu(e.currentTarget)}
                size={'small'}
                sx={{backgroundColor: grey[300], ml:1}}
              >
                <MoreVertIcon />
              </IconButton>
              <Menu
                id="demo-positioned-menu"
                aria-labelledby="demo-positioned-button"
                anchorEl={menu}
                open={menuOpen}
                onClose={() => setMenu(null)}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'left',
                }}
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
              >
                {
                  Boolean(defaultGroup?.id !== forwardItem.id) &&
                  <MenuItem onClick={_handleSetdefaultGroup}>
                    <ListItemIcon>
                      <CheckCircleOutlineIcon fontSize="small" />
                    </ListItemIcon>
                    <ListItemText sx={{fontSize: 'small'}}>Set Default</ListItemText>
                  </MenuItem>
                }
                {/* <MenuItem onClick={_handleSetPrefix}>
                  <ListItemIcon>
                    <LabelImportantIcon fontSize="small" />
                  </ListItemIcon>
                  <ListItemText sx={{fontSize: 'small'}}>Prefix</ListItemText>
                </MenuItem> */}
                <MenuItem onClick={_handleOpenAlias}>
                  <ListItemIcon>
                    <LabelImportantIcon fontSize="small" />
                  </ListItemIcon>
                  <ListItemText sx={{fontSize: 'small'}}>Keywords</ListItemText>
                </MenuItem>
                <MenuItem onClick={_handleDeleteItem}>
                  <ListItemIcon>
                    <DeleteOutlineIcon fontSize="small" />
                  </ListItemIcon>
                  <ListItemText sx={{fontSize: 'small'}}>Archive</ListItemText>
                </MenuItem>
              </Menu>
            </Box>
          </Box>
        </>
      }
      >
        {/* <Collapse in={open} timeout="auto"> */}
          <FlowList forwardGroup={forwardItem} />
        {/* </Collapse> */}
        <>
          <ListSubheader component="div" id="nested-list-subheader" sx={{mt:1}}>
            <>         
              <div style={{display: 'flex', alignItems: 'center',justifyContent:'space-between'}}>
                <div style={{display: 'flex', alignItems: 'center'}}>
                  <Button size={'small'} sx={{color:grey[700],backgroundColor:grey[300],mr:1}} onClick={_handleAddFlow}>
                    <AddIcon fontSize={'small'} /> 
                    Add Flow
                  </Button>
                  <Button size={'small'} sx={{color:grey[700],backgroundColor:grey[300]}} onClick={() => alert('help.')}>
                    Help Me
                  </Button>
                </div>
              </div>
            </>
          </ListSubheader>
        </>
      </List>
      <Divider />
      {
        detailName === 'flow-group-keyword-'+forwardItem.id &&
        <Portal>
          <BasicModal>
            <Alias forwardItem={forwardItem} />
          </BasicModal>
        </Portal>
      }
      {
        detailName === 'flow-group-prefix-'+forwardItem.id &&
        <Portal>
          <BasicModal>
            <Prefix forwardItem={forwardItem} />
          </BasicModal>
        </Portal>
      }
    </>
  );
}
