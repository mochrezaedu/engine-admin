import {useEffect} from 'react';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import IconButton from '@mui/material/IconButton';
import DragHandleIcon from '@mui/icons-material/DragHandle';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { Draggable } from 'react-beautiful-dnd';
import { useDispatch, useSelector } from 'react-redux';
import { Box, Button, Chip, Divider, Menu, MenuItem, Paper, Portal, Typography } from '@mui/material';
import BasicModal from '../../components/BasicModal';
import { useMemo } from 'react';
import FlowDetail from './FlowDetail';
import {actions as modalActions,get as get} from '../../services/app/modal/ModalSlicer'
import {actions as inputActions,get as getInput} from '../../services/app/input/InputSlicer'
import EditableText from '../../components/EditableText';
import { useState } from 'react';
import { grey } from '@mui/material/colors';
import flow, {Getter,actions as FlowModel} from '../../services/datacenter/flow'
import {Getter as flowChoicesGetter,actions as flowChoicesActions} from '../../services/datacenter/flow_choices'
import {Getter as flowExtraGetter,actions as flowExtraActions} from '../../services/datacenter/flow_extra'
import {Getter as flowExtraRequiredGetter,actions as flowExtraRequiredActions} from '../../services/datacenter/flow_extra_required'
import CloseIcon from '@mui/icons-material/Close';
import PriorityHighIcon from '@mui/icons-material/PriorityHigh';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import FlowApi from '../../services/api/Flow/api';
import { useNavigate, useParams } from 'react-router-dom';
import FiberManualRecord from '@mui/icons-material/FiberManualRecord';
import { flowNextActions, getFlowNextState } from '../../services/app/engine/FlowNextAction';
import { useDraggableInPortal } from '../../hook/AppHooks';
import WarningIcon from '@mui/icons-material/Warning';

const menus = [
  {
    icon: ''
  }
]

export default function FlowItem(props) {
  const {forwardIndex,forwardItem} = props
  const {applicationId} = useParams()
  const dispatch = useDispatch()
  const renderDraggable = useDraggableInPortal();
  const detailName = useSelector((state) => get(state,'name'))
  const defaultFlow = useSelector((state) => Getter.get(state, (item) => item.flow_group_id === forwardItem.flow_group_id && Boolean(item.default))).first()
  const editMode = useSelector((state) => getInput(state,'activate'));
  const pendingCurrent = useSelector((state) => Getter.isPending(state,forwardItem.id))
  const pendingIds = useSelector(Getter.getPendingIds)
  const failedCurrent = useSelector((state) => Getter.isFailed(state,forwardItem.id))

  const nextMode = useSelector((state) => getFlowNextState(state))
  const _handleSetNextMode = (args) => {
    dispatch(flowNextActions.setNextMode({active: true,source: forwardItem.id, to: null}))
  }
  const _handleExitNextMode = (args) => {
    dispatch(flowNextActions.closeAction())
  }
  const _handleEndNextMode = (args) => {
    console.log('Source: ', nextMode.source)
    console.log('Destination: ', forwardItem.id)

    dispatch(flowNextActions.closeAction())
  }

  const _inputKey = useSelector((state) => getInput(state, 'key'))
  const _inputValue = useSelector((state) => getInput(state, 'value'))

  const navigate = useNavigate()

  const _handleOpenDetail = () => {
    // dispatch(modalActions.openModal({
    //   activate: true,
    //   title: forwardItem.name,
    //   name: 'flow-detail-'+forwardItem.id,
    //   loading: true
    // }))
    navigate(`/application/${applicationId}/flow/${forwardItem.id}/detail`)
  }

  const _handleEditMode = (args) => {
    editMode !== forwardItem.id+args.name && dispatch(inputActions.openInput({activate:'flow-input-'+forwardItem.id+args.name,value:args.value || '',key:args.key}))
  }
  const _handleCloseEditMode = (args) => {
    dispatch(inputActions.closeInput())
  }
  const _handleSubmit = (args) => {
    dispatch(inputActions.closeInput())
    var itemToUpdate = {}
    itemToUpdate[_inputKey] = args
    var payload = {...forwardItem, ...itemToUpdate}
    dispatch(FlowModel.update(payload))
    dispatch(FlowApi.updateSingleAttribute({
      data: {
        flow: {...forwardItem, ...itemToUpdate}
      }
    }))
  }

  const [menu, setMenu] = useState(null);
  const menuOpen = Boolean(menu);
  const _handleOpenMenu = (args) => {
    setMenu(args.currentTarget);
  }
  const _handleCloseMenu = () => {
    setMenu(null);
  }

  const _handleDeleteItem = () => {
    dispatch(FlowModel.delete([forwardItem.id]))
    dispatch(FlowApi.destroy({
      data: {
        flow_id: forwardItem.id
      }
    }))
  }

  const _handleSetDefaultFlow = () => {
    if(defaultFlow) {
      dispatch(FlowModel.update({...defaultFlow, default: null}))
    }

    const existingFlow = {...forwardItem, default: 'yes'}
    dispatch(FlowModel.update(existingFlow))
    dispatch(FlowApi.setDefaultRoute({
      data: {flow_id: forwardItem.id, flow_group_id: forwardItem.flow_group_id}
    }))
    _handleCloseMenu()
  }

  useEffect(() => {
    if(forwardItem.order === null) {
      dispatch(FlowApi.autoOrder({
        data: {flow_group_id: forwardItem.flow_group_id, flow_id: forwardItem.id}
      }))
    }
  }, [])
  
  return (
    <Draggable draggableId={'flow-'+forwardItem.id.toString()} index={forwardIndex}>
        {
          renderDraggable(
            provided => {
              return (
                <Paper
                ref={provided.innerRef}
                {...provided.draggableProps}  
                elevation={0}>
                  <ListItem
                  secondaryAction={
                    <div edge="end">
                      {/* {
                        !forwardItem.next &&
                        <>
                          {
                            !nextMode.active &&
                            <Button size={'small'} onClick={_handleSetNextMode}>Set Next</Button>
                          }
                          {
                            nextMode.active && nextMode.source == forwardItem.id &&
                            <Button size={'small'} onClick={_handleExitNextMode} color={'error'}>Cancel</Button>
                          }
                          {
                            nextMode.active && nextMode.source != forwardItem.id &&
                            <IconButton variant={'outlined'} size={'small'} onClick={_handleEndNextMode}>
                              <FiberManualRecord sx={[{'&:hover': {color:'green'}}]} />
                            </IconButton>
                          }
                        </>
                      } */}
                      {
                        !forwardItem.type &&
                        <IconButton 
                        size={'small'} 
                        sx={{'&:hover': {backgroundColor: 'transparent'},cursor: 'default'}} 
                        title={'This flow cannot be using. Please fill Type'}
                        >
                          <WarningIcon color='error' size={'small'} />
                        </IconButton>
                      }
                      {
                        !pendingCurrent && !failedCurrent && 
                        <>
                          <IconButton
                            aria-label="more"
                            id="long-button"
                            aria-haspopup="true"
                            onClick={_handleOpenMenu}
                            size={'small'}
                            sx={{backgroundColor: grey[200], ml:1}}
                          >
                            <MoreVertIcon fontSize={'small'} />
                          </IconButton>
                          <Menu
                            id="demo-positioned-menu"
                            aria-labelledby="demo-positioned-button"
                            anchorEl={menu}
                            open={menuOpen}
                            onClose={_handleCloseMenu}
                            anchorOrigin={{
                              vertical: 'top',
                              horizontal: 'left',
                            }}
                            transformOrigin={{
                              vertical: 'top',
                              horizontal: 'right',
                            }}
                          >
                            {
                              Boolean(defaultFlow?.id !== forwardItem.id) &&
                              <MenuItem onClick={_handleSetDefaultFlow}>
                                <ListItemIcon>
                                  <CheckCircleOutlineIcon fontSize="small" />
                                </ListItemIcon>
                                <ListItemText sx={{fontSize: 'small'}}>Set Default</ListItemText>
                              </MenuItem>
                            }
                            <MenuItem onClick={_handleDeleteItem}>
                              <ListItemIcon>
                                <DeleteOutlineIcon fontSize="small" />
                              </ListItemIcon>
                              <ListItemText sx={{fontSize: 'small'}}>Archive</ListItemText>
                            </MenuItem>
                          </Menu>
                        </>
                      }
                      {
                        failedCurrent && 
                        <IconButton aria-label="opentab" size={'small'} onClick={_handleDeleteItem} sx={{backgroundColor: grey[300]}}>
                          <CloseIcon fontSize={'small'} />
                        </IconButton>
                      }
                    </div>
                  }
                  disablePadding
                  >
                    <ListItemButton role={undefined} dense>
                      {/* <ListItemIcon>
                        <Checkbox
                          edge="start"
                          // checked={checked.indexOf(value) !== -1}
                          tabIndex={-1}
                          disableRipple
                          // inputProps={{ 'aria-labelledby': labelId }}
                        />
                      </ListItemIcon> */}
                      <ListItemIcon>
                        <IconButton aria-label="drag handle" 
                        disabled={pendingCurrent}
                        {...provided.dragHandleProps} 
                        >
                          {
                            !pendingCurrent && !failedCurrent &&
                            <DragHandleIcon />
                          }
                        </IconButton>
                      </ListItemIcon>
                      <ListItemText 
                      onClick={_handleOpenDetail}
                      primary={
                        <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
                          <div>
                            <Box sx={{mb:1,display:'flex',alignItems:'center',fontWeight: 700,opacity: (failedCurrent || pendingCurrent ? 0.3:1)}}>
                              <div>{`${forwardItem.name ? `${forwardItem.name}`: ''}`}</div>
                              {
                                Boolean(forwardItem.default) &&
                                <Chip label={`Default`} size='small' color={'success'} sx={{ml:1,fontSize:11,fontWeight:500}} />
                              }
                            </Box>
                            {
                              !failedCurrent && !pendingCurrent &&
                              <Box>
                                <div>{forwardItem.response}</div>
                                {
                                  Boolean(forwardItem.type && forwardItem.type !== 'text') &&
                                  <Chip label={`${forwardItem.type}`} size='small' sx={{mt:1}} />
                                }
                              </Box>
                            }
                            {
                              failedCurrent &&
                              <Typography variant="caption" color={'red'}>
                                Failed to create Flow.
                              </Typography>
                            }
                          </div>
                          {/* <Chip label={forwardItem.order} sx={{opacity: (failedCurrent || pendingCurrent ? 0.3:1)}} /> */}
                        </div>
                      } 
                      />
                    </ListItemButton>
                  </ListItem>
                  <Divider />
                </Paper>
              );
            }
          )
        }
      </Draggable>
  );
}

