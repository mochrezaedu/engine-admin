import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  activeGroup: null
}

const methods = {
  setActiveGroup: (state,action) => {
    const {payload} = action
    state.activeGroup = payload
  }
}

export const Engine = createSlice({
  name: 'engine',
  initialState: initialState,
  reducers: methods
})

export default Engine

export const engineActions = Engine.actions
export const engineState = {
  getActiveGroup: (state) => state.engine.activeGroup
}