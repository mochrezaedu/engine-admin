import FlowGroup from "./FlowGroup";
import {DragDropContext} from 'react-beautiful-dnd';
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { Button, Divider, IconButton, InputBase, Menu, MenuItem, Paper, Portal } from "@mui/material";
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import { Box } from "@mui/system";
import { actions as FlowGroupModel, flowGroupGetter } from "../../services/datacenter/flow_group";
import { grey } from "@mui/material/colors";
import { store } from "../../services/api/FlowGroup/api";
import { get as getInput, actions as inputActions } from "../../services/app/input/InputSlicer";
import EditableText from "../../components/EditableText";
import { Uniqid } from "../../helpers/helper";
import { useParams } from "react-router-dom";
import { getFlowGroups } from "../../services/api/Resources";
import FlowExtraRequired from "../../services/api/FlowExtraRequired/api";
import BackComponent from "../../components/BackComponent";
import SelectGroup from "./FlowGroup/SelectGroup";
import BasicModal from "../../components/BasicModal";
import { get as getModal, modalActions } from "../../services/app/modal/ModalSlicer";
import EngineLayout from "../Layout/EngineLayout";
import Empty from "./FlowGroup/Empty";

function ExngineTest() {
  return (
    <div>test</div>
  )
}

export default function Engine() {
  const {applicationId} = useParams()
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const dispatch = useDispatch();  
  const groups = useSelector(flowGroupGetter.all)
  const [selectedGroup,setSelectedGroup] = useState(null)
  const detailName = useSelector((state) => getModal(state,'name'))
  
  const handleClick = (event) => {
    dispatch(modalActions.openModal({
      activate: true,
      title: 'Select Flow Group',
      name: 'flow-group-select',
      loading: false
    }))
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const _handleSelectGroup = (args) => {
    setSelectedGroup(args)
  }

  return (
    <EngineLayout>
      {/* <Paper elevation={0} variant={'outlined'} sx={{display: 'flex', alignItems:'center',flexGrow:1,backgroundColor: 'white', mb:2}}>
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          MenuListProps={{
            'aria-labelledby': 'basic-button',
          }}
        >
          {
            groups.map((item) => (
              <MenuItem key={item.id} onClick={() => _handleSelectGroup(item)}>{item.name}</MenuItem>
            ))
          }
        </Menu>
        <InputBase
          sx={{ ml: 2, flex: 1, flexGrow: 1 }}
          placeholder="Search anityng"
          inputProps={{ 'aria-label': 'search google maps' }}
          disabled={true}
        />
        <IconButton type="button" sx={{ p: '10px' }} aria-label="search">
          <SearchIcon />
        </IconButton>
      </Paper> */}
      {
        Boolean(groups.length > 0) ?
        <Paper variant={'outlined'}><FlowGroup /></Paper>:
        <Empty />
      }
      {
        detailName === 'flow-group-select' &&
        <Portal>
          <BasicModal>
            <SelectGroup />
          </BasicModal>
        </Portal>
      }
    </EngineLayout>
  );
}