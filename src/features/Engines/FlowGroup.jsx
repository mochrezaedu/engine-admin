import {useEffect, useState} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { getStoreState } from '../../helpers/Store';
import FlowExtraRequired from '../../services/api/FlowExtraRequired/api';
import { getFlowGroups } from '../../services/api/Resources';
import { rootLoaderActions } from '../../services/app/loader/RootLoaderSlice';
import { flowGroupGetter, Getter, path } from '../../services/datacenter/flow_group';
import { engineActions, engineState } from './EngineSlicer';
import Empty from './FlowGroup/Empty';
import FlowGroupItem from './FlowGroupItem';

export default function FlowGroup() {
  const dispatch = useDispatch();
  const {applicationId,flowGroupId} = useParams()
  const flowGroups = useSelector((state) => Getter.get(state,(item) => item.application_id == applicationId));
  const status = useSelector((state) => getStoreState(state,path)['status'])
  const activeGroup = useSelector((state) => flowGroupGetter.get(state,(item) => item.id == flowGroupId))

  useEffect(() => {
    if(status === 'pending') {
      dispatch(rootLoaderActions.add('flow_group'))
    }
    if(status === 'idle') {
      dispatch(rootLoaderActions.remove('flow_group'))
    }
  }, [status])

  return (
    <>
      {
        activeGroup.first() &&
        <FlowGroupItem forwardItem={activeGroup.first()} />
      }
    </>
  );
}
