import { Box, Button } from "@mui/material";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import EditableText from "../../components/EditableText";
import { getStoreState } from "../../helpers/Store";
import FlowExtraRequired from "../../services/api/FlowExtraRequired/api";
import FlowTypeApi from "../../services/api/FlowType/api";
import { getInput, inputActions } from "../../services/app/input/InputSlicer";
import { isActive, rootLoaderActions } from "../../services/app/loader/RootLoaderSlice";
import { FlowExtraRequiredActions } from "../../services/datacenter/flow_extra_required";
import { FlowTypeGetter, path } from "../../services/datacenter/flow_type";
import BaseLayout from "../Layout/BaseLayout";
import FlowTypeItem from "./FlowTypeItem";

export default function Index(props) {
  const dispatch = useDispatch()
  const flow_types = useSelector(FlowTypeGetter.all)
  const loading = useSelector((state) => getStoreState(state,path)['status'])
  const inputName = useSelector((state) => getInput(state,'activate'))


  const _handleStoreFlowType = (args) => {
    dispatch(inputActions.closeInput());
    dispatch(FlowTypeApi.store({
      data: {name: args}
    }))
  }

  useEffect(() => {
    dispatch(FlowTypeApi.all())
    dispatch(FlowExtraRequired.all())
  }, [])

  useEffect(() => {
    if(loading === 'pending') {
      dispatch(rootLoaderActions.add('flow_type'))
    }
    if(loading === 'idle') {
      dispatch(rootLoaderActions.remove('flow_type'))
    }
  }, [loading])

  return (
    <BaseLayout>
      <Box>
        {
          flow_types.map((item) => <FlowTypeItem key={item.id} forwardItem={item} />)
        }
        {
          Boolean(inputName === 'create-new-flow-type') &&
          <Box sx={{mb:2}}>
            <EditableText onClose={() => dispatch(inputActions.closeInput())} onSubmit={_handleStoreFlowType} placeholder={'Flow Type Name'} />
          </Box>
        }
        <Box>
          <Button variant={'outlined'} onClick={() => dispatch(inputActions.openInput({activate: 'create-new-flow-type'}))}>Add New Type</Button>
        </Box>
      </Box>
    </BaseLayout>
  )
}