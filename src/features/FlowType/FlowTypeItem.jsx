import { Button, Card, CardActions, CardContent, Collapse, Divider, Grid, IconButton, ImageList, ImageListItem, InputBase, List, ListItemButton, ListItemIcon, ListItemText, MenuItem, Paper, Select, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import StarBorder from '@mui/icons-material/StarBorder';
import { Box } from "@mui/system";
import { useDispatch } from "react-redux";
import FlowType from "../../services/api/FlowType/api";
import { FlowExtraRequiredActions, FlowExtraRequiredGetter } from "../../services/datacenter/flow_extra_required";
import { useSelector } from "react-redux";
import CreateIcon from '@mui/icons-material/Create';
import { grey } from "@mui/material/colors";
import { get as getInput, inputActions } from "../../services/app/input/InputSlicer";
import TextareaAutosize from '@mui/base/TextareaAutosize';
import { FileUploader } from "react-drag-drop-files";
import ArchiveIcon from '@mui/icons-material/Archive';
import FlowExtraRequired from "../../services/api/FlowExtraRequired/api";
import { getStoreState } from "../../helpers/Store";
import { rootLoaderActions } from "../../services/app/loader/RootLoaderSlice";
import { Uniqid } from "../../helpers/helper";
import CloseIcon from '@mui/icons-material/Close';
import FlowTypeApi from "../../services/api/FlowType/api";
import { FlowTypeAction } from "../../services/datacenter/flow_type";
import ConfirmationDialog from "../../components/ConfirmationDialog";
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import VisibilityIcon from '@mui/icons-material/Visibility';

const RenderEditTypeImage = (props) => {
  const {forwardItem, onSubmit} = props
  const [file,setFile] = useState(forwardItem?.example_image)
  const [preview,setPreview] = useState(forwardItem?.example_image)
  const dispatch = useDispatch()

  const _handlePreviewChange = (args) => {
    setFile(args)
  }

  const _handleSubmit = () => {
    onSubmit(file,preview)
    dispatch(inputActions.closeInput())
  }

  const _handleClose = () => {
    dispatch(inputActions.closeInput())
  }

  useEffect(() => {
    if(file && file instanceof File) {
      setPreview(URL.createObjectURL(file))
    }
  }, [file])

  return (
    <Box>
      <Box sx={{display:'flex',alignItems:'center',mb:1}}>
        <Box sx={{border:'1px solid '+grey[300],px:2,py:1,my:1}}>
          <FileUploader onDrop={_handlePreviewChange} handleChange={_handlePreviewChange} name="file" types={["JPG", "PNG", "GIF"]}>
            <Typography variant={'caption'}>
              {file?.name || 'Drag file here or Klik to Upload'}
            </Typography>
          </FileUploader>
        </Box>
      </Box>
      <Box>
        <Button variant={'contained'} size={'small'} color={'success'} onClick={_handleSubmit} disabled={!file}>Simpan</Button>
        <Button variant={'text'} size={'small'} color={'inherit'} 
        onClick={_handleClose} 
        sx={{'&:hover': {backgroundColor: 'transparent'}}}>Batal</Button>
      </Box>
    </Box>
  )
}

const RenderEditRequiredExtra = (props) => {
  const {item,onSubmit} = props
  const [key,setKey] = useState(item?.key || '')
  const [description,setDescription] = useState(item?.description || '')
  const [files,setFiles] = useState(null)
  const [type,setType] = useState(item?.type || '')
  const [preview,setPreview] = useState(item?.example_image_url)
  const [submittable,setSubmittable] = useState(false)
  const dispatch = useDispatch()
  const options = ['text','choice','image','images']

  const _handlePreviewChange = (args) => {
    setFiles(args)
  }

  const _handleSubmit = () => {
    onSubmit({key: key, description: description,type: type,file: files,type,preview})
    dispatch(inputActions.closeInput())
  }

  const _handleClose = () => {
    dispatch(inputActions.closeInput())
  }

  useEffect(() => {
    if(files && files instanceof File) {
      setPreview(URL.createObjectURL(files))
    }
  }, [files])

  useEffect(() => {
    if(key && description && preview) {
      setSubmittable(true)
    } else {
      setSubmittable(false)
    }
  }, [key,description,preview])

  return (
    <Grid item xs={4} sx={{mb:2}}>
      <Box sx={{border: '1px solid '+grey[300],p:1}}>
        <Box sx={{mb:1}}>
          <Typography variant={'subtitle2'}>Name</Typography>
          <InputBase sx={{width:'100%',border: '1px solid '+grey[300], px:1}} value={key} onChange={(e) => setKey(e.target.value)} />
        </Box>
        <Box>
          <Typography variant={'subtitle2'}>Description (Help)</Typography>
          <textarea 
          rows={4}
          style={{resize: 'none',width:'100%',border: '1px solid '+grey[300], padding:8}} 
          value={description}
          onChange={(e) => setDescription(e.target.value)} />
        </Box>
        <Box>
          <Typography variant={'subtitle2'}>Type</Typography>
          <Select
            labelId="demo-customized-select-label"
            sx={{ px:2,flex: 1,backgroundColor:'white',border: '1px solid '+grey[300], mb:2,flex:1,width:'100%',fontSize: 14}}
            inputProps={{ 'aria-label': 'Without label' }}
            id="demo-customized-select"
            value={type}
            onChange={(e) => {
              setType(e.target.value)
            }}
            input={<InputBase />}
          >
            <MenuItem disabled value="">
              <em>None</em>
            </MenuItem>
            {
              options?.map((item) => {
                return (
                  <MenuItem key={item} value={item} selected={Boolean(type === item)}>{item}</MenuItem>
                )
              })
            }
          </Select>
        </Box>
        <Box sx={{mb:2}}>
          <Typography variant={'subtitle2'}>Preview</Typography>
          <Box sx={{p:1,border: '1px solid '+grey[300], width: '100%', display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
            {
              !preview &&
              <FileUploader onDrop={_handlePreviewChange} handleChange={_handlePreviewChange} name="file" types={["JPG", "PNG", "GIF"]}>
                <Typography variant={'caption'}>
                  {files?.name || 'Drag file here or Klik to Upload'}
                </Typography>
              </FileUploader>
            }
            {
              preview &&
              <>
                <ImageListItem>
                  <img
                    src={`${preview}`}
                    loading="lazy"
                  />
                </ImageListItem>
                <IconButton 
                title={'Remove Input'}
                size={'small'} variant={'text'} sx={{ml:1}} onClick={() => {
                  setFiles(null)
                  setPreview(null)
                }}>
                  <CloseIcon fontSize={'small'} />
                </IconButton>
              </>
            }
          </Box>
        </Box>
        <Box>
          <Button 
          variant={'contained'} 
          size={'small'} 
          color={'success'}
          onClick={_handleSubmit}>Simpan</Button>
          <Button variant={'text'} size={'small'} color={'inherit'} 
          onClick={_handleClose} 
          sx={{'&:hover': {backgroundColor: 'transparent'}}}>Batal</Button>
        </Box>
      </Box>
    </Grid>
  )
}

export default function FlowTypeItem(props) {
  const {forwardItem} = props
  const [open, setOpen] = useState(true)
  const requiredExtras = useSelector((state) => FlowExtraRequiredGetter.get(state, (item) => item.flow_type_id === forwardItem.id))
  const dispatch = useDispatch()
  const editMode = useSelector((state) => getInput(state,'activate'))
  const prefix = 'edit-flow-type-'+forwardItem.id+'-item-'
  const [openConfirmDeleteDialog,setOpenConfirmDeleteDialog] = useState(false)

  const _handleEditMode = (args) => {
    dispatch(inputActions.openInput({activate:prefix+args}))
  }

  const _handleUpdateTypeImage = (args,preview) => {
    dispatch(FlowTypeAction.update({...forwardItem,example_image:preview}))
    const payload = {flow_type_id: forwardItem.id,image:args}
    dispatch(FlowTypeApi.updateExampleImage({
      data: payload
    }))
  }
  const _handleDeleteFlowType = () => {
    dispatch(FlowTypeAction.delete([forwardItem.id]))
    dispatch(FlowTypeApi.destroy({
      data: {flow_type_id: forwardItem.id}
    }))
  }

  const _handleUpdate = (args, updated) => {
    const uri = (updated.file) ? (updated.file instanceof File ? URL.createObjectURL(updated.file): updated.file): null
    const payload = {
      ...args, 
      key: updated.key, 
      description: updated.description, 
      image: updated.file,
      example_image: args.example_image,
      example_image_url: updated.preview,
      flow_type_id:forwardItem.id,
      type:updated.type
    }
    dispatch(FlowExtraRequiredActions.update({...payload,image: null}))
    dispatch(FlowExtraRequired.update({
      data: {...payload, flow_type_required_id:payload.id}
    }))
  }
  const _handleCreate = (args) => {
    const uri = (args.file) ? (args.file instanceof File ? URL.createObjectURL(args.file): args.file): null
    const payload = {
      id: Uniqid(), 
      type_name: forwardItem.name,
      key: args.key, 
      description: args.description, 
      image: uri,
      example_image: uri,
      flow_type_id:forwardItem.id,
      type:args.type
    }
    dispatch(FlowExtraRequiredActions.createEmpty(payload))
    dispatch(FlowExtraRequired.store({
      data: {...payload,image: args.file}
    }))
  }
  const _handleDelete = (args) => {
    dispatch(FlowExtraRequiredActions.delete([args.id])) 
    dispatch(FlowExtraRequired.destroy({
      data:{flow_type_required_id:args.id}
    }))
  }

  const _handleDisableFlowType = () => {
    dispatch(FlowTypeApi.toggleFlowTypeUsable({
      data: {flow_type_id: forwardItem.id}
    }))
  }

  return (
    <Paper sx={{mb:2}}>
      <ListItemButton>
        <ListItemText primary={`${forwardItem.name}`} />
        {
          forwardItem.disabled &&
          <IconButton onClick={() => _handleDisableFlowType()} title={'Enable Type'}>
            <VisibilityIcon />
          </IconButton>
        }
        {
          !forwardItem.disabled && 
          <IconButton onClick={() => _handleDisableFlowType()} title={'Disable Type'}>
            <VisibilityOffIcon />
          </IconButton>
        }
        <IconButton onClick={() => setOpenConfirmDeleteDialog(true)} title={'Archive'}>
          <ArchiveIcon />
        </IconButton>
        <ConfirmationDialog 
        title={'Delete Type ?'}
        description={'Apakah anda yakin ingin menghapus ??'}
        open={openConfirmDeleteDialog}
        onConfirm={_handleDeleteFlowType} 
        onClose={() => setOpenConfirmDeleteDialog(false)} />
        <IconButton onClick={() => setOpen(!open)}>
          {open ? <ExpandLess /> : <ExpandMore />}
        </IconButton>
      </ListItemButton>
      {
        open &&
        <Divider />
      }
      <Collapse in={open} timeout="auto">
        <Box sx={{px:2,pt:1}}>
          <Box sx={{display: 'flex', alignItems:'center'}}>
            <Typography variant={'subtitle2'}>Image Examples</Typography>
            {
              editMode !== prefix+'edit-examples' &&
              <IconButton 
              onClick={() => _handleEditMode('edit-examples')}
              size={'small'} variant={'contained'} sx={{ml:1,backgroundColor: grey[300]}}>
                <CreateIcon fontSize={'small'} />
              </IconButton>
            }
          </Box>
          {
            editMode === prefix+'edit-examples' ?
            <RenderEditTypeImage forwardItem={forwardItem} onSubmit={_handleUpdateTypeImage} />:
            (
              forwardItem.example_image &&
              <ImageList cols={3}>
                <ImageListItem>
                  <img
                    src={`${forwardItem.example_image}`}
                    srcSet={`${forwardItem.example_image}`}
                    alt={forwardItem.name}
                    loading="lazy"
                  />
                </ImageListItem>
              </ImageList>
            )
          }
        </Box>
        {
          <Box sx={{px:2,py:2}}>
            <Box sx={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', mb:1}}>
              <Typography variant={'subtitle2'}>Master Data</Typography>
              <Button variant={'contained'} size={'small'} color={'primary'} onClick={() => _handleEditMode('create')}>Add</Button>
            </Box>
            <Grid container spacing={2}>
            {
              requiredExtras.map((item) => {
                return (
                  editMode === prefix+item.id ?
                  <RenderEditRequiredExtra key={item.id} item={item} onSubmit={(e) => _handleUpdate(item, e)} />:
                  <Grid item xs={4} sx={{mb:2}} key={item.id}>
                    <Card variant={'outlined'}>
                      <CardContent>
                        <Box sx={{display: 'flex',alignItems:'center',justifyContent: 'space-between'}}>
                          <Typography sx={{ fontSize: 14 }} variant="h6" gutterBottom>
                            {item.key}
                          </Typography>
                          <Box>
                            <IconButton 
                            onClick={() => _handleEditMode(item.id)}
                            size={'small'} variant={'contained'} sx={{ml:1,backgroundColor: grey[300]}}>
                              <CreateIcon fontSize={'small'} />
                            </IconButton>
                            <IconButton 
                            onClick={() => _handleDelete(item)}
                            size={'small'} variant={'contained'} sx={{ml:1,backgroundColor: grey[300]}}>
                              <ArchiveIcon fontSize={'small'} />
                            </IconButton>
                          </Box>
                        </Box>
                        <Typography sx={{ mb: 1.5 }} variant={'body2'}>
                          {item.description}
                        </Typography>
                        {
                          item.example_image_url &&
                          <ImageListItem>
                            <img
                              src={`${item.example_image_url}`}
                              loading="lazy"
                            />
                          </ImageListItem>
                        }
                      </CardContent>
                    </Card>
                  </Grid>
                )
              })
            }
            {
              editMode === prefix+'create' &&
              <RenderEditRequiredExtra onSubmit={_handleCreate} />
            }
            </Grid>
          </Box>
        }
      </Collapse>
    </Paper>
  )
}

const itemData = [
  {
    img: 'https://images.unsplash.com/photo-1551963831-b3b1ca40c98e',
    title: 'Breakfast',
  },
  {
    img: 'https://images.unsplash.com/photo-1551782450-a2132b4ba21d',
    title: 'Burger',
  },
  {
    img: 'https://images.unsplash.com/photo-1522770179533-24471fcdba45',
    title: 'Camera',
  }
]; 