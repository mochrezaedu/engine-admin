import {useCallback, useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import MailIcon from '@mui/icons-material/Mail';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import LayoutBar from './LayoutBar';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import MeetingRoomIcon from '@mui/icons-material/MeetingRoom';
import { Collapse, LinearProgress } from '@mui/material';
import { ExpandLess, ExpandMore } from '@mui/icons-material';
import { Uniqid } from '../../helpers/helper';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import AlignVerticalCenterIcon from '@mui/icons-material/AlignVerticalCenter';
import UpcomingIcon from '@mui/icons-material/Upcoming';
import SmartToyIcon from '@mui/icons-material/SmartToy';
import DashboardIcon from '@mui/icons-material/Dashboard';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import { useLocation, useMatch, useNavigate } from 'react-router-dom';
import LaptopMacIcon from '@mui/icons-material/LaptopMac';
import { authActions } from '../../services/app/Auth';
import { useDispatch } from 'react-redux';
import BackComponent from '../../components/BackComponent';

const drawerWidth = 240;

const RenderMenuItem = (props) => {
  const {menu,expand} = props
  const [open,setOpen] = useState(expand)

  return (
    <>
      <ListItemButton onClick={() => {
        if(Boolean(menu.subs?.length > 0)) {
          setOpen(!open)          
        }
        if(menu.onClick instanceof Function) {
          menu.onClick()
        }
      }}>
        {
          Boolean(menu.icon) &&
          <ListItemIcon>
            {menu.icon}
          </ListItemIcon>
        }
        {
          Boolean(menu.subs?.length > 0) &&
          <ListItemIcon>
            {(open ? <ExpandMore /> : <ChevronRightIcon />)}
          </ListItemIcon>
        }
        <ListItemText primary={menu.text} />
      </ListItemButton>
      {
        Boolean(menu.subs?.length > 0) &&
        <Collapse in={open} timeout="auto">
          <List component="div" sx={{pl:2}} disablePadding>
            {
              menu.subs?.map((item) => {
                return (
                  <ListItemButton key={item.id} onClick={item.onClick}>
                    <ListItemIcon>
                      {item.icon || ''}
                    </ListItemIcon>
                    <ListItemText primary={item.text} />
                  </ListItemButton>
                )
              })
            }
          </List>
        </Collapse>
      }
    </>
  )
}

function BaseLayout(props) {
  const { window, children } = props;
  const [mobileOpen, setMobileOpen] = useState(false);
  const navigator = useNavigate()
  const _navigate = useCallback((args) => navigator(args, {replace:true}), [navigator])
  const dispatch = useDispatch()
  const {pathname} = useLocation()

  const [menus] = useState([
    {id: Uniqid(), text: 'Dashboard', icon: <DashboardIcon />, onClick: () => _navigate('/')},
    {id: Uniqid(), text: 'Apps', icon: <LaptopMacIcon />, onClick: () => _navigate('/application')},
    {id: Uniqid(), text: 'Flow Resources', subs: [
      // {id: Uniqid(), text: 'Flow Master', icon: <AccountTreeIcon />, onClick: () => _navigate('/flow')},
      {id: Uniqid(), text: 'Type', icon: <AlignVerticalCenterIcon />, onClick: () => _navigate('/flow/type')}
    ]},
    {id: Uniqid(), text: 'Data Center', subs: [
      {id: Uniqid(), text: 'Conversations', icon: <InboxIcon />},
      {id: Uniqid(), text: 'Collected Inputs', icon: <UpcomingIcon />}
    ]},
    {id: Uniqid(), text: 'Bot Account', icon: <SmartToyIcon />, onClick: () => _navigate('/bots')},
    {id: Uniqid(), text: 'Logout', icon: <MeetingRoomIcon />, onClick: () => dispatch(authActions.logout())}
  ])

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  useEffect(() => {
    // console.log(menus)
  }, [])

  const drawer = (
    <div>
      <Toolbar>
        <Typography variant="h6" noWrap component="div">
          Upscale
        </Typography>
      </Toolbar>
      <Divider />
      <List>

        {
          menus.map((item) => <RenderMenuItem 
          key={item.id} 
          menu={item}
          expand={item.text === 'Flow Resources'} />)
        }

      </List>
      <Divider />
      {/* <List>
        {['All mail', 'Trash', 'Spam'].map((text, index) => (
          <ListItem key={text} disablePadding>
            <ListItemButton>
              <ListItemIcon>
                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
              </ListItemIcon>
              <ListItemText primary={text} />
            </ListItemButton>
          </ListItem>
        ))}
      </List> */}
    </div>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <LayoutBar drawer={mobileOpen} setDrawer={handleDrawerToggle} />
      <Box
        component="nav"
        sx={{ width: { md: drawerWidth }, flexShrink: { md: 0 } }}
        aria-label="mailbox folders"
      >
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: 'block', md: 'none' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
        >
          {drawer}
        </Drawer>
        <Drawer
          variant="permanent"
          sx={{
            display: { xs: 'none', md: 'block' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
          open
        >
          {drawer}
        </Drawer>
      </Box>
      <Box
        component="main"
        sx={{ flexGrow: 1, p: 3, width: { xs: '100%', md: `calc(100% - ${drawerWidth}px)` } }}
      >
        <Toolbar />
        {children}
      </Box>
    </Box>
  );
}

BaseLayout.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default BaseLayout;
