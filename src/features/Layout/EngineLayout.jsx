import {useCallback, useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import LayoutBar from './LayoutBar';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import MeetingRoomIcon from '@mui/icons-material/MeetingRoom';
import { Button, Chip, Collapse, InputBase, LinearProgress, Portal, Stack } from '@mui/material';
import { ExpandLess, ExpandMore } from '@mui/icons-material';
import { Uniqid } from '../../helpers/helper';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import AlignVerticalCenterIcon from '@mui/icons-material/AlignVerticalCenter';
import UpcomingIcon from '@mui/icons-material/Upcoming';
import SmartToyIcon from '@mui/icons-material/SmartToy';
import DashboardIcon from '@mui/icons-material/Dashboard';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import { Link, useLocation, useMatch, useNavigate, useParams } from 'react-router-dom';
import LaptopMacIcon from '@mui/icons-material/LaptopMac';
import { authActions } from '../../services/app/Auth';
import { useDispatch } from 'react-redux';
import BackComponent from '../../components/BackComponent';
import { getInput, inputActions } from '../../services/app/input/InputSlicer';
import { actions as FlowGroupModel, flowGroupGetter, Getter } from '../../services/datacenter/flow_group';
import { useSelector } from 'react-redux';
import EditableText from '../../components/EditableText';
import { store } from '../../services/api/FlowGroup/api';
import { get as getModal, modalActions } from '../../services/app/modal/ModalSlicer';
import BasicModal from '../../components/BasicModal';
import SelectGroup from '../Engines/FlowGroup/SelectGroup';
import CreateGroup from '../Engines/FlowGroup/CreateGroup';
import FlowExtraRequired from '../../services/api/FlowExtraRequired/api';
import { getFlowGroups } from '../../services/api/Resources';
import { engineActions, engineState } from '../Engines/EngineSlicer';
import { grey, indigo } from '@mui/material/colors';
import MediationIcon from '@mui/icons-material/Mediation';
import LaunchIcon from '@mui/icons-material/Launch';
import AddIcon from '@mui/icons-material/Add';
import SearchIcon from '@mui/icons-material/Search';
import CloseIcon from '@mui/icons-material/Close';

const drawerWidth = 240;

function ClickComponent(props) {
  const {menu,setOpen,open} = props

  const _handleClick = () => {
    if(Boolean(menu.subs?.length > 0)) {
      setOpen(!open)          
    }
    if(menu.onClick instanceof Function) {
      menu.onClick()
    }
  }

  return (
    menu.to ?
    <Link to={menu.to} target={menu.newTab?'_blank':''}>
      <ListItemButton>
        {
          Boolean(menu.icon) &&
          <ListItemIcon sx={{minWidth:'32px'}}>
            {menu.icon}
          </ListItemIcon>
        }
        {
          Boolean(menu.subs?.length > 0) &&
          <ListItemIcon sx={{minWidth:'32px'}}>
            {(open ? <ExpandMore /> : <ChevronRightIcon />)}
          </ListItemIcon>
        }
        <ListItemText primary={menu.text} />
        {
          Boolean(menu.iconEnd) &&
          menu.iconEnd
        }
      </ListItemButton>
    </Link>:
    <ListItemButton>
      {
        Boolean(menu.icon) &&
        <ListItemIcon sx={{minWidth:'32px'}}>
          {menu.icon}
        </ListItemIcon>
      }
      {
        Boolean(menu.subs?.length > 0) &&
        <ListItemIcon sx={{minWidth:'32px'}} onClick={_handleClick}>
          {(open ? <ExpandMore /> : <ChevronRightIcon />)}
        </ListItemIcon>
      }
      <ListItemText primary={menu.text} onClick={_handleClick} />
      {
        Boolean(menu.iconEnd) &&
        menu.iconEnd
      }
    </ListItemButton>
  )
}

const RenderMenuGroup = (props) => {
  const {applicationId} = useParams()
  const groups = useSelector((state) => Getter.get(state,(item) => item.application_id == applicationId))
  const groupsCount = groups.length
  const navigator = useNavigate()
  const _navigate = useCallback((args) => navigator(args, {replace:true}), [navigator])
  const dispatch = useDispatch()

  const _handleCreateFlowGroup = () => {
    dispatch(modalActions.openModal({
      activate: true,
      title: 'Create Group',
      name: 'flow-group-create',
      loading: false
    }))
  }

  const [menu,setMenu] = useState({})
  const [open,setOpen] = useState(true)
  const [searchMode,setSearchMode] = useState(null)
    
  const _handleToggleSearchMode = (args) => {
    setSearchMode(args)
  }

  const _reMenu = () => {
    setMenu({
      id: 'flow_group_menu', 
      text: 'Flow Groups', subs: [groups.length], 
      expand: true,
      iconEnd: 
      <Stack direction={'row'} spacing={1}>
        <IconButton size={'small'} sx={{bgcolor:grey[200]}} onClick={_handleCreateFlowGroup}>
          <AddIcon fontSize={'small'} />
        </IconButton>
        {
          !searchMode &&
          <IconButton size={'small'} sx={{bgcolor:grey[200]}} onClick={() => _handleToggleSearchMode(true)}>
            <SearchIcon fontSize={'small'} />
          </IconButton>
        }
        {
          searchMode &&
          <IconButton size={'small'} sx={{bgcolor:grey[200]}} onClick={() => _handleToggleSearchMode(false)}>
            <CloseIcon fontSize={'small'} />
          </IconButton>
        }
      </Stack>
    })
  }

  const [search,setSearch] = useState('')
  const [filteredGroups,setFilteredGroups] = useState([])
  const _handleSearchGroups = () => {

  }

  useEffect(() => {
    _reMenu()
    if(!searchMode) {
      setFilteredGroups([...groups])
    }
  }, [searchMode])

  useEffect(() => {
    setFilteredGroups([...groups])
  }, [groupsCount])

  useEffect(() => {
    if(!search) {
      setFilteredGroups([...groups])
    } else {
      const indexesGroup = groups.filter((item) => {
        return item.name.toLowerCase().includes(search.toLowerCase())
      })
      setFilteredGroups([...indexesGroup])
    }
  }, [search])

  return (
    <>
      <ClickComponent menu={menu} open={open} setOpen={setOpen} />
      {
        searchMode &&
        <Box sx={{px:2,pl:4}}>
          <InputBase
            sx={{ px:2, width:'100%', border: '1px solid '+grey[300], borderRadius: '999px', my:1}}
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            placeholder={'Search'}
          />
        </Box>
      }
      {
        Boolean(groups?.length > 0) &&
        <Collapse in={open} timeout="auto">
          <List component="div" sx={{ml:3}} dense disablePadding>
            {
              filteredGroups?.map((item) => {
                return (
                  <ListItemButton key={item.id} onClick={() => _navigate(`/application/${applicationId}/g/${item.id}/flow`)}>
                    <ListItemIcon sx={{minWidth:'32px'}}>
                      <MediationIcon fontSize="small" />
                    </ListItemIcon>
                    <ListItemText primary={item.name} />
                    {
                      item.default &&
                      <Chip size={'small'} label="D" color="success" />
                    }
                  </ListItemButton>
                )
              })
            }
          </List>
        </Collapse>
      }
    </>
  )
}

const RenderMenuItem = (props) => {
  const {menu,expand} = props
  const [open,setOpen] = useState(expand)

  return (
    <>
      <ClickComponent menu={menu} open={open} setOpen={setOpen} />
      {
        Boolean(menu.subs?.length > 0) &&
        <Collapse in={open} timeout="auto">
          <List component="div" sx={{ml:3}} dense disablePadding>
            {
              menu.subs?.map((item) => {
                return (
                  <ListItemButton key={item.id} onClick={item.onClick}>
                    <ListItemIcon sx={{minWidth:'32px'}}>
                      {item.icon || ''}
                    </ListItemIcon>
                    <ListItemText primary={item.text} />
                    {
                      Boolean(item.iconEnd) &&
                      item.iconEnd
                    }
                  </ListItemButton>
                )
              })
            }
          </List>
        </Collapse>
      }
    </>
  )
}

function EngineLayout(props) {
  const { window, children } = props;
  const [mobileOpen, setMobileOpen] = useState(false);
  const navigator = useNavigate()
  const _navigate = useCallback((args) => navigator(args, {replace:true}), [navigator])
  const dispatch = useDispatch()
  const {pathname} = useLocation()
  const inputName = useSelector((state) => getInput(state, 'activate'))
  const {applicationId,flowGroupId} = useParams()
  const groups = useSelector((state) => Getter.get(state,(item) => item.application_id == applicationId))
  const detailName = useSelector((state) => getModal(state,'name'))
  const activeGroup = useSelector(engineState.getActiveGroup)
  const [search,setSearch] = useState('')

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const [menus,setMenus] = useState([
    {id: Uniqid(), text: 'Dashboard', icon: <DashboardIcon fontSize="small" />},
    {
      id: Uniqid(), text: 'Live Chat', icon: <LaptopMacIcon fontSize="small" />, 
      iconEnd:
      <ListItemIcon sx={{minWidth: 'auto', color: indigo[500]}}>
        <LaunchIcon fontSize="small" />          
      </ListItemIcon>,
      onClick: () => {}, to: `/application/${applicationId}/live_chat`, newTab: true
    }
  ])

  useEffect(() => {
    dispatch(FlowExtraRequired.all())
  }, [])

  useEffect(() => {
    if(groups.length === 0) {
      dispatch(getFlowGroups())
    }
  }, [])

  // useEffect(() => {
  // }, [groupsLength])



  const drawer = (
    <div>
      <Toolbar>
        <Typography variant="h6" noWrap component="div">
          Engine
        </Typography>
      </Toolbar>

      {/* <Divider />
      <Box sx={{width: '100%',display: 'flex', justifyContent: 'flex-start',px: 2,py:2}}>
        <Button variant="contained" sx={{my: 1,width: '100%'}} size={'small'} onClick={() => {}}>Dashboard</Button>
      </Box>

      <Divider />
      <Box sx={{width: '100%',display: 'flex', justifyContent: 'flex-start',px: 2,py:2}}>
        <Button variant="contained" sx={{my: 1,width: '100%'}} size={'small'} onClick={() => {}}>Live Chats</Button>
      </Box>

      <Divider />
      <Box sx={{width: '100%',display: 'flex', justifyContent: 'flex-start',px: 2,py:2}}>
        <Button variant="contained" sx={{my: 1,width: '100%'}} size={'small'} onClick={_handleCreateFlowGroup}>Add Group</Button>
      </Box>
      <Box sx={{width: '100%',display: 'flex', justifyContent: 'flex-start',px: 2,py:0, mb:2}}>
        <InputBase
          sx={{ px: 1,flex: 1, flexGrow: 1,cursor: 'pointer', border: '1px solid '+grey[300], borderRadius: '4px'}}
          value={search}
          onChange={(e) => {
            setSearch(e.target.value)
          }}
          placeholder={'Search'}
        />
      </Box>
      <Divider /> */}
      <List sx={{py:0}} dense>
        {
          menus.map((item) => <RenderMenuItem 
          key={item.id} 
          menu={item}
          activeId={flowGroupId}
          expand={item.expand} />)
        }
        <RenderMenuGroup />
      </List>

      {/* <Divider />
      <Box sx={{width: '100%',display: 'flex', justifyContent: 'flex-start',px: 2,py:2}}>
        <Button variant="contained" sx={{my: 1,width: '100%'}} size={'small'} onClick={() => {}}>Add Prefix</Button>
      </Box>
      <Box sx={{width: '100%',display: 'flex', justifyContent: 'flex-start',px: 2,py:0, mb:2}}>
        <InputBase
          sx={{ px: 1,flex: 1, flexGrow: 1,cursor: 'pointer', border: '1px solid '+grey[300], borderRadius: '4px'}}
          defaultValue={''}
          placeholder={'Search'}
        />
      </Box> */}

      {/* <Divider />
      <List sx={{py:0}} dense>
        {
          menus.map((item) => <RenderMenuItem 
          key={item.id} 
          menu={item}
          activeId={flowGroupId}
          expand={item.text === 'Flow Resources'} />)
        }
      </List> */}

      {/* <Divider />
      <List>
        {['All mail', 'Trash', 'Spam'].map((text, index) => (
          <ListItem key={text} disablePadding>
            <ListItemButton>
              <ListItemIcon>
                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
              </ListItemIcon>
              <ListItemText primary={text} />
            </ListItemButton>
          </ListItem>
        ))}
      </List> */}
    </div>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <LayoutBar drawer={mobileOpen} setDrawer={handleDrawerToggle} />
      <Box
        component="nav"
        sx={{ width: { md: drawerWidth }, flexShrink: { md: 0 } }}
        aria-label="mailbox folders"
      >
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: 'block', md: 'none' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
        >
          {drawer}
        </Drawer>
        <Drawer
          variant="permanent"
          sx={{
            display: { xs: 'none', md: 'block' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
          open
        >
          {drawer}
        </Drawer>
      </Box>
      <Box
        component="main"
        sx={{ flexGrow: 1, p: 3, width: { xs: '100%', md: `calc(100% - ${drawerWidth}px)` } }}
      >
        <Toolbar />
        {children}
        {
          detailName === 'flow-group-create' &&
          <Portal>
            <BasicModal>
              <CreateGroup />
            </BasicModal>
          </Portal>
        }
      </Box>
    </Box>
  );
}

EngineLayout.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default EngineLayout;
