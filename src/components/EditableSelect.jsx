import { Box, Button, InputBase, MenuItem, OutlinedInput, Select } from "@mui/material";
import { grey } from "@mui/material/colors";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {actions,get} from '../services/app/input/InputSlicer'

export default function EditableSelect(props) {
  const {onClose,onSubmit,options} = props;
  if(!(onClose instanceof Function)) {
    throw 'Action close required.'
  }

  const dispatch = useDispatch()
  const value = useSelector((state) => get(state,'value'))

  return (
    <Box sx={{mt:1}}>
      {/* <InputBase
        sx={{ px:2,flex: 1,backgroundColor:grey[200],mb:2,flex:1,width:'100%' }}
        placeholder=""
        inputProps={{ 'aria-label': '' }}
        value={value}
        onChange={(e) => dispatch(actions.setValue(e.target.value))}
        onKeyUp={_handleUserSubmitting}
      /> */}
      <Select
        labelId="demo-customized-select-label"
        sx={{ px:2,flex: 1,backgroundColor:'white', borderRadius: '4px',mb:2,flex:1,width:'100%',fontSize: 14}}
        inputProps={{ 'aria-label': 'Without label' }}
        id="demo-customized-select"
        value={value}
        onChange={(e) => {
          dispatch(actions.setValue(e.target.value))
        }}
        input={<InputBase />}
      >
        <MenuItem disabled value="">
          <em>None</em>
        </MenuItem>
        {
          options?.map((item) => {
            return (
              <MenuItem key={item} value={item} selected={Boolean(value === item)}>{item}</MenuItem>
            )
          })
        }
      </Select>
      <Box>
        <Button variant={'contained'} size={'small'} color={'success'} onClick={() => onSubmit(value)}>Simpan</Button>
        <Button variant={'text'} size={'small'} color={'inherit'} onClick={onClose} sx={{'&:hover': {backgroundColor: 'transparent'}}}>Batal</Button>
      </Box>
    </Box>
  )
}