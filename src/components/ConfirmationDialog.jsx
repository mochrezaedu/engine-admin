import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function ConfirmationDialog(props) {
  const {title,description,open,onClose,onConfirm} = props
  const _handleClose = () => {
    onClose()
  };
  const _handleConfirm = () => {
    onClose()
    onConfirm()
  };

  return (
    <div>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={_handleClose}
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle>{title || ''}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            {description || ''}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={_handleClose}>No</Button>
          <Button onClick={_handleConfirm}>Yes</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
