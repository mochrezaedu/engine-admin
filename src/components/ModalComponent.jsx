import { useDispatch, useSelector } from 'react-redux';
import Backdrop from '@mui/material/Backdrop';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import { Box, Chip, Divider, Fab, IconButton, LinearProgress, ListItem, Paper, Typography } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { grey } from '@mui/material/colors';
import {actions,get} from '../services/app/modal/ModalSlicer'
import { useEffect, useState } from 'react';

export default function ModalComponent(props) {  
  const {titleAction,open,onClose} = props
  const TitleText = useSelector((state) => get(state,'title'))
  const Title = props?.title
  const loading = useSelector((state) => get(state,'loading'))

  const _handleClose = () => {
    onClose()
  }

  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      open={open}
      onClose={_handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 100,
      }}
    >
      <Paper sx={style} variant={'outlined'}>
        {/* {loading && <LinearProgress />} */}
        <Box sx={titleStyle}>
          <Typography id="modal-modal-title" variant="h6" component="h2" sx={{width: '100%'}}>
            {
              Title || TitleText
            }
          </Typography>
          {
            titleAction &&
            titleAction
          }
          <IconButton aria-label="opentab" onClick={_handleClose} sx={{backgroundColor: grey[300], ml:1}}>
            <CloseIcon fontSize={'small'} />
          </IconButton>
        </Box>
        {/* <Divider variant="middle" sx={{my:1}} /> */}
        <Box sx={contentStyle}>
          {
            props?.children || <div></div>
          }
        </Box>
      </Paper>
    </Modal>
  );
}
const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  bgcolor: grey[100],
  width: '60%',
  minHeight: '50Vh',  
  maxHeight: '95vh',
  overflowY: 'auto'
};
const titleStyle = {
  display: 'flex', 
  flexDirection: 'row', 
  justifyContent: 'space-between', 
  alignItems: 'center',
  p:2,
  pb:0
}
const contentStyle = {
  p:2,
}
