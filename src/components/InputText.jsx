import { InputBase } from "@mui/material"
import { grey } from "@mui/material/colors"

export default function InputText(props) {
  const {sx, ...rest} = props
  var styleSx = { 
    px:2,
    flex: 1,
    backgroundColor:'white', 
    borderRadius: '4px',
    flex:1,
    width:'100%', 
    borderStyle: 'solid',
    borderWidth: '1px',
    borderColor: grey[300],
  }
  if(sx) {
    styleSx = {...styleSx, ...sx}
  }

  return (
    <InputBase
      sx={styleSx}
      {...rest}
    />
  )
}