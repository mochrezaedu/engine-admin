import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import { Button, IconButton, InputBase, List, ListItem, ListItemButton, ListItemText } from '@mui/material';
import { Box } from '@mui/system';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { grey } from '@mui/material/colors';
import CreateIcon from '@mui/icons-material/Create';

export default function EditableSelectWithSearch(props) {
  const {onClose,onSubmit,options,keyMatches,optionLabel,description,defaultValue,prependButton} = props
  const [value,setValue] = useState((defaultValue ? defaultValue[optionLabel || 'name']:'') || '')
  const [filteredItems,setFilteredItems] = useState([])
  const [_v,setInputValue] = useState((defaultValue ? defaultValue[optionLabel || 'name']:'') || '')
  const [openSearch, setOpenSearch] = useState(false)

  const _handleInputChange = (args) => {
    if(args) {
      const filteredOptions = options.filter((item) => {
        var found = false
        if(keyMatches) {
          for(var i = 0; i < keyMatches.length; i++) {
            const k = Boolean(String(item[keyMatches[i]])?.toLowerCase().indexOf(args.toLowerCase()) > -1)
            if(k) {
              found = true
              break;
            }
          }
        } else {
          found = Boolean(item['id']?.toLowerCase().indexOf(args.toLowerCase()) > -1)
        }
        return found 
      })
      setFilteredItems(filteredOptions)
    } else {
      setFilteredItems([])
    }
  }
  
  // useEffect(() => {
  //   setFilteredItems([options.slice(20)])
  // }, [])

  return (
    <>
      <Box sx={{pb: 2, mt:1}}>
        <Box sx={{display: 'flex', alignItems: 'center'}}>
          <InputBase
          disabled={Boolean(_v)}
          value={_v[optionLabel || 'name'] || value || ''}
          onChange={(e) => {
            setValue(e.target.value)
            _handleInputChange(e.target.value?.toLowerCase())
          }}
          sx={{ px:2,flex: 1,backgroundColor:'white', borderRadius: '4px',mb:2,flex:1,width:'100%',mb:0, backgroundColor: Boolean(_v) ? grey[300]: 'white', borderWidth: 1, borderStyle: 'solid', borderColor: grey[300]}}
          placeholder={'Search name or id'} />
          {
            Boolean(_v) &&
            <Button aria-label="opentab" onClick={() => setInputValue('')} sx={{backgroundColor: grey[300], color: grey[700], minWidth: 'auto', ml: 1}}>
              <CreateIcon fontSize={'small'} />
            </Button>
          }
        </Box>
        {
          Boolean(value) && Boolean(!_v) &&
          <List sx={{backgroundColor: 'white', maxHeight: 196, overflowY: 'auto',pb:0, borderWidth: 1, borderStyle: 'solid', borderColor: grey[200],mt:1}} dense>
          {
            Boolean(filteredItems.length > 0) &&
            filteredItems.map((item) => {
              return (
                <ListItem disablePadding key={item.id} onClick={() => {
                  setInputValue(item)
                }}>
                  <ListItemButton>
                    <ListItemText primary={`[${item.id}] ${item.name}`} secondary={Boolean(description instanceof Function) ? description(item): ''} />
                  </ListItemButton>
                </ListItem>
              )
            })
          }
          {
            Boolean(value && filteredItems.length === 0) && Boolean(!_v) &&
            <ListItem disablePadding>
              <ListItemButton>
                <ListItemText primary={'No Item Found.'} />
              </ListItemButton>
            </ListItem>
          }
          </List>
        }
      </Box>
      <Box>
        {prependButton || null}
        <Button variant={'contained'} size={'small'} color={'success'} onClick={() => {onSubmit(_v)}}>Simpan</Button>
        <Button variant={'text'} size={'small'} color={'inherit'} onClick={onClose} sx={{'&:hover': {backgroundColor: 'transparent'}}}>Batal</Button>
      </Box>
    </>
  );
}