import { DragDropContext } from "react-beautiful-dnd";
import { useDispatch } from "react-redux";
import FlowApi from "../services/api/Flow/api";
import FlowTextImageEditorListApi from "../services/api/FlowTextImageEditorList/api";
import FlowTextImageEditorListImageApi from "../services/api/FlowTextImageEditorListImage/api";
import { inputActions } from "../services/app/input/InputSlicer";
import { FlowModelActions } from "../services/datacenter/flow";
import { FlowChoiceActions } from "../services/datacenter/flow_choices";
import { FlowTextImageEditorListAction } from "../services/datacenter/flow_text_image_editor_list";
import { FlowTextImageEditorListImageActions } from "../services/datacenter/flow_text_image_editor_list_image";

export default function DndProvider(props) {
  const {children,action} = props;
  const dispatch = useDispatch();

  const _handleDragStart = (args) => {
    dispatch(inputActions.closeInput())
  }
  
  const _handleDragEnd = (args) => {
    const {destination,source} = args

    if(!destination) {
      return
    }
    if(destination.droppableId === source.droppableId && destination.index === source.index) {
      return
    }

    const regexDroppableFlow = /flow\-group\-([a-zA-Z0-9]+)/
    if(destination?.droppableId.match(regexDroppableFlow)) {
      if(!destination) {
        return
      } else if(destination.droppableId === source.droppableId && destination.index === source.index) {
        
      } else {
        const group_id = args.destination?.droppableId?.replace('flow-group-', '')
        const payload = {...args, filter: {'flow_group_id': [group_id]}}
        dispatch(FlowModelActions.reorderRows(payload))
        dispatch(FlowApi.reorder({
          data: {...payload, flow_group_id: group_id}
        }))
      }
    } 

    const regexDroppableChoice = /flow\-([a-zA-Z0-9]+)\-dp\-choice/
    if(destination?.droppableId.match(regexDroppableChoice)) {
      const {droppableId} = destination
      const flowId = droppableId.match(regexDroppableChoice)[1]
      if(flowId) {
        if(!destination) {
          return
        } else if(destination.droppableId === source.droppableId && destination.index === source.index) {
          const payload = {...args, filter: {flow_master_id: [flowId]}}
          dispatch(FlowChoiceActions.reorderRows(payload)) 
        }
      }
    } 

    const regexDroppableFlowEditorList = /flow\-([a-zA-Z0-9]+)\-editor/
    if(destination?.droppableId.match(regexDroppableFlowEditorList)) {
      const {droppableId} = destination
      const flowId = droppableId.match(regexDroppableFlowEditorList)[1]
      const payload = {...args, filter: {flow_id: [flowId]}}
      dispatch(FlowTextImageEditorListAction.reorderRows(payload))
      dispatch(FlowTextImageEditorListApi.reorder({
        data: {...payload, flow_id: flowId}
      }))
    }

    const regexDroppableFlowEditorListImage = /flow\-([a-zA-Z0-9]+)\-text\-image\-editor\-([0-9]+)\-image/
    if(destination?.droppableId.match(regexDroppableFlowEditorListImage)) {
      const {droppableId} = destination
      const flowId = droppableId.match(regexDroppableFlowEditorListImage)[1]
      const flowTextImageEditorListImageId = droppableId.match(regexDroppableFlowEditorListImage)[2]
      const payload = {...args, filter: {flow_text_image_editor_list_id: [flowTextImageEditorListImageId]}}
      dispatch(FlowTextImageEditorListImageActions.reorderRows(payload))
      dispatch(FlowTextImageEditorListImageApi.reorder({
        data: {...payload, flow_text_image_editor_list_id: flowTextImageEditorListImageId}
      }))
    }
  }

  return (
    <DragDropContext onDragStart={_handleDragStart} onDragEnd={_handleDragEnd}>
      {children}
    </DragDropContext>
  )
}