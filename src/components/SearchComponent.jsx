import { Box, Button, InputBase } from "@mui/material";
import { grey } from "@mui/material/colors";
import { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {actions,get} from '../services/app/input/InputSlicer'

export default function EditableText(props) {
  const {onClose,onSubmit,...rest} = props;
  if(!(onClose instanceof Function)) {
    throw 'Action close required.'
  }

  const dispatch = useDispatch()
  const key = useSelector((state) => get(state,'key'))
  const value = useSelector((state) => get(state,'value'))

  const [_value,setValue] = useState(value || '')
  const [_submitted, setSubmitted] = useState(false)

  const _handleUserSubmitting = (args) => {
    if(args.key === 'Enter' && args.keyCode === 13) {
      // dispatch(actions.setValue(_value))
      // setSubmitted(true)
    }
  }

  useEffect(() => {
    if(_submitted) {
      onSubmit()
    }
  }, [value,_submitted])

  return (
    <>
      <InputBase
        sx={{ px:2,flex: 1,backgroundColor:'white', borderRadius: '4px',mb:2,flex:1,width:'100%', borderStyle: 'solid',borderWidth: '1px',borderColor: grey[300] }}
        placeholder=""
        inputProps={{ 'aria-label': '' }}
        value={_value}
        onChange={(e) => setValue(e.target.value)}
        onKeyUp={_handleUserSubmitting}
        {...rest}
      />
      <Box>
        <Button variant={'contained'} size={'small'} color={'success'} onClick={() => {
          dispatch(actions.setValue(_value))
          setSubmitted(true)
          onSubmit(_value)
        }}>Simpan</Button>
        <Button variant={'text'} size={'small'} color={'inherit'} onClick={onClose} sx={{'&:hover': {backgroundColor: 'transparent'}}}>Batal</Button>
      </Box>
    </>
  )
}