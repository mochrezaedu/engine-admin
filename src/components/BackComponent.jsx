import { Button, IconButton } from "@mui/material";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

export default function BackComponent(props) {
  return (
    <IconButton edge={'start'} size={'large'} sx={{color: 'white',fontWeight: '900'}} {...props}>
      <ArrowBackIcon />
    </IconButton>
  )
}