export const reorderArray = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  result.splice(endIndex, 0, result.splice(startIndex, 1)[0]);

  return result;
};
export const Uniqid = () => {
  return Math.random().toString(36).substring(2) + (new Date()).getTime().toString(36);
}