import { reorderArray } from "./helper";

export default class Collection {
    constructor(props) {
      if(!(props instanceof Array)) {
          throw 'Collection props must be an Array.';
      }

      this.values = props;
    }

    all() {
      const r = this.values
      return r.map((item) => {const n = item;return n;});
    }
    first() {
      return this.values[0] || null
    }
    reorderAsc(startIndex,endIndex) {
      this.values = reorderArray(this.values,startIndex,endIndex)
      return this.values
    }

    mergeByKey(items, key) {
        if(!(items instanceof Array)) {
            throw 'Merge prop must be an Array.';
        }
        if(!key) {
            throw 'Merge key required.';
        }

        const values = this.values
        const array = items

        array.forEach((a,b) => {
            const found = values.findIndex((c) => c.id === a[key || 'id']);
            if(found === -1) {
              const z = a
              values.push(z);
            }
        })
        
        const _n = values
        return _n
    }
}