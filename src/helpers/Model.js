import { Uniqid } from "./helper"

export default class Model {
  constructor(props) {
    if(!props) {
      throw 'Path required.'
    }

    // SET PATH
    this.path = props

    // EXTRACTOR
    this.extractSetter = this.extractSetter.bind(this)
    this.extractGetter = this.extractGetter.bind(this)
    
    // ADDITIONAL BINDER
    this.getStoreState = this.getStoreState.bind(this)
    this.reorderRows = this.reorderRows.bind(this)
    this.isPending = this.isPending.bind(this)
    this.getPendingIds = this.getPendingIds.bind(this)
    this.isFailed = this.isFailed.bind(this)
    this.getLoadingState = this.getLoadingState.bind(this)

    // GETTER BINDER
    this.all = this.all.bind(this)
    this.get = this.get.bind(this)
    this.last = this.last.bind(this)

    // SETTER BINDER
    this.createMany = this.createMany.bind(this)
    this.createEmpty = this.createEmpty.bind(this)
    this.create = this.create.bind(this)
    this.createPending = this.createPending.bind(this)
    this.createFailed = this.createFailed.bind(this)
    this.delete = this.delete.bind(this)
    this.deletePending = this.deletePending.bind(this)
    this.update = this.update.bind(this)
    this.updateMany = this.updateMany.bind(this)
    this.sync = this.sync.bind(this)
  }

  extractSetter() {
    return {
      createEmpty: this.createEmpty,
      createMany: this.createMany,
      create: this.create,
      reorderRows: this.reorderRows,
      delete: this.delete,
      deletePending: this.deletePending,
      deleteFailed: this.deleteFailed,
      update: this.update,
      updateMany: this.updateMany,
      sync:this.sync
    }
  }
  extractGetter() {
    return {
      all: this.all,
      get: this.get,
      last: this.last,
      isPending:this.isPending,
      getPendingIds: this.getPendingIds,
      isFailed:this.isFailed,
      getLoadingState: this.getLoadingState
    }
  }
  getStoreState(state) {
    const prefixes = this.path.split('.');
    let stateStore = null;
    
    prefixes.map((item,index) => {
      if(stateStore === null) {
        stateStore = state[item]
      } else if(stateStore !== false) {
        stateStore = stateStore[item]
      } else {
        stateStore = false;
      }
    })

    if(!stateStore) {
      throw 'Store path not found.'
    }
  
    return stateStore
  }

  // ADDITIONAL METHODS
  reorderRows(state, action) {
    const {draggableId, source, destination, filter} = action.payload

    if(!destination) {
      return
    }
    if(destination.droppableId === source.droppableId && destination.index === source.index) {
      return
    }

    if(filter && filter instanceof Object) {
      const newRows = state.data.filter((item) => {
        const keys = Object.keys(filter)
        for(var i = 0; i < keys.length; i++) {
          if(filter[keys[i]].includes(String(item[keys[i]]))) {
            return true
          }
        }      
      })
      if(newRows.length > 0) {
        newRows.sort((i,j) => (i.order - j.order)).reorderAsc(source.index,destination.index)
        this.updateMany(state,{payload: newRows})
      }
    } else if(filter && filter instanceof Function) {
      const newRows = state.data.filter(filter)
      newRows.sort((i,j) => (i.order - j.order)).reorderAsc(source.index,destination.index)
      this.updateMany(state,{payload: newRows})
    }
  }
  isFailed(state,payload) {
    if(!(payload)) {
      throw 'Model method [isFailed] payload required.'
    }
    if(!this.path) {
      throw 'Model method [isFailed] required path.';
    }

    const currentFailedState = this.getStoreState(state)?.failedIds;
  
    if(currentFailedState.includes(payload)) {
      return true;
    } 
  
    return false
  }
  isPending(state,payload) {
    if(!(payload)) {
      throw 'Model method [isPending] payload required.'
    }
    if(!this.path) {
      throw 'Model method [isPending] required path.';
    }

    const currentPendingState = this.getStoreState(state)?.pendingIds;
  
    if(currentPendingState.includes(payload)) {
      return true;
    } 
  
    return false
  }
  
  hasPending(state) {
    return (this.getStoreState(state, this.path)?.pendingIds.length > 0);
  }

  getLoadingState(state) {
    return (this.getStoreState(state, this.path)?.status); 
  }

  getPendingIds(state) {
    const activeData = this.getStoreState(state, this.path)?.data
    const pendingIds = this.getStoreState(state, this.path)?.pendingIds
    const pendingData = activeData.filter((item) => pendingIds.includes(item.id))
    return pendingIds
  }

  // GETTER
  all(state) {
    const currentRows = this.getStoreState(state)?.data
  
    return currentRows;
  }
  get(state, callback) {
    if(!(callback instanceof Function)) {
      throw 'Get parameter must be function.'
    }
    if(!this.path) {
      throw 'Model method [isDataPending] required path.';
    }

    const currentRows = this.getStoreState(state, this.path)?.data;

    return currentRows.filter(callback);
  }
  last(state) {
    const currentRows = this.getStoreState(state)?.data
    if(currentRows.length > 0) {
      return currentRows[currentRows.length -1]
    }
    return null;
  }

  // SETTER

  create(state, action) {
    const {payload} = action
    let oldData = state.data
    const assignData = {id:Uniqid(),name: Uniqid(),...payload}
    state.data = [...oldData,assignData]
  }

  createEmpty(state, action) {
    const {payload} = action
    let oldData = state.data
    const assignData = {id:Uniqid(),name: Uniqid(),...payload}
    state.data = [...oldData,assignData]
    this.createPending(state, {payload: assignData.id})
  }

  /**
   * 
   * @param {*} state 
   * @param {Array} action
   */
  upsert(state, action) {
    const payload = action.payload
    if(!['function','array','object'].includes(typeof payload)) {
      throw 'Update Many instance must be [Function / Array].'
    }
    
    payload.map((itemObject) => {
      const itemIndex = state.data.findIndex((item) => item.id === itemObject.id)
      if(itemIndex > -1) {
        state.data[itemIndex] = itemObject
      }
    })
  }

  /**
   * 
   * @param {*} state 
   * @param {Array} action
   */
  updateMany(state, action) {
    const payload = action.payload
    if(!['function','array','object'].includes(typeof payload)) {
      throw 'Update Many instance must be [Function / Array].'
    }
    
    payload.map((itemObject) => {
      const itemIndex = state.data.findIndex((item) => item.id === itemObject.id)
      if(itemIndex > -1) {
        state.data[itemIndex] = itemObject
      }
    })
  }

  /**
   * 
   * @param {*} state 
   * @param {Array} action
   */
  update(state, action) {
    const payload = action.payload
    if(!['function','array','object'].includes(typeof payload)) {
      throw 'Update instance must be [Function / Array].'
    }
    
    const itemIndex = state.data.findIndex((item) => item.id === payload.id)
    if(itemIndex > -1) {
      state.data[itemIndex] = {...payload}
    }
  }

  /**
   * 
   * @param {*} state 
   * @param {Array} action
   */
  createMany(state, action) {
    const payload = action.payload
    if(!(payload instanceof Array)) {
      throw 'Create many instance must be Array.';
    }
    const existingData = state.data
    state.data = existingData.mergeByKey(payload, 'id')
  }

  /**
   * 
   * @param {*} state 
   * @param {(String,Number):id} action
   */
  createPending(state, action) {
    const payload = action.payload
    if(!['string','number'].includes(typeof payload)) {
      throw 'Create pending payload must be [String / Number]'
    }

    state.pendingIds = [...state.pendingIds, payload]
  }
  
  /**
   * 
   * @param {*} state 
   * @param {String:id} action
   */
  createFailed(state, action) {
    const payload = action.payload
    if(!['string','number'].includes(typeof payload)) {
      throw 'Create pending payload must be [String / Number]'
    }

    const hasActive = state.data.filter((item) => item.id === payload)
    const hasPending = state.pendingIds.filter((item) => item.id == payload)
    const oldFailed = state.failedIds
    if(!(oldFailed.includes(payload)) && hasActive && hasPending) {
      const newPendingids = state.pendingIds.filter((item) => item !== payload)
      state.pendingIds = [...newPendingids]
      state.failedIds = [...state.failedIds, payload]
    }
  }

  /**
   * 
   * @param {*} state 
   * @param {Array:id, Closure} action
   */
  delete(state, action) {
    const {payload} = action
    if(!['object','function'].includes(typeof payload)) {
      throw 'Delete instance must be [Array / Function].';
    }

    if(payload instanceof Array) {
      const oldData = state.data
      state.data = oldData.filter((item) => !(payload.includes(item.id)))
    }

    if(payload instanceof Function) {
      const oldData = state.pendingIds
      state.data = oldData.filter(payload) 
    }

    this.deletePending(state,action)
    this.deleteFailed(state,action)
  }

  /**
   * 
   * @param {*} state 
   * @param {Array, Closure, Undefined} action
   */
  deletePending(state, action) {
    const {payload} = action
    if(!['object','function','undefined'].includes(typeof payload)) {
      throw 'Delete instance must be [Array / Function / Undefined].';
    }

    if(typeof payload === undefined) {
      state.pendingIds = []
    }

    if(payload instanceof Array) {
      const oldPendingIds = state.pendingIds
      state.pendingIds = oldPendingIds.filter((item) => !(payload.includes(item)))
    }

    if(payload instanceof Function) {
      const oldPendingIds = state.pendingIds
      state.pendingIds = oldPendingIds.filter(payload) 
    }
  }

  /**
   * 
   * @param {*} state 
   * @param {Array, Closure, Undefined} action
   */
  deleteFailed(state, action) {
    const {payload} = action
    if(!['object','function','undefined'].includes(typeof payload)) {
      throw 'Delete instance must be [Array / Function / Undefined].';
    }

    if(typeof payload === undefined) {
      state.pendingIds = []
    }

    if(payload instanceof Array) {
      const oldFailedIds = state.failedIds
      state.failedIds = oldFailedIds.filter((item) => !(payload.includes(item)))
    }

    if(payload instanceof Function) {
      const oldFailedIds = state.failedIds
      state.failedIds = oldFailedIds.filter(payload) 
    }
  }
  sync(state,action) {
    const {payload} = action
    
    if(payload.data instanceof Object) {
      const existingItems = state.data.filter((item) => {
        return item[payload.key] != payload.data[payload.key]
      })
    }

    if(payload.data instanceof Array) {
      if(payload.data.length > 0) {
        // const existingId = payload.data[0][payload.key] || null
        const existingItems = state.data.filter((item) => {
          var found = 0
          const filterKeys = Object.keys(payload.filter)
          filterKeys.map((itemKey) => {
            if(item[itemKey] == payload.filter[itemKey]) {
              found++
            }
          })
          return found === 0
        })
        state.data = existingItems
        this.createMany(state,{payload:payload.data})
      } else {
        const existingItems = state.data.filter((item) => {
          var found = 0
          const filterKeys = Object.keys(payload.filter)
          filterKeys.map((itemKey) => {
            if(item[itemKey] == payload.filter[itemKey]) {
              found++
            }
          })
          return found > 0
        })
        this.delete(state,{payload:existingItems.map((item) => item.id)})
      }
    }
  }
}