export const getStoreState = (state, path) => {
  if(!path) {
    throw 'Path required.';
  }

  const prefixes = path.split('.');
  let stateStore = null;
  
  prefixes.map((item,index) => {
    if(stateStore === null) {
      stateStore = state[item]
    } else if(stateStore !== false) {
      stateStore = stateStore[item]
    } else {
      stateStore = false;
    }
  })

  return stateStore
}